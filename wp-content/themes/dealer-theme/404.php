<?php get_template_part('templates/page', 'header'); ?>

<div class="alert alert-warning">
 <h2 class="404-error one">404</h2>
 <h2 class="404-error two">PAGE NOT FOUND</h2>
<p style="font-size:2em;"><?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?></p>


<h3>Search Our Products?</h3>

    <form id="fast-search-desktop" style="float:none;" class="form-wrapper search" role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
    <label class="screen-reader-text" for="s"><?php _e( 'Search for:', 'woocommerce' ); ?></label>
    <input class="search-bar" style="border:1px solid black; margin:.5em 0 10em;" type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search Products&hellip;', 'placeholder', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'woocommerce' ); ?>" />
    <input type="hidden" name="post_type" value="product" />
    </form>

    <div class="hot-tub-img">
      <img src="
<?php echo get_stylesheet_directory_uri(); ?>/dist/img/broken-hottub.png"
    </div>

</div>
