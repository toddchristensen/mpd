<style>
	.content-container {
		display: flex !important;
		align-items: center;
		flex-wrap: wrap;
	}

	.carousel-image img {
		max-height: 500px;
	}

	.interchangeable-carousel h3 {
		font-size: 2em;
		text-transform: uppercase;
	}

	.interchangeable-carousel p {
		font-weight: bold;
	}

	.interchangeable-carousel {
		padding: 100px 0;
	}

	.interchangeable-carousel span {
		font-weight: bold;
		font-family: sans-serif;
	}

	/* Default styles from slick slider */
	.slick-dots {
		margin: auto: !important;
	}

.interchangeable-carousel .slick-list {
	text-align: initial !important;
}

	@media (min-width: 1025px) {
		.carousel-image img {
			float: right;
		}

		.carousel-copy h3 {
			text-align: initial !important;
		}
	}

	@media (max-width: 1024px) {
		.slick-slide img {
			display: initial;
		}

		.content-container {
			text-align: center;
		}
	}

</style>


<?php

$categoryName = get_field( 'carousel_category' );

?>

<div class="interchangeable-carousel container">

	<div class="row">

		<div class="sliderauto">

			<?php

			//arguments
			$args = array(

				'category_name'  => $categoryName,
				'posts_per_page' => - 1,
				'orderby'        => 'rand'

			);

			// the query
			$the_query = new WP_Query( $args ); ?>

			<?php if ( $the_query->have_posts() ) : ?>

				<!-- the loop -->
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<section class="content-container">

						<!-- image -->
						<div class="carousel-image small-12 large-5 large-offset-1 columns">

							<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>

							<img src="<?php echo $image[0]; ?>" ;>

						</div>

						<!-- title and content -->
						<div class="carousel-copy small-12 large-4 columns">

							<h3><?php the_title(); ?></h3>

							<?php the_content(); ?>

						</div>

					</section>


				<?php endwhile; ?>

				<!-- end of the loop -->

				<?php wp_reset_postdata(); ?>

			<?php else : ?>

				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>

			<?php endif; ?>


		</div>

	</div>

</div>
