<?php
//FET VARIABLES FROM PAGE

$dsHaveCart = get_field('have_cart', 'option');
$containerWidth = get_field('field_564bafc5095ad', 'option');
?>


<script>
jQuery(function($){
   $('html').addClass('header-4');
});
</script>
<a id="top"></a>
<div class="off-canvas-wrap" data-offcanvas>
    <div class="inner-wrap">
       <div class="header4 the-header">

          <?php include('mobile-header.php') ?>

            <!-- header -->
            <header class="desktop nav cf">
              <?  if($containerWidth == 'auto') { ?>
                  <div class="container cf">
              <? } else { ?>
                  <div class="ds-nav-container cf">
              <? } ?>

<?php $image = get_field('header_logo', 'option'); ?>
<h1><a href="<?= esc_url(home_url('/')); ?>"><?php the_title(); ?> | <?php bloginfo('name'); ?><img src="<?php echo $image['url']; ?>" alt=""></a></h1>

<?php $headtext = get_field('header_additional_text', 'option');
if( !empty($headtext) ){?>
<p class="head-text"><?php echo $headtext ?></p>
<?php } ?>

                </div>

                <nav class="utilities nav">
                  <?  if($containerWidth == 'auto') { ?>
                      <div class="container">
                  <? } else { ?>
                      <div class="ds-nav-container cf">
                  <? } ?>
                   <div class="right">
                   <div class="themenu">
                    <?php $defaults = array(
                                    'theme_location'  => 'util_navigation',
                                    'menu'            => '',
                                    'container'       => '',
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'menu_class'      => 'menu utilities',
                                    'menu_id'         => '',
                                    'echo'            => true,
                                    // 'fallback_cb'     => 'wp_page_menu',
                                    'before'          => '',
                                    'after'           => '',
                                    'link_before'     => '',
                                    'link_after'      => '',
                                    'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                                    'depth'           => 0,
                                    'walker'          => ''
                                );
                                wp_nav_menu( $defaults );

                       if($dsHaveCart == true){

                        // WooCommerce Global variable
                        global $woocommerce;
                        // Header Cart Logic
                        $woo_cart_quantity = $woocommerce->cart->get_cart_contents_count();
                        $cart_link = ($woo_cart_quantity > 0) ? "cart" : "store";
                        ?>
                        <div class="cart-search-combo">
                            <a class="cart" href="<?= $woocommerce->cart->get_cart_url(); ?>" title="Cart"><span class="the-cart-quantity"><?php echo $woo_cart_quantity; ?></span><img src="//watkinsdealer.s3.amazonaws.com/Images/Icons/cart.png" alt="shopping cart" /></a>

                            <?php }else{ ?>

                                <div class="cart-search-combo">

                            <?php } ?>

                            <a class="the-search-icon">Search</a>
                        </div>

                        </div>
                        </div>
                        </div>
                    </nav>

                <nav class="global nav">
                  <?  if($containerWidth == 'auto') { ?>
                      <div class="container">
                  <? } else { ?>
                      <div class="ds-nav-container cf">
                  <? } ?>
                        <?php $defaults = array(
                                'theme_location'  => 'primary_navigation',
                                'menu'            => '',
                                'container'       => '',
                                'container_class' => '',
                                'container_id'    => '',
                                'menu_class'      => 'main nav',
                                'menu_id'         => '',
                                'echo'            => true,
                                // 'fallback_cb'     => 'wp_page_menu',
                                'before'          => '',
                                'after'           => '',
                                'link_before'     => '',
                                'link_after'      => '',
                                'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                                'depth'           => 4,
                                'walker'          => ''
                            );
                            wp_nav_menu( $defaults ); ?>
                            </div>
                   </nav>

                   <div class="form-wrapper">
                       <div class="esc-form"><a href="javascript:;" class="close-search">x</a></div>
                        <form id="fast-search-desktop form-wrapper" class="form-wrapper" style="display:block !important;" role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
                        <label class="screen-reader-text" for="s"><?php _e( 'Search for:', 'woocommerce' ); ?></label>
                        <input class="popup-search-bar search" type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search Products&hellip;', 'placeholder', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'woocommerce' ); ?>" />
                        <input type="hidden" name="post_type" value="product" />
                    </form>
                </div>

           <?php include('product-search-form.php') ?>

            </header>
                </div>
