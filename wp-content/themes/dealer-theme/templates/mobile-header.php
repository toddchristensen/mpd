<div class="mobile nav">

              <header class="mobile">
                  <?php $image = get_field('header_logo', 'option'); ?>
                   <h1><a href="<?php echo esc_url(home_url('/')); ?>" class="hide-text"><?php the_title(); ?> | <?php bloginfo('name'); ?><img src="<?php echo $image['url']; ?>" alt=""></a></h1>

                   <a id="nav-toggle" href="javascript:;"><span></span></a>

                   <nav class="mobile nav">

                        <?php

                        if($dsHaveCart == true){

                        // WooCommerce Global variable
                        global $woocommerce;
                        // Header Cart Logic
                        $woo_cart_quantity = $woocommerce->cart->get_cart_contents_count();
                        $cart_link = ($woo_cart_quantity > 0) ? "cart" : "store";
                        ?>
                        <div class="cart-search-combo">
                            <a class="cart" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="Cart"><span class="the-cart-quantity"><?php echo $woo_cart_quantity; ?></span><img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/dist/img/cart.png" alt="" /></a>

                            <?php }else{ ?>

                                <div class="cart-search-combo">

                            <?php } ?>

                            <form role="search" class="mobile-header-search" method="get" action="<?php echo esc_url(home_url('/')); ?>">

                            <input type="search" size="16" value="" name="s" class="search-field form-control" placeholder="Search Products" required>

                            </form>
                        </div>

                        <div class="the-navigation">

                                    <?php $defaults = array(
                                    'theme_location'  => 'primary_navigation',
                                    'menu'            => '',
                                    'container'       => '',
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'menu_class'      => 'main nav',
                                    'menu_id'         => '',
                                    'echo'            => true,
                                    // 'fallback_cb'     => 'wp_page_menu',
                                    'before'          => '',
                                    'after'           => '',
                                    'link_before'     => '',
                                    'link_after'      => '',
                                    'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                                    'depth'           => 1,
                                    'walker'          => ''
                                );
                                wp_nav_menu( $defaults ); ?>

                                <?php $defaults = array(
                                    'theme_location'  => 'util_navigation',
                                    'menu'            => '',
                                    'container'       => '',
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'menu_class'      => 'menu utilities',
                                    'menu_id'         => '',
                                    'echo'            => true,
                                    // 'fallback_cb'     => 'wp_page_menu',
                                    'before'          => '',
                                    'after'           => '',
                                    'link_before'     => '',
                                    'link_after'      => '',
                                    'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                                    'depth'           => 1,
                                    'walker'          => ''
                                );
                                wp_nav_menu( $defaults ); ?>

                     </div>

                  </nav>

              </header>

        </div>
