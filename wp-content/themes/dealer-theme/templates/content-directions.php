<?php
$startLocation = get_field('start_location');
$endLocation = get_field('end_location');

?>

  <?php mainAspot();
  ?>
<main>
   <div class="container cf">
    <div id="map-overlay">
      <section id="dsMap" class="fifty-fifty"></section>
    </div>
    <section class="instructions fifty-fifty">
       <h2><?php echo the_title(); ?></h2>
       <div><?php echo the_content(); ?></div>
        <div id="instructions">

        </div>
    </section>
</div>
<div>
<?php
// Retrieve General Content Rows
$general_rows = get_field('general_content_block_selector');

$general_rows_orders = array();

// Place General Content Row orders into array
$num_general_rows = 0;

if($general_rows){

    foreach($general_rows as $row){

        $general_rows_orders[$num_general_rows] = $row['global_page_order'];

        $num_general_rows++;
    }
}

// Add main section orders to single array - $sections_array
$sections_array = array($reviews_order, $products_order, $brands_order);

// Add General Content Row orders to array - $sections_array
if(!empty($general_rows_orders)){

    foreach($general_rows_orders as $order){

        array_push($sections_array, $order);
    }
}

/*
 * Loop through the all sections in $sections_array.
 * Displays them according to their section orders.
 * Sections with equivalent orders will cause errors.
 */
$order_count = count($sections_array) + 1 ;
for($i=0; $i<10; $i++){
        $row_num = 0;
        foreach($general_rows as $row){
            if($row['global_page_order'] == $i){
               set_query_var('row_num', $row_num);
                /*----- GENERAL CONTENT ROW (GCR) SECTION -----*/
                /*------- can display multiple GCRs -----------*/
                get_template_part('/templates/blocks/blocks', 'general');
            }
            else {
                $row_num++;
            }
        }
}
            ?>

</div>
</main>
<style type="text/css">
.page-template-template-directions-php main .container{
padding-top: 3em;
padding-bottom: 3em;
padding: 1em;
max-width: 1400px;
}
#dsMap {
/*width: 100%;*/
height: 75vh;
border: #ccc solid 1px;
margin: 20px 0;
width:100%;
}

.page-template-template-directions main #map-overlay {
    float: right;
    width: 50%;
}

/* fixes potential theme css conflict */
#dsMap img {
max-width: inherit !important;
}
#instructions li{
list-style: inside;
font-size: 1.25em;
}
@media only screen and (min-width:720px){
    .fifty-fifty{
        width: 50%;
        float: right;
        padding-bottom: 2em;
        padding-top: 1em;
    }
    .instructions{
        padding-right: 2.5em;
    }
}
</style>



<?php

$startLocationString = get_field('start_location_override');
$endLocationString = get_field('end_location_override');

?>


<script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyAhqthofUu97xg_zGhgzQs5gVjNQfvFnOc"></script>
<script type="text/javascript">

var endLocation = new google.maps.LatLng(<?php echo $endLocation['lat']; ?>, <?php echo $endLocation['lng']; ?>);
var startLocation = new google.maps.LatLng(<?php echo $startLocation['lat']; ?>, <?php echo $startLocation['lng']; ?>);

var endLocationString = '<?php echo $endLocationString ?>';
var startLocationString = '<?php echo $startLocationString ?>';

if (endLocationString) {
    endLocation = endLocationString;
}

if (startLocationString) {
    startLocation = startLocationString;
}



var directionsDisplay = new google.maps.DirectionsRenderer();
var directionsService = new google.maps.DirectionsService();
var map;

function initialize() {
  var mapOptions = { zoom:12, center: endLocation  };
  map = new google.maps.Map(document.getElementById('dsMap'), mapOptions);
  directionsDisplay.setMap(map);
  directionsDisplay.setPanel(document.getElementById('instructions'));
}

function calcRoute(from, to) {
  var request = {
      origin:from,
      destination:to,
      travelMode: google.maps.TravelMode.DRIVING
  };
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
}

console.log('start location: <?php echo $startLocation['lat']; ?>, <?php echo $startLocation['lng']; ?>');
console.log('end location: <?php echo $endLocation['lat']; ?>, <?php echo $endLocation['lng']; ?>');

google.maps.event.addDomListener(window, 'load',initialize);
calcRoute(startLocation,endLocation);




</script>
