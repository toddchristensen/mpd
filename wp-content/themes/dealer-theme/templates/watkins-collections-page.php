<?php //the_content(); ?>
<style>
.entry-title {
    display:none;
}
</style>
<?php //wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
<?php
//Check & add class if is sub page of Hot Tubs
$body_class_php = '';
$is_HotTub = 0;
$actual_link = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if ( (stripos($actual_link, 'hot-tubs')) !== false ) { $body_class_php = 'hot-tub'; $is_HotTub = 1; }
else { $body_class_php = 'non-hot-tub';}
$body_class_php = json_encode($body_class_php);
?>
<script type="text/javascript">
    jQuery(function ($) {
        var body_class = <?php echo $body_class_php; ?>;
        $('body').addClass(body_class);
    });
</script>
<?php
/* GET PAGE CLASS/ BRAND */
$classes = get_body_class();
$brand_class = '';
$brand_page = 0;
if(in_array('hot-spring', $classes)){
    $brand_class = 'hot-spring';
    $brand_page = 1;
}
elseif(in_array('caldera-spas', $classes)){
    $brand_class = 'caldera-spas';
    $brand_page = 1;
}
elseif(in_array('freeflow-spas', $classes)){
    $brand_class = 'freeflow-spas';
    $brand_page = 1;
}
elseif(in_array('fantasy-spas', $classes)){
    $brand_class = 'fantasy-spas';
    $brand_page = 1;
}


?>
<?php

// Custom Static Menu
$custom_static_menu = get_field('custom_static_menu');

$show_title_bar = get_field('show_title_bar');
$text_align = get_field('text_align');
$show_breadcrumbs = get_field('show_breadcrumbs');
$show_anchor = get_field('show_anchor_links');
$show_boxes = get_field('show_collection_boxes');
$show_products = get_field('col_show_products');
$show_videos =get_field('show_videos_reviews');
$boxes_link = 'Categories';
$videos_link = 'Videos & Reviews';
$products_link = 'All Products';
$pricing_link = 'Pricing';
if($is_HotTub) {
    $boxes_link = 'Collections';
    $products_link = 'Hot Tub Models';
    $pricing_link = 'Spa Pricing';
}
?>
<div class="anchor-links-bar">
    <div class="anchor-links-wrapper">
        <?php
echo '<a class="anchor-link-top smooth-scroll" href="#top">Back To Top</a>';

/*if (!empty($show_boxes)) {
                echo '<a class="anchor-link-top smooth-scroll" href="#collections">'.$boxes_link.'</a>';
            }*/

if(!empty($custom_static_menu)) {
  if( have_rows('collections_static_links') ) {
     while ( have_rows('collections_static_links') ) : the_row();

     $collections_static_link_title = get_sub_field('collections_static_link_title');
     $collections_static_link_url = get_sub_field('collections_static_link_url');

      echo '<a class="anchor-link-top smooth-scroll" href="'.$collections_static_link_url.'">'.$collections_static_link_title.'</a>';
     endwhile;
   }
  } else {

  if (!empty($show_videos)) {
      echo '<a class="anchor-link-top smooth-scroll" href="#videoreview">'.$videos_link.'</a>';
  }
  if (!empty($show_products)) {
      echo '<a class="anchor-link-top smooth-scroll" href="#models">'.$products_link.'</a>';
  }
  echo '<a class="anchor-link-top smooth-scroll" href="#pricing">'.$pricing_link.'</a>';
}

        ?>
    </div>
</div>


<?php
$collection_page_title = get_field('ht-page-title');
if(empty($collection_page_title)){
    $collection_page_title = get_the_title();
}
?>

<?php

echo mainAspot();


?>
<?php

if(!empty($show_boxes) ) {
?>
<a id="collections"></a>
<div class="collection-wrapper">
    <div class="row">
       <?php if(!empty($show_title_bar)){?>
        <div class="title-bar">
            <h2 class="title collections-page-title" style="text-align:<?php the_field('text_align'); ?> !important; float:none;"><?php echo $collection_page_title; ?></h2>

            <?php  if(!empty($show_anchor)){

              if(!empty($custom_static_menu)) {
                if( have_rows('collections_static_links') ) {
                  echo '<div class="anchor-wrap">';
                   while ( have_rows('collections_static_links') ) : the_row();

                   $collections_static_link_title = get_sub_field('collections_static_link_title');
                   $collections_static_link_url = get_sub_field('collections_static_link_url');

                    echo '<a class="anchor-link-top smooth-scroll" href="'.$collections_static_link_url.'"><div class="anchor-button">'.$collections_static_link_title.'</div></a>';
                   endwhile;
                   echo '</div>';
                 }
                } else {

                          echo '<div class="anchor-wrap">';
                          echo '<a class="anchor-link-top smooth-scroll" href="#pricing"><div class="anchor-button">'.$pricing_link.'</div></a>';
                          if(!empty($show_products))  { echo '<a href="#models" class="smooth-scroll"><div class="anchor-button">'.$products_link.'</div></a>'; }
                          if(!empty($show_videos))    { echo '<a href="#videoreview" class="smooth-scroll"><div class="anchor-button">'.$videos_link.'</div></a>'; }
                          echo '</div>';
                           } ?>

                           <? } ?>

                          <?php if( !is_front_page() ): ?>

                          <div class="breadcrumbs">
                            <div class="row" style="text-align:center !important;">
                            <? if(!empty($show_breadcrumbs)) { ?>
                              <?php //BREADCRUMBS
                              if ( function_exists('yoast_breadcrumb') ) {
                                yoast_breadcrumb('','');
                              }
                            }
                              ?>
                            </div>
                          </div>
                        <?php endif; ?>
            <div class="clearfix"></div>
        </div>
        <?php } ?>


        <?php // TOP CONTENT AREA

                $show_top_content = get_field('show_top_content_area');
                if(!empty($show_top_content)) {
                    $top_body = get_field('col_top_body_content');
          ?>
                    <div class="collection-top-content cf">
                      <div class="top-body-content">
                        <div class="content-container" style="max-width:<?php the_field('container_width'); ?>px; width: 100%; margin: 0px auto; padding-top:<? the_field('container_padding_top'); ?>px; padding-bottom:<? the_field('container_padding_bottom'); ?>px;">
          <?        echo $top_body; ?>
                        </div>
                      </div>
                    </div>
          <?    } else { ?>


                  <div class="collection-top-content cf">
                    <div class="top-body-content">
                      <div class="content-container" style="max-width:<?php the_field('container_width'); ?>px; width: 100%; margin: 0px auto; padding-top:<? the_field('container_padding_top'); ?>px; padding-bottom:<? the_field('container_padding_bottom'); ?>px;">
              <?php  echo the_content(); ?>
                      </div>
                    </div>
                  </div>
            <?php  } ?>


        <?php


                          $num_collections = get_field('num-collections');
                          $columns = 3;
                          $box_classes = '';
                          if ($num_collections == '3' ) { $large = 4; $medium = 6; $small = 6; $xsmall = 12; $box_classes = 'three-col';}
                          elseif ($num_collections == '4') { $large = 3; $medium = 6; $small = 6; $xsmall = 12; $box_classes = 'four-col'; }
                          elseif ($num_collections == '2') { $large = 6; $medium = 6; $small = 6; $xsmall = 12; $box_classes = 'two-col';}
                          $current_category = get_field('product-category-slider');
                          // check if the repeater field has rows of data
                          if( have_rows('hot-tubs') ):
                          $count = 0;
                          // loop through the rows of data
                          while ( have_rows('hot-tubs') ) : the_row();
                          $image = get_sub_field('collection-image');
                          $title = get_sub_field('collection-title');
                          $desc = get_sub_field('collection-description');
                          $tub_link = get_sub_field('collection-link');
                          $page_title = get_the_title();
                          if ($page_title == 'saunas' ) :
                          $title = str_ireplace('sauna', '', $title);
                          endif;
                          $large_pull = 0;
                          $med_pull = 0;
                          // $title_gradient_bg = "get_field('title_gradient_bg', 'option')";

                          if(get_field('title_gradient_bg')) {
                            $title_gradient_bg = "background: -webkit-linear-gradient(rgba(0,0,0,0.6) 30%, transparent 100%); background: -moz-linear-gradient(rgba(0,0,0,0.6) 30%, transparent 100%); background: -o-linear-gradient(rgba(0,0,0,0.6) 30%, transparent 100%); background: linear-gradient(rgba(0,0,0,0.6) 30%, transparent 100%);";
                          }
                          else {
                            $title_gradient_bg = "background: none;";
                          }
                          //if (($num_collections == '3') && ($count == ($num_collections-1))) {  $med_pull = 6; }
                          //else { $pull = 0; }
                          echo '<div class="collection-box large-'.$large.' medium-6 small-12 xsmall-12 '.$box_classes.' ">';

                          echo '<a href="'.$tub_link.'" target="'.$link_target.'" >

                          <div class="image-title-box  '.$box_classes.' ">';


                           if(!empty($image)){
                            echo '<img src="'.$image.'" alt="'.$title.' Family Image" />';
                          }





                          if(!empty($title)){ ?>
                            <h3 style="<? echo $title_gradient_bg; ?>" class="title"><? echo $title ?></h3> <? }
                          echo '</div></a><!-- end image-title-box -->';
                          //description box
                          if(!empty($desc)){ echo '<div class="description-box">'.$desc.'</div><!-- end desc box -->'; }

                          echo '</div><!-- end collection box -->';
                          $count ++;
                          endwhile;
//                          if(($count%4) != 0){
//                              echo '<div class="collection-box collection-extra large-'.$large.' medium-6 small-6 xsmall-12 ">';
//                              echo '<div class="description-box" >Your source for '.$current_category.' in the ((Location)) area!</div><!-- end desc box -->';
//                              echo '</div><!-- end collection box -->';
//                          } else {}
                          else :
                          // no rows found
                          endif;


?>


<?php } ?>


    </div>
  <!-- CLOSE row -->

  </div>
  <!-- CLOSE collection-wrapper -->


<?php
//Title section condition
$show_slider = get_field('col_show_products');
$model_header = get_field('models_section_header');
if( (!empty($show_title_bar)) && (!empty($show_slider)) && ($show_boxes == false) ){
    ?>
    <style>
        .collection-wrapper{padding-bottom:0;}
        .product-carousel{padding-top:0; }
    </style>
    <?php
    echo '<div class="collection-wrapper">';
    echo '<div class="row">'; ?>
<div class="title-bar title-bar-2">
            <h1 class="title collections-page-title" style="text-align:center !important; float:none;"><?php echo $collection_page_title; ?></h1>


            <?php  if(!empty($show_anchor)){

                          echo '<div class="anchor-wrap">';
                          echo '<a class="anchor-link-top smooth-scroll" href="#pricing"><div class="anchor-button">'.$pricing_link.'</div></a>';
                          if(!empty($show_products))  { echo '<a href="#models" class="smooth-scroll"><div class="anchor-button">'.$products_link.'</div></a>'; }
                          if(!empty($show_videos))    { echo '<a href="#videoreview" class="smooth-scroll"><div class="anchor-button">'.$videos_link.'</div></a>'; }
                          echo '</div>';
                         } ?>

                          <?php if( !is_front_page() ): ?>
                            <div class="breadcrumbs">
                              <div class="row" style="text-align:center !important;">
                              <? if(!empty($show_breadcrumbs)) { ?>
                                <?php //BREADCRUMBS
                                if ( function_exists('yoast_breadcrumb') ) {
                                  yoast_breadcrumb('','');
                                }
                              }
                                ?>
                              </div>
                            </div>
                        <?php endif; ?>
            <div class="clearfix"></div>
        </div>
<?
        $show_top_content = get_field('show_top_content_area');
        if(!empty($show_top_content)) {
            $top_body = get_field('col_top_body_content');
  ?>
            <div class="collection-top-content cf">
              <div class="top-body-content">
                <div class="content-container" style="max-width:<?php the_field('container_width'); ?>px; width: 100%; margin: 0px auto; padding-top:<? the_field('container_padding_top'); ?>px; padding-bottom:<? the_field('container_padding_bottom'); ?>px;">
  <?        echo $top_body; ?>
                </div>
              </div>
            </div>
  <?    } else { ?>


          <div class="collection-top-content cf">
            <div class="top-body-content">
              <div class="content-container" style="max-width:<?php the_field('container_width'); ?>px; width: 100%; margin: 0px auto; padding-top:<? the_field('container_padding_top'); ?>px; padding-bottom:<? the_field('container_padding_bottom'); ?>px;">
      <?php  echo the_content(); ?>
              </div>
            </div>
          </div>
    <?  } ?>



<?php
    echo '</div>';
    echo '</div>';

    $model_header = '';
}
?>
<?php
//H RULE Condition
$show_slider = get_field('col_show_products');
$show_videos =get_field('show_videos_reviews');
if( (!empty($show_slider)) && (!empty($show_boxes)) && (empty($show_videos))  ){
    ?>
    <style>
        .collection-wrapper { padding-bottom:0;}
</style>

    <?php
}
?>


<!------------------------ Video & REVIEWS SECTION-------------------------------------->

<?php
if (!empty(get_field('show_videos_reviews'))) :
$selected_reviews = get_field('select_reviews', false, false);
?>
<a id="videoreview"></a>
<div class="collection-video-reviews">
    <div class="row">
        <!-- SECTION TITLES -->

        <div class="large-6 medium-12 xsmall-12 columns">
            <h2 class="title"><?php the_field('col_videos_header'); ?></h2>
            <div class="video-section">
                <div class="slider">
                    <?php
                    if( have_rows('select_videos') ):

                    while ( have_rows('select_videos') ) : the_row();
                    $posts = get_sub_field('pick_video');
                    foreach($posts as $post){
                        setup_postdata($post);
                        $video_title = get_field('video_header');
                        $embed_code = get_field('youtube_embed_code');
                        if(!empty($embed_code)) {
                            echo '<div>';

                            echo '<div>'.$embed_code.'</div>';
                            echo '</div>';
                        }
                    }
                    wp_reset_postdata();
                    endwhile;

                    else :

                    // no rows found

                    endif;
                    ?>
                </div>
            </div>
        </div>
        <div class="product-reviews large-6 medium-12 xsmall-12 columns">
            <h2 class="title"><?php the_field("col_reviews_header"); ?></h2>
            <div class="quote-section">
                <!--<div class="row">-->
                <?php
//get image variables
    $left_quote = '//watkinsdealer.s3.amazonaws.com/images/sitewide/left-quote.png';
    $right_quote = '//watkinsdealer.s3.amazonaws.com/images/sitewide/right-quote.png';
    $right_quote_white = '//watkinsdealer.s3.amazonaws.com/images/sitewide/right-quote-white1.png';
    $left_quote_white = '//watkinsdealer.s3.amazonaws.com/images/sitewide/left-quote-white1.png';
                ?>
                <?php
$current_category = get_field('col_reviews_category');
$cat_term = get_term($current_category, 'reviews_cat');

                ?>
                <div class="large-12 mobile-quotes"><img src="//watkinsdealer.s3.amazonaws.com/images/sitewide/commas-174x54.png" alt="Commas Icon" /></div>
                <div class="left-quote large-2 medium-2 small-1 xsmall-1 columns"><img src="<?php echo $left_quote_white; ?>" alt="Left Quote Icon" style="float:right;"/></div>
                <div class="large-8 medium-8 small-12 xsmall-12  columns" style="padding-left: 0; padding-right:0;">
                    <div class="slider">

                        <?php



$args = array(
    'post_type'     => 'reviews',
    //'reviews_cat'   => $cat_term->slug,
    'tax_query'     => array(
        array(
            'taxonomy'  =>  'reviews_cat',
            'field'     =>  'slug',
            'terms'     =>  $cat_term->slug,
        ),
    ),
    //'orderby'       => 'count',
    'post__in'          => $selected_reviews,
    'posts_per_page' => 5
);
$query = new WP_Query( $args );
// The Loop
if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {
        $query->the_post();
        $id = get_the_ID();
        /*if( has_category($current_category->term_id) ){*/
        $quote = custom_field_excerpt();
        $owner = get_field('review-owner');
        $location = get_field('review-location');
        $raw_score = get_field('review-score');
        $score = round($raw_score);
        $score_img = '//watkinsdealer.s3.amazonaws.com/images/sitewide/'.$score.'_star_rating.png';

        echo '<div>';
        echo '';
        //if (!empty($score)) : echo '<h3>Rating: '.$score.'</h3>'; else :  endif;
        if (!empty($raw_score)){
            echo '<img src="'.$score_img.'" alt="'.$score_img.' Review Score Icon" class="review-score-img" />';
        }
        //echo '<img src="/wp-content/uploads/2015/07/1_star_rating-Copy.png" style="display:block; margin: 0 auto;" />';
        echo '<p class="quote-author">'.get_the_title().'</p>';
        echo '<blockquote>'. $quote .'</blockquote>';
        if( (!empty($owner))  && (!empty($location)) ){
            echo '<p class="quote-author">'. $owner .' - '.$location.'</p>';
        }
        echo '</div>';
        /*}else { echo " &nbsp;"; }*/
    }
} else {
    echo '<p class="quote-author">There are no reviews for this category at this time</p>';
}
wp_reset_postdata();
                        ?>
                    </div>
                </div>
                <div class="right-quote large-2 medium-2 small-1 xsmall-1 columns"><img src="<?php echo $right_quote_white; ?>" alt="Right Quote Icon" style="float:left;" /></div>
                <div style="clear:both;"></div>
                <!--</div>-->
                <!--<div class="large-12 columns"><a href="<?php the_field('reviews-link'); ?>"><div class="view-more-reviews"><?php the_field('all-reviews-text'); ?></div></a></div>-->
                <div style="clear:both;"></div>
            </div>

        </div><!--end review side-->
    </div>
</div>
<?php
else :
endif;
?>
<!------------------------ end reviews section---------------------------------->


<!-------------------- PRODUCT LIST ---->


<!-- FACET FILTER FOR HOT TUBS PAGE (ONLY HOT TUBS PAGE FOR NOW)-->
<?php
if( is_page( 'hot-tubs')){ ?>

  <div class="search-bar">
    <div class="row">
      <div class="filter-col xsmall-12 small-12 medium-12 large-2 columns">
        <p class="form-head">Filter</p>
      </div>
      <div class="filter-col xsmall-12 small-12 medium-12 large-3 columns">
          <?php echo facetwp_display( 'facet', 'categories' ); ?>
      </div>
      <div class="filter-col xsmall-12 small-12 medium-12 large-3 columns">
          <?php echo facetwp_display( 'facet', 'hot_tub_size' ); ?>
      </div>
      <div class="filter-col xsmall-12 small-12 medium-12 large-3 columns">
        <style>
        .facetwp-search {
          height:50px !important;
          width:100%;
        }
        .facetwp-facet {
          margin-bottom: initial !important;
          margin-top: 5px;
        }

        .facetwp-dropdown {
          /*margin-top: 5px;*/
        }

        /*.product-model-section {
          display: flex !important;
          flex-wrap: wrap !important;
          width: 100% !important;
          margin-top: 50px;
        }*/

        .product-model-section .facetwp-template {
          display: flex !important;
          flex-wrap: wrap !important;
          width: 100% !important;
          margin-top: 50px;

        }

        .search-bar button {
          margin-bottom: 10px !important;
          padding: 15px 27px;
          margin-top: 6px !important;

        }

        .page-template-template-collections .facetwp-search,
        .page-template-template-collections .facetwp-dropdown {
          padding-left: 20px;
          max-width: 500px;
        }

        .page-template-template-collections .search-bar {
          min-height: 60px;
          height: initial;
          padding-top: 1em;
          padding-bottom: .5em;
        }
        @media (max-width:1025px) {
          .page-template-template-collections .search-bar {
            padding-bottom: 1.5em;
          }
        }

        @media (min-width: 642px) {
          .page-template-template-collections .facetwp-search,
          .page-template-template-collections .facetwp-dropdown {
            font-size: 18px;
          }
        }

          </style>
          <?php echo facetwp_display( 'facet', 'hot_tub_search' ); ?>
      </div>
        <button onclick="FWP.reset()">Reset</button>
    </div>


  </div>
  <div class="product-model-section">



      <?php echo facetwp_display( 'template', 'hottubs' ); ?>

      <div class="clearfix"></div>
  </div>
  <?php
} else {

?>

<?php
if (!empty(get_field('col_show_products') ) ) :
$show_search = get_field('show_product_search');
$search_placeholder = get_field('search_bar_placeholder');
?>
<a id="models"></a>
<div class="product-carousel">
    <div class="row">
        <?php if(!empty($model_header)){ ?><h2 class="title"><?php echo $model_header; ?></h2><?php } ?>
        <?php if(!empty($show_search)) { ?>
        <div class="search-bar">
            <?php
            $search = $_GET['search'];
            $size = $_GET['size'];
            $brand = $_GET['brand'];
            $idents = array();
            if(!empty($search)){
            $idents = explode(" ", $search);
            }
            if(!empty($size)){

            }
            if(!empty($brand)){
            array_push($idents, $brand);
            $uppercase_brand = ucwords(str_ireplace('-', ' ', $brand)) . ' Spas';
            }
            $nospace_identifier = str_replace(' ', '', $search);

            ?>
            <p class="form-head">Filter</p>
            <form action="#models" method="get" class="tub-form" id="tub-form">

                <?php if(!$brand_page){
                    $brandsFilterOptions = get_field('brands_filter_options');
                  ?>
                <select name="brand" class="brand-select" form="tub-form" onchange='this.form.submit()'>
                    <option value="">Brand</option>
                    <?php if( in_array('hot-spring', $brandsFilterOptions) ) { ?>
                    <option value="hot-spring" <?php if($brand == 'hot-spring'){ echo ' selected'; } ?>>Hot Spring Spas</option>
                    <? } ?>
                    <?php if( in_array('caldera', $brandsFilterOptions) ) { ?>
                    <option value="caldera" <?php if($brand == 'caldera'){ echo ' selected'; } ?>>Caldera Spas</option>
                    <? } ?>
                    <?php if( in_array('freeflow', $brandsFilterOptions) ) { ?>
                    <option value="freeflow" <?php if($brand == 'freeflow'){ echo ' selected'; } ?>>Freeflow Spas</option>
                      <? } ?>
                      <?php if( in_array('fantasy', $brandsFilterOptions) ) { ?>
                    <option value="fantasy" <?php if($brand == 'fantasy'){ echo ' selected'; } ?>>Fantasy Spas</option>
                      <? } ?>
                </select>
                <?php } else{?>
                <style>
                .search-bar form.tub-form input.filter-text{
                    width: calc(66% - 215px);
                }
                @media screen and (max-width:820px){
                    .search-bar form.tub-form input.filter-text{
                    width: calc(66% - 2px);
                    }
                    .search-bar form.tub-form select.size-select{
                        width: calc(33.33% - 1px;)

                    }
                }

                @media screen and (max-width:530px){
                    .search-bar form.tub-form input.filter-text{
                        width: 100%;
                        margin-bottom:5px !important;
                    }
                    .search-bar form.tub-form input.filter-submit{
                        width: 100%;
                        margin-left: 0 !important;
                    }
                }
                </style>
                <?php }?>
                <select name="size" class="size-select" form="tub-form" onchange='this.form.submit()'>
                    <option value="">Size</option>
                    <option value="2" <?php if($size == 2){ echo ' selected'; } ?>>2 Person</option>
                    <option value="3" <?php if($size == 3){ echo ' selected'; } ?>>3 Person</option>
                    <option value="4" <?php if($size == 4){ echo ' selected'; } ?>>4 Person</option>
                    <option value="5" <?php if($size == 5){ echo ' selected'; } ?>>5 Person</option>
                    <option value="6" <?php if($size == 6){ echo ' selected'; } ?>>6 Person</option>
                    <option value="7" <?php if($size == 7){ echo ' selected'; } ?>>7 Person</option>
                    <option value="8" <?php if($size == 8){ echo ' selected'; } ?>>8 Person</option>
                </select>

                <?php if (!empty($search_placeholder)) { ?>
                <input type="text" name="search" class="filter-text" placeholder="<?php echo $search_placeholder;?>">
                <?php } else { ?>
                <input type="text" name="search" class="filter-text">
                <?php } ?>

                <!--<noscript><input type="submit" value="Search" class="filter-submit"></noscript>-->
                <input type="submit" value="Search" class="filter-submit">
                <a href="<?php echo get_permalink(). '#models'; ?>" class="filter-reset">Reset</a>
                <div class="clear"></div>
            </form>
        </div>
        <?php } ?>
        <div class="product-model-section">

            <?php
                remove_all_filters('posts_orderby');
                $current_category = get_field('product-category-slider');
                $cat_term = get_term($current_category, 'product_cat');
                $args = array(
                    'post_type'     => 'product',
                    'product_cat'   => $cat_term->slug,
                    'meta_key'              => 'global_product_order',
                    'orderby' 				=> 'meta_value_num',
                    'order' 				=> 'DESC',
                    'posts_per_page' => -1,
                );

                $query = new WP_Query( $args );
                $count = 0;
                $no_size = 0;
                if( (empty($size)) && ($count==0) ) { $no_size = 1; }
                // The Loop
                if ( $query->have_posts() ) {
                    while ( $query->have_posts() ) {
                        $query->the_post();
                        /*Search Vars*/
                        $product_id = get_the_ID();
                        $product_name = get_the_title();
                        $product_cap = get_field('product_cap');


                        /*Get Product Collection*/
                        $collection_field = get_field('product-collection');
                        $collection = get_term($collection_field, 'product_cat');
                        $collection_name = $collection->name;
                        /*Get product Brand*/
                        $product_brand = '';
                        $terms = get_the_terms( $product_id, 'product_cat' );
                        $brands = array('hot-spring', 'caldera-spas', 'fantasy-spas', 'freeflow-spas');
                        $hot_spring_collections = array('highlife-nxt', 'highlife', 'limelight', 'hotspot');
                        $caldera_collections = array('utopia', 'paradise', 'vacanza');


                        foreach($terms as $term){
                            //echo $term->slug . '<br/>';
                            $parent_cat = $term->slug;
                            //Freeflow or Fantasy
                            if(empty($product_brand)){
                                foreach($brands as $brand){
                                    if($brand === $parent_cat){
                                        $product_brand = $parent_cat;
                                    }
                                }
                            }
                            //Hotspring
                            if(empty($product_brand)){
                                foreach($hot_spring_collections as $brand){
                                    if($brand === $parent_cat){
                                        $product_brand = 'hot-spring';
                                    }
                                }
                            }
                            //Caldera
                            if(empty($product_brand)){
                                foreach($caldera_collections as $brand){
                                    if($brand === $parent_cat){
                                        $product_brand = 'caldera';
                                    }
                                }
                            }
                        }
                        /*Filter Process*/
                        if (!empty($idents) || !empty($size) ) {
                            $current_id = $product_id;
                            $previous_id = '';
                            if(empty($idents)){
                                array_push($idents, $size);
                            }
                            if($no_size){
                                $size = $product_cap;
                            }
                            $identities = array($product_name, $product_cap, $collection_name, $product_brand );
                            foreach($identities as $attr){
                                foreach($idents as $ident) {
                                    if ( (stripos($attr, $ident)!==false) && (stripos($product_cap, $size)!==false)){



                                        if($previous_id == $current_id){
                                            //Skip
                                        }
                                        else {
                                            $top_image = get_field('top_down_image_thumbnail_url');/*get_field('jet-top-image');*/
                                            $link = get_permalink($query->post->ID);
                                            $jet_count = get_field('jet_count');
                                            $width = get_field('width');
                                            $length = get_field('length');
                                            $height = get_field('height');
                                            /*$dim = $width. ' x ' .$length. ' x ' .$height;*/
                                            $dim = $width. ' x ' .$length. '';
                                            if(empty($height)){
                                            $dim = $width. ' x ' .$length. '';
                                            }
                                            if(empty($length)){
                                            $dim = $width. ' x ' .$height. '';
                                            }
                                            if(empty($width)){
                                            $dim = $length. ' x ' .$height. '';
                                            }
                                            $vol = get_field('product_volume');
                                            $dim_toggle = 0;
                                            if ( (!empty($width)) || (!empty($length)) || (!empty($height))  ) { $dim_toggle = 1; }
                                            $feat_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full');

                                            if ((!empty($feat_img[0])) || (!empty($top_image)) ) :

                                            echo '<div class="product-slider-box large-4 medium-6 small-12 xsmall-12">';
                                            /*echo '<a href="'.$link.'"><p class="product-model-slide-title">'.$product_name.' '.$product_cap.'</p>';*/
                                            if (!is_array($collection_name) && ($current_category == 'Hot-Tubs') ) :
                                            echo '<a href="'.$collection_link.'" class="collection-link"><p>'.$collection_name.'</p></a>';
                                            endif;
                                            if(!empty($top_image) ): echo '<a class="prod-img-wrap" href="'.$link.'"><img src="'.$top_image.'" alt="'.$collection_name.' Family Image" /></a>';
                                            else :
                                            //$feat_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full');
                                            $alt_img = get_field('jet-top-image');
                                            if (!empty($alt_img)) :
                                            echo '<a class="prod-img-wrap" href="'.$link.'"><img src="'.$alt_img.'" alt="Jet Top Image" /></a>';
                                            else : echo '<a class="prod-img-wrap" href="'.$link.'"><img src="'.$feat_img[0].'" alt="Prod Image" /></a>';
                                            endif;

                                            endif;
                                            echo '<a href="'.$link.'"><p class="product-model-slide-title">'.$product_name.'</p>';
                                            if(!empty($product_cap)){
                                                echo '<p class="product-capacity">'.$product_cap.' Person Spa</p>';
                                            }
                                            if ( $dim_toggle == 1 ) {
                                                if(!empty($jet_count)){
                                                    echo '<p class="extra-details">'.$dim.'&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;'.$jet_count.' Jets </p>';
                                                }
                                                else {
                                                    echo '<p class="extra-details">'.$dim.'</p>';
                                                }
                                            }
                                            /*$price = get_post_meta( get_the_ID(), '_regular_price');
                                                                echo '<p>'.$price[0].'</p>';*/
                                            echo '</a>';
                                            echo '<div class="clearfix"></div>';

                                            echo '</div>';
                                            $count++;
                                            //if (($count % 3) === 0 && ($count > 0 )) : echo '<div class="clearfix"></div><hr>'; endif;
                                            else : continue;

                                            endif;

                                            $previous_id = $current_id;
                                        }

                                    }
                                }
                            }

                        }else{
                            /* No search query*/
                            /* Show All Hot Tub Products */
                            $product_name = get_the_title();
                            $collection_field = get_field('product-collection');
                            $collection = get_term($collection_field, 'product_cat');
                            $collection_slug = $collection->slug;
                            $collection_name = $collection->name;
                            $top_image = get_field('top_down_image_thumbnail_url');/*get_field('jet-top-image');*/
                            $product_cap = get_field('product_cap');
                            $link = get_permalink($query->post->ID);
                            $jet_count = get_field('jet_count');
                            $width = get_field('width');
                            $length = get_field('length');
                            $height = get_field('height');
                            /*$dim = $width. ' x ' .$length. ' x ' .$height;*/
                            $dim = $width. ' x ' .$length. '';
                            if(empty($height)){
                            $dim = $width. ' x ' .$length. '';
                            }
                            if(empty($length)){
                            $dim = $width. ' x ' .$height. '';
                            }
                            $vol = get_field('product_volume');
                            $dim_toggle = 0;
                            if ( (!empty($width)) || (!empty($length)) || (!empty($height))  ) { $dim_toggle = 1; }
                            $feat_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full');

                            if ((!empty($feat_img[0])) || (!empty($top_image)) ) :

                            echo '<div class="product-slider-box large-4 medium-6 small-12 xsmall-12">';
                            /*echo '<a href="'.$link.'"><p class="product-model-slide-title">'.$product_name.' '.$product_cap.'</p>';*/
                            if (!is_array($collection_name) && ($current_category == 'Hot-Tubs') ) :
                            echo '<a href="'.$collection_link.'" class="collection-link"><p>'.$collection_name.'</p></a>';
                            endif;
                            if(!empty($top_image) ): echo '<a class="prod-img-wrap" href="'.$link.'"><img src="'.$top_image.'" alt="Collection Top Image" /></a>';
                            else :
                            //$feat_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full');
                            $alt_img = get_field('jet-top-image');
                            if (!empty($alt_img)) :
                            echo '<a class="prod-img-wrap" href="'.$link.'"><img src="'.$alt_img.'" alt="Jet Top Image" /></a>';
                            else : echo '<a class="prod-img-wrap" href="'.$link.'"><img src="'.$feat_img[0].'" alt="Product Image"/></a>';
                            endif;

                            endif;
                            echo '<a href="'.$link.'"><p class="product-model-slide-title">'.$product_name.'</p>';
                            if(!empty($product_cap)){
                                echo '<p class="product-capacity">'.$product_cap.' Person Spa</p>';
                            }
                            if ( $dim_toggle == 1 ) {
                                if(!empty($jet_count)){
                                    echo '<p class="extra-details">'.$dim.'&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;'.$jet_count.' Jets </p>';
                                }
                                else {
                                    echo '<p class="extra-details">'.$dim.'</p>';
                                }
                            }
                            /*$price = get_post_meta( get_the_ID(), '_regular_price');
                                                        echo '<p>'.$price[0].'</p>';*/
                            echo '</a>';
                            echo '<div class="clearfix"></div>';

                            echo '</div>';
                            $count++;
                            //if (($count % 3) === 0 && ($count > 0 )) : echo '<div class="clearfix"></div><hr>'; endif;
                            else : continue;

                            endif;

                        }


                    }
                } else {
                    echo " ";
                }
                wp_reset_postdata();
            ?>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php
else:
endif;

} //END IF/ELSE HOT-TUBS PAGE

?>


<?



$show_bottom_content = get_field('show_bottom_content_area');
if(!empty($show_bottom_content)) {
    $bottom_body = get_field('col_bottom_body_content');
?>
    <div class="collection-top-content cf">
      <div class="top-body-content">
          <div class="content-container" style="max-width:<?php the_field('bottom_container_width'); ?>px; width: 100%; margin: 0px auto; padding-top:<? the_field('bottom_container_padding_top'); ?>px; padding-bottom:<? the_field('bottom_container_padding_bottom'); ?>px;">
<?  echo $bottom_body; ?>
          </div>
      </div>
    </div>
<? } ?>

<!-------------------- REVIEWS ----------->
<?php
if ( (!empty(get_field('show-collection-reviews')) ) && (empty(get_field('show_videos_reviews')))  ):
?>
<div class="collection-reviews">
    <div class="row">
        <div class="quote-section">
            <div class="row">
                <?php
$review_single_header = get_field('col_reviews_header_single');
if(!empty($review_single_header)){
                ?>
                <h2 class="title"><?php echo $review_single_header; ?></h2>
                <?php }
//get image variables
    $left_quote = '//watkinsdealer.s3.amazonaws.com/images/sitewide/left-quote.png';
    $right_quote = '//watkinsdealer.s3.amazonaws.com/images/sitewide/right-quote.png';
                ?>
                <div class="large-12 columns mobile-quotes"><img src="//watkinsdealer.s3.amazonaws.com/images/sitewide/commas-174x54.png" alt="Commas Icon"/></div>
                <div class="large-2 medium-2 small-1 columns"><img src="<?php echo $left_quote; ?>" class="right" alt="Left Quote Icon" /></div>
                <div class="large-8 medium-8 small-12 columns">
                    <div class="slider">

                        <?php


$review_category = get_field('product-category-slider');
$review_term = get_term($current_category, 'product_cat');
$args = array(
    'post_type'     => 'reviews',
    'tax_query'     => array(
        array(
            'taxonomy'  =>  'reviews_cat',
            'field'     =>  'slug',
            'terms'     =>  $review_term->slug,
        ),
    ),
    'posts_per_page' => 5
);
$query = new WP_Query( $args );
// The Loop
if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {
        $query->the_post();
        //$quote = get_field('review-quote');
        $quote = custom_field_excerpt();
        $owner = get_field('review-owner');
        $location = get_field('review-location');
        $raw_score = get_field('review-score');
        $score = round($raw_score);
        $score_img = '//watkinsdealer.s3.amazonaws.com/images/sitewide/'.$score.'_star_rating.png';

        echo '<div>';
        echo '';
        if (!empty($raw_score)){
            echo '<img src="'.$score_img.'" alt="'.$raw_score.' Review Score Icon" class="review-score-img" />';
        }
        echo '<p class="quote-author">'.get_the_title().'</p>';
        echo '<blockquote>'. $quote .'</blockquote>';
        echo '<p class="quote-author">'. $owner .' - '.$location.'</p>';
        echo '</div>';
    }
} else {
    echo " ";
}
wp_reset_postdata();
                        ?>
                    </div>
                </div>
                <div class="large-2 medium-2 small-1 columns"><img src="<?php echo $right_quote; ?>" alt="Right Quote Icon" /></div>
            </div>
        </div>
    </div>
</div>
<?php
else:
endif;
?>
<script>
console.log('watkins collections page');
</script>

            <?php
// Retrieve General Content Rows
$general_rows = get_field('general_content_block_selector');

$general_rows_orders = array();

// Place General Content Row orders into array
$num_general_rows = 0;

if($general_rows){

    foreach($general_rows as $row){

        $general_rows_orders[$num_general_rows] = $row['global_page_order'];

        $num_general_rows++;
    }
}

// Add main section orders to single array - $sections_array
$sections_array = array($reviews_order, $products_order, $brands_order);

// Add General Content Row orders to array - $sections_array
if(!empty($general_rows_orders)){

    foreach($general_rows_orders as $order){

        array_push($sections_array, $order);
    }
}

/*
 * Loop through the all sections in $sections_array.
 * Displays them according to their section orders.
 * Sections with equivalent orders will cause errors.
 */
$order_count = count($sections_array) + 1 ;
for($i=0; $i<10; $i++){
        $row_num = 0;
        if(!empty($general_rows)){
            foreach($general_rows as $row){
                if($row['global_page_order'] == $i){
                   set_query_var('row_num', $row_num);
                    /*----- GENERAL CONTENT ROW (GCR) SECTION -----*/
                    /*------- can display multiple GCRs -----------*/
                    get_template_part('/templates/blocks/blocks', 'general');
                }
                else {
                    $row_num++;
                }
            }
        }
}
?>
