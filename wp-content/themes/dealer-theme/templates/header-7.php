<?php

$containerWidth = get_field( 'field_564bafc5095ad', 'option' );

?>

<script>
    jQuery(function ($) {
        $('html').addClass('header-7');
    });

</script>
<a id="top"></a>
<div class="off-canvas-wrap" data-offcanvas>
    <div class="inner-wrap">
        <div class="header7 the-header">

            <?php include( 'mobile-header.php' ) ?>

            <!-- header -->
            <header class="desktop nav">

                <hr class="thewhiteline">

                <?php if ($containerWidth == 'auto') { ?>

                <div class="container">

                <?php } else { ?>

                <div class="container-fluid">

                <?php } ?>


                        <h1><a href="<?= esc_url( home_url( '/' ) ); ?>"
                               style="background-image:url('<?php echo $image['url']; ?>')">Logo</a></h1>

                        <nav class="global nav">
                            <?php $defaults = array(
                                'theme_location'  => 'primary_navigation',
                                'menu'            => '',
                                'container'       => '',
                                'container_class' => '',
                                'container_id'    => '',
                                'container_id'    => '',
                                'menu_class'      => 'main nav',
                                'menu_id'         => '',
                                'echo'            => true,
                                // 'fallback_cb'     => 'wp_page_menu',
                                'before'          => '',
                                'after'           => '',
                                'link_before'     => '',
                                'link_after'      => '',
                                'items_wrap'      => '<ul class="%2$s block-menu">%3$s</ul>',
                                'depth'           => 0,
                                'walker'          => ''
                            );
                            wp_nav_menu( $defaults ); ?>
                        </nav>


                        <nav class="utilities nav">
                            <?php $defaults = array(
                                'theme_location'  => 'util_navigation',
                                'menu'            => '',
                                'container'       => '',
                                'container_class' => '',
                                'container_id'    => '',
                                'menu_class'      => 'menu utilities',
                                'menu_id'         => '',
                                'echo'            => true,
                                // 'fallback_cb'     => 'wp_page_menu',
                                'before'          => '',
                                'after'           => '',
                                'link_before'     => '',
                                'link_after'      => '',
                                'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                                'depth'           => 0,
                                'walker'          => ''
                            );
                            wp_nav_menu( $defaults ); ?>
                            <?php
                            // WooCommerce Global variable
                            global $woocommerce;
                            // Header Cart Logic
                            $woo_cart_quantity = $woocommerce->cart->get_cart_contents_count();
                            $cart_link         = ( $woo_cart_quantity > 0 ) ? "cart" : "store";
                            ?>
                            <div class="cart-search-combo">
                                <a class="cart" href="<?= $woocommerce->cart->get_cart_url(); ?>" title="Cart"><span
                                        class="the-cart-quantity">(<?php echo $woo_cart_quantity; ?>
                                        ) Items in</span><img src="//watkinsdealer.s3.amazonaws.com/Images/Icons/cart.png" alt="shopping cart" /></a>
                                <a class="the-search-icon">Search</a>
                            </div>
                        </nav>

                    </div>
                    <div class="form-wrapper">
                        <div class="esc-form"></div>
                        <form id="fast-search-desktop" class="form-wrapper search" role="search" method="get"
                              class="woocommerce-product-search" action="/">
                            <label class="screen-reader-text" for="s">Search for:</label>
                            <input class="popup-search-bar" type="search" class="search-field"
                                   placeholder="Search Products&hellip;" value="" name="s" title="Search for:"/>
                            <input type="hidden" name="post_type" value="product"/>
                        </form>
                    </div>
            </header>
        </div>
        <!-- <script>
            jQuery(function ($) {
              $('.block-menu').find('a').each(function(){

var el = $(this),
     elText = el.text();

el.addClass("three-d");
el.append('<span aria-hidden="true" class="three-d-box"><span class="front">'+elText+'</span><span class="back">'+elText+'</span></span>');


});
            });


        </script> -->
