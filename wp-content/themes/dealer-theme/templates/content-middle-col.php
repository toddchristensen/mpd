<?php
// Extract Page Data Field Variables
$title = get_the_title();
$content = get_the_content();
//Display Fields
?>
<style>
#middle-col {
    width:680px;
    margin:0 auto;
}

#middle-col p {
  padding:15px 0px;
}

.page-template-template-middle-col h2 {
    text-align:center;
    padding-top: 40px;
}

@media screen and ( max-width: 980px) {
    #middle-col {
      width:95%;
      margin:0 auto;
    }
}
</style>
  <div class="page-middle-col-content">
    <div class="row">
      <div id="middle-col">
        <?php the_content(); ?>
      </div>
    </div>
