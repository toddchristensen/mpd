<?php
//FET VARIABLES FROM PAGE

$dsHaveCart = get_field('have_cart', 'option');
$containerWidth = get_field('field_564bafc5095ad', 'option');
?>


<script>
jQuery(function($){
   $('html').addClass('header-8');
});
</script>
<a id="top"></a>
<div class="off-canvas-wrap" data-offcanvas>
    <div class="inner-wrap">
       <div class="header8 the-header">

           <?php include('mobile-header.php') ?>

            <!-- header -->
            <header class="desktop nav">

            <?php if ($containerWidth == 'auto' ) { ?>

               <div class="container">

                <?php } else { ?>

                <div class="ds-nav-container">

                <?php } ?>

                    <?php $image = get_field('header_logo', 'option'); ?>

                   <h1><a href="<?= esc_url(home_url('/')); ?>"><?php the_title(); ?> | <?php bloginfo('name'); ?><img src="<?php echo $image['url']; ?>" alt=""></a></h1>

                    <?php $headtext = get_field('header_additional_text', 'option');
                        if( !empty($headtext) ){?>
                            <p class="header-text"><?php echo $headtext ?></p>
                    <?php } ?>

                    <?php $headtext2 = get_field('header_additional_text_2', 'option');
                        if( !empty($headtext2) ){?>
                            <p class="header-text2"><?php echo $headtext2 ?></p>
                    <?php } ?>

                </div>

                <div class="util">

                <?php if ($containerWidth == 'auto' ) { ?>

                <div class="util container">

                 <?php } else { ?>


                <div class="util ds-nav-container">

                <?php } ?>

                <nav class="utilities nav">
                   <div class="container">
                   <div class="themenu cf">
                    <?php $defaults = array(
                                    'theme_location'  => 'util_navigation',
                                    'menu'            => '',
                                    'container'       => '',
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'menu_class'      => 'menu utilities',
                                    'menu_id'         => '',
                                    'echo'            => true,
                                    // 'fallback_cb'     => 'wp_page_menu',
                                    'before'          => '',
                                    'after'           => '',
                                    'link_before'     => '',
                                    'link_after'      => '',
                                    'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                                    'depth'           => 0,
                                    'walker'          => ''
                                );
                                wp_nav_menu( $defaults );

                       if($dsHaveCart == true){

                        // WooCommerce Global variable
                        global $woocommerce;
                        // Header Cart Logic
                        $woo_cart_quantity = $woocommerce->cart->get_cart_contents_count();
                        $cart_link = ($woo_cart_quantity > 0) ? "cart" : "store";
                        ?>
                        <div class="cart-search-combo">
                            <a class="cart" href="<?= $woocommerce->cart->get_cart_url(); ?>" title="Cart"><span class="the-cart-quantity"><?php echo $woo_cart_quantity; ?></span><img src="//watkinsdealer.s3.amazonaws.com/Images/Icons/cart.png" alt="shopping cart" /></a>

                            <?php }else{ ?>

                                <div class="cart-search-combo">

                            <?php } ?>

                            <a class="the-search-icon">Search</a>
                        </div>

                        </div>
                        </div>
                    </nav>

                <nav class="utilities nav2">
                   <div class="container">
                   <div class="themenu cf">
                    <?php $defaults = array(
                                    'theme_location'  => '',
                                    'menu'            => 'Utilities Menu 2 (Header 8)',
                                    'container'       => '',
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'menu_class'      => 'menu utilities',
                                    'menu_id'         => '',
                                    'echo'            => true,
                                    // 'fallback_cb'     => 'wp_page_menu',
                                    'before'          => '',
                                    'after'           => '',
                                    'link_before'     => '',
                                    'link_after'      => '',
                                    'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                                    'depth'           => 0,
                                    'walker'          => ''
                                );
                                wp_nav_menu( $defaults ); ?>
                        </div>
                        </div>
                    </nav>
                    </div>
                    </div>

                <nav class="global nav">
                  <?  if($containerWidth == 'auto') { ?>
                      <div class="container cf">
                  <? } else { ?>
                      <div class="ds-nav-container cf">
                  <? } ?>
                        <?php $defaults = array(
                                'theme_location'  => 'primary_navigation',
                                'menu'            => '',
                                'container'       => '',
                                'container_class' => '',
                                'container_id'    => '',
                                'menu_class'      => 'main nav',
                                'menu_id'         => '',
                                'echo'            => true,
                                // 'fallback_cb'     => 'wp_page_menu',
                                'before'          => '',
                                'after'           => '',
                                'link_before'     => '',
                                'link_after'      => '',
                                'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                                'depth'           => 0,
                                'walker'          => ''
                            );
                            wp_nav_menu( $defaults ); ?>
                            </div>
                   </nav>


           <?php include('product-search-form.php') ?>

            </header>
                </div>
