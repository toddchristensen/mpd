<?php while (have_posts()) : the_post(); ?>

    <?php
        $featured_img = $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
        if($featured_img){
    ?>
        <div class="hero" style="background-image:url('<?php echo $featured_img[0] ?>');">
            <img src="<?php echo $featured_img[0] ?>" alt="">
        </div>

    <?php
        }
        $the_video = get_field('add_blog_post_video');
        if( !empty($the_video) ){
    ?>
            <div class="video-wrap">
                <?php echo $the_video ?>
            </div>
    <? } ?>

    <div class="single-post-wrap" style="margin: 0 auto; padding:1em;">
        <h2 class="entry-title"><?php the_title(); ?></h2>
        <?php //get_template_part('templates/entry-meta'); ?>
        <?php add_filter( 'the_content', 'wpautop' ); ?>
        <?php the_content(); ?>
        <?php comments_template('/templates/comments.php'); ?>        
    </div>
<?php endwhile; ?>

