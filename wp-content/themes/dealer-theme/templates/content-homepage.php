<?php
//global $post;
/*-----------------------
    ASPOT SECTION
-----------------------*/
$aspot_type = get_field( 'homepage_aspot_content_type' );

switch ( $aspot_type ) {

    /*----- VIDEO -----*/
    case 'video':
        get_template_part( 'templates/aspot/aspot', 'video' );
        break;

    /*----- IMAGE -----*/
    case 'image':
        get_template_part( 'templates/aspot/aspot', 'image' );
        break;

         /*----- IMGPAN -----*/
    case 'image-pan':
        get_template_part('templates/aspot/aspot' , 'imgpan');
    break;


    /*----- CUSTOM -----*/
    case 'custom':
        get_template_part( 'templates/aspot/aspot', 'custom' );
        break;

    default:
}
?>


<?php
/*-----------------------
    Entry Copy
-----------------------*/
//global $more;
$more = 0;

$show_read_more = get_field( 'show_read_more' );
$read_more = get_field( 'read_more' );

$content = get_the_content();

 if ($content != '') { ?>
<div class="content-container" style="max-width:<?php the_field('container_width'); ?>px; width: 100%; margin: 0px auto; padding-top:<? the_field('container_padding_top'); ?>px; padding-bottom:<? the_field('container_padding_bottom'); ?>px;">
  <div class="container homepage-copy read-more-wrap" id="section1">
      <style>
      .page-template-template-elementor .homepage-copy{
    width:100% !important;
    max-width:100% !important;
    padding:0 !important;
    margin:0 !important;
}
      </style>
   <?php the_content();

 /* Read more */
  if ( !empty($read_more) && !empty($show_read_more) ) { ?>



    <!-- Read more well -->

      <div class="read-more-content">

        <?php echo $read_more; ?>




     </div>

     <!-- Read more Link -->

     <a role="button" class="read-more-toggle"><strong>Read More</strong></a>
<?php } ?>
  </div>

<?php } ?>

</div>

<?php
/*-----------------------
    End Entry Copy
-----------------------*/
 ?>


<?php
//setup_postdata($post);
// Retrieve Homepage Section Ordering
$brands_order = get_field( 'global_row_order_homebrands' );

$reviews_order = get_field( 'homepage_section_order_reviews' );

$products_order = get_field( 'homepage_section_order_products' );

// Retrieve General Content Rows
$general_rows = get_field( 'general_content_block_selector' );


$general_rows_orders = array();

// Place General Content Row orders into array
$num_general_rows = 0;

if ( $general_rows ) {

    foreach ( $general_rows as $row ) {

        $general_rows_orders[ $num_general_rows ] = $row['global_page_order'];

        $num_general_rows ++;
    }
}

// Add main section orders to single array - $sections_array
$sections_array = array( $reviews_order, $products_order, $brands_order );

// Add General Content Row orders to array - $sections_array
if ( ! empty( $general_rows_orders ) ) {

    foreach ( $general_rows_orders as $order ) {

        array_push( $sections_array, $order );
    }
}

/*
 * Loop through the all sections in $sections_array.
 * Displays them according to their section orders.
 * Sections with equivalent orders will cause errors.
 */
$order_count = count( $sections_array ) + 1;
for ( $i = 0; $i < 10; $i ++ ) {
    if ( $i == $reviews_order ) {
        /*----- review SLIDER SECTION -----*/
        get_template_part( '/templates/sliders/slider', 'reviews-homepage' );
    } else if ( $i == $products_order ) {
        /*----- FEATURE PRODUCTS SLIDER SECTION -----*/
        get_template_part( '/templates/sliders/slider', 'products-homepage' );
    } else if ( $i == $brands_order ) {
        /*----- BRANDS SECTION -----*/
        get_template_part( '/templates/blocks/blocks', 'brands-homepage' );
    } else {
        $row_num = 0;
        /*-- Fixed added (array) so it only does a foreach if its indeed an array if not it skips --*/
        if(!empty($general_rows)){
            foreach($general_rows as $row){

              if ( $row['global_page_order'] == $i ) {
                  set_query_var( 'row_num', $row_num );
                  /*----- GENERAL CONTENT ROW (GCR) SECTION -----*/
                  /*------- can display multiple GCRs -----------*/
                  get_template_part( '/templates/blocks/blocks', 'general' );
              } else {
                  $row_num ++;
              }
          }
        }


    }
}
?>
