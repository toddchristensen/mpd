<?php
//FET VARIABLES FROM PAGE
$containerWidth = get_field('field_564bafc5095ad', 'option');
$dsHaveCart = get_field('have_cart', 'option');
$image = get_field('header_logo', 'option'); 

?>


<script>
jQuery(function($){
   $('html').addClass('header-1');
});
</script>
<a id="top"></a>
<div class="off-canvas-wrap" data-offcanvas>
    <div class="inner-wrap">
       <div class="header1 the-header">

           <?php include('mobile-header.php') ?>

            <!-- header -->

            <header class="desktop nav">
                <?php $headtext = get_field('header_additional_text', 'option');
                if( !empty($headtext) ){?>
                   <div class="border"></div>
                    <nav class="utilities-nav cf">

                     <?  if($containerWidth == 'auto') { ?>
                        <div class="container cf">

                     <? } else { ?>
                        <div class="ds-nav-container cf">
                        <? } ?>

                        <div class="float-right">
                       <?php $defaults = array(
                                    'theme_location'  => 'util_navigation',
                                    'menu'            => '',
                                    'container'       => '',
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'menu_class'      => 'menu utilities cf',
                                    'menu_id'         => '',
                                    'echo'            => true,
                                    // 'fallback_cb'     => 'wp_page_menu',
                                    'before'          => '',
                                    'after'           => '',
                                    'link_before'     => '',
                                    'link_after'      => '',
                                    'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                                    'depth'           => 0,
                                    'walker'          => ''
                                );
                                wp_nav_menu( $defaults );
                        ?>
                        <?php

                        if($dsHaveCart == true){

                            // WooCommerce Global variable
                            global $woocommerce;
                            // Header Cart Logic
                            $woo_cart_quantity = $woocommerce->cart->get_cart_contents_count();
                            $cart_link = ($woo_cart_quantity > 0) ? "cart" : "store";
                            ?>
                            <div class="cart-search-combo cf">
                                <a class="cart" href="<?= $woocommerce->cart->get_cart_url(); ?>" title="Cart"><span class="the-cart-quantity"><?php echo $woo_cart_quantity; ?></span><img src="//watkinsdealer.s3.amazonaws.com/Images/Icons/cart.png" alt="shopping cart" /></a>

                            <?php }else{ ?>

                                <div class="cart-search-combo cf">

                                     <?php } ?>
                                    <a class="the-search-icon">Search</a>
                                    <p class="head-text"><?php echo $headtext ?></p>
                                </div>
                            </div> <!-- CLOSE CART SEARCH COMBO -->

                        </div> <!-- CLOSE FLOAT RIGHT -->

                    </nav> <!-- CLOSE UTIL NAV -->
                <?php } ?>

            <?  if($containerWidth == 'auto') { ?>
                        <div class="container">

                     <? } else { ?>
                        <div class="ds-nav-container cf">
                        <? } ?>

            <h1 style="background-image:url('<?php echo $image['url']; ?>')" ><a href="<?= esc_url(home_url('/')); ?>" class="hide-text"><?php the_title(); ?> | <?php bloginfo('name'); ?></a></h1>

            <nav class="global">

             <?  if($containerWidth == 'auto') { ?>
                <?php $defaults = array(
                    'theme_location'  => 'primary_navigation',
                    'menu'            => '',
                    'container'       => '',
                    'container_class' => '',
                    'container_id'    => '',
                    'menu_class'      => 'main nav cf',
                    'menu_id'         => '',
                    'echo'            => true,
                    // 'fallback_cb'     => 'wp_page_menu',
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                    'depth'           => 4,
                    'walker'          => ''
                );
                wp_nav_menu( $defaults ); ?>

                <?php } else {
                     $defaults = array(
                    'theme_location'  => 'primary_navigation',
                    'menu'            => '',
                    'container'       => '',
                    'container_class' => '',
                    'container_id'    => '',
                    'menu_class'      => 'main nav cf right',
                    'menu_id'         => '',
                    'echo'            => true,
                    // 'fallback_cb'     => 'wp_page_menu',
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                    'depth'           => 4,
                    'walker'          => ''
                );
                wp_nav_menu( $defaults );
                    }
                 ?>


            </nav>
            </div>

           <?php include('product-search-form.php') ?>

            </header>
                </div>
