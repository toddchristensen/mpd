<?php
//Block Settings
$section_layout = get_field('section_layout_general');
$gbs_bg_color = get_field('section_background_color_general');
$gbs_container = get_field('general_block_has_container');
$gbs_padding = get_field('block_spacing_inner');
$gb_custom_class = get_field('gb_custom_class');
$gb_text_align = get_field('general_blocks_text_align');

//Tabbed Settings
$tabbed_section = get_field('tabbed_section');
$tab_count = get_field('tab_count');
$tab_color = get_field('tab_color');
$tab_active_text_color = get_field('tab_active_text_color');
$tab_content_text_color = get_field('tab_content_text_color');
//Tab 1
$tab1_column_count = get_field('tab1_column_count');
$tab1_title = get_field('tab1_title');
$tab1_header_section = get_field('tab1_header_section');
$tab1_column1_content = get_field('tab1_column1_content');
$tab1_column2_content = get_field('tab1_column2_content');
$tab1_column3_content = get_field('tab1_column3_content');
$tab1_column4_content = get_field('tab1_column4_content');
//Tab 2
$tab2_column_count = get_field('tab2_column_count');
$tab2_title = get_field('tab2_title');
$tab2_header_section = get_field('tab2_header_section');
$tab2_column1_content = get_field('tab2_column1_content');
$tab2_column2_content = get_field('tab2_column2_content');
$tab2_column3_content = get_field('tab2_column3_content');
$tab2_column4_content = get_field('tab2_column4_content');
//Tab 3
$tab3_column_count = get_field('tab3_column_count');
$tab3_title = get_field('tab3_title');
$tab3_header_section = get_field('tab3_header_section');
$tab3_column1_content = get_field('tab3_column1_content');
$tab3_column2_content = get_field('tab3_column2_content');
$tab3_column3_content = get_field('tab3_column3_content');
$tab3_column4_content = get_field('tab3_column4_content');
//Tab 4
$tab4_column_count = get_field('tab4_column_count');
$tab4_title = get_field('tab4_title');
$tab4_header_section = get_field('tab4_header_section');
$tab4_column1_content = get_field('tab4_column1_content');
$tab4_column2_content = get_field('tab4_column2_content');
$tab4_column3_content = get_field('tab4_column3_content');
$tab4_column4_content = get_field('tab4_column4_content');
//Tab 5
$tab5_column_count = get_field('tab5_column_count');
$tab5_title = get_field('tab5_title');
$tab5_header_section = get_field('tab5_header_section');
$tab5_column1_content = get_field('tab5_column1_content');
$tab5_column2_content = get_field('tab5_column2_content');
$tab5_column3_content = get_field('tab5_column3_content');
$tab5_column4_content = get_field('tab5_column4_content');
//Tab 6
$tab6_column_count = get_field('tab6_column_count');
$tab6_title = get_field('tab6_title');
$tab6_header_section = get_field('tab6_header_section');
$tab6_column1_content = get_field('tab6_column1_content');
$tab6_column2_content = get_field('tab6_column2_content');
$tab6_column3_content = get_field('tab6_column3_content');
$tab6_column4_content = get_field('tab6_column4_content');
//Tab 7
$tab7_column_count = get_field('tab7_column_count');
$tab7_title = get_field('tab7_title');
$tab7_header_section = get_field('tab7_header_section');
$tab7_column1_content = get_field('tab7_column1_content');
$tab7_column2_content = get_field('tab7_column2_content');
$tab7_column3_content = get_field('tab7_column3_content');
$tab7_column4_content = get_field('tab7_column4_content');
//Tab 8
$tab8_column_count = get_field('tab8_column_count');
$tab8_title = get_field('tab8_title');
$tab8_header_section = get_field('tab8_header_section');
$tab8_column1_content = get_field('tab8_column1_content');
$tab8_column2_content = get_field('tab8_column2_content');
$tab8_column3_content = get_field('tab8_column3_content');
$tab8_column4_content = get_field('tab8_column4_content');


if(!empty($tabbed_section)){
  $tabbed = 'tabbed-section';
} else {
  $tabbed = '';
}

if($tab_count == 6 || $tab_count == 3 || $tab_count == 2) {
  $tab_nav_count = 3;
} elseif($tab_count == 4 || $tab_count == 8) {
  $tab_nav_count = 4;
}


if($tab1_column_count == 2){
  $tab1_div_class = "large-6 xsmall-12 columns content";
}elseif($tab1_column_count == 3){
  $tab1_div_class = "large-4 xsmall-12 columns content";
}elseif($tab1_column_count == 4){
  $tab1_div_class = "large-3 xsmall-12 columns content";
}

if($tab2_column_count == 2){
  $tab2_div_class = "large-6 xsmall-12 columns content";
}elseif($tab2_column_count == 3){
  $tab2_div_class = "large-4 xsmall-12 columns content";
}elseif($tab2_column_count == 4){
  $tab2_div_class = "large-3 xsmall-12 columns content";
}

if($tab3_column_count == 2){
  $tab3_div_class = "large-6 xsmall-12 columns content";
}elseif($tab3_column_count == 3){
  $tab3_div_class = "large-4 xsmall-12 columns content";
}elseif($tab3_column_count == 4){
  $tab3_div_class = "large-3 xsmall-12 columns content";
}

if($tab4_column_count == 2){
  $tab4_div_class = "large-6 xsmall-12 columns content";
}elseif($tab4_column_count == 3){
  $tab4_div_class = "large-4 xsmall-12 columns content";
}elseif($tab4_column_count == 4){
  $tab4_div_class = "large-3 xsmall-12 columns content";
}

if($tab5_column_count == 2){
  $tab5_div_class = "large-6 xsmall-12 columns content";
}elseif($tab5_column_count == 3){
  $tab5_div_class = "large-4 xsmall-12 columns content";
}elseif($tab5_column_count == 4){
  $tab5_div_class = "large-3 xsmall-12 columns content";
}

if($tab6_column_count == 2){
  $tab6_div_class = "large-6 xsmall-12 columns content";
}elseif($tab6_column_count == 3){
  $tab6_div_class = "large-4 xsmall-12 columns content";
}elseif($tab6_column_count == 4){
  $tab6_div_class = "large-3 xsmall-12 columns content";
}

if($tab7_column_count == 2){
  $tab7_div_class = "large-6 xsmall-12 columns content";
}elseif($tab7_column_count == 3){
  $tab7_div_class = "large-4 xsmall-12 columns content";
}elseif($tab7_column_count == 4){
  $tab7_div_class = "large-3 xsmall-12 columns content";
}

if($tab8_column_count == 2){
  $tab8_div_class = "large-6 xsmall-12 columns content";
}elseif($tab8_column_count == 3){
  $tab8_div_class = "large-4 xsmall-12 columns content";
}elseif($tab8_column_count == 4){
  $tab8_div_class = "large-3 xsmall-12 columns content";
}

//Converting HEX to RGB to add an opacity
$tab_hover_hex = $tab_color;
$tab_hover_rgb = hex2rgb($tab_hover_hex);
$tab_hover_color = implode(", ", $tab_hover_rgb);

?>

<style>

.tabbed-section div.tabs .ui-tabs-nav li a:hover {
  background-color: rgba(<?php echo $tab_hover_color; ?>,0.3) !important;
}

.tabbed-section div.tabs p,
.tabbed-section div.tabs h1,
.tabbed-section div.tabs h2,
.tabbed-section div.tabs h3,
.tabbed-section div.tabs h4,
.tabbed-section div.tabs li,
.tabbed-section div.tabs ul {
  color: <? echo $tab_content_text_color; ?>!important;
}

.tabbed-section div.tabs .ui-tabs-nav li a {
  color: <? echo $tab_color; ?>!important;
  border: 4px solid <? echo $tab_color; ?> !important;
}

.tabbed-section div.tabs .ui-tabs-nav li.ui-state-active a {
  color: <? echo $tab_active_text_color; ?>!important;
  background-color: <? echo $tab_color; ?>!important;
}
</style>

<div class="tabs ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-vertical ui-helper-clearfix" style="background-color:<? echo $gbs_bg_color; ?>; padding:<?php echo $gbs_padding ?>;">
    <div class="row tab-nav">
        <ul class="product-desc-list large-12 medium-12 small-12 xsmall-12 columns large-block-grid-<? echo $tab_nav_count; ?> medium-block-grid-<? echo $tab_nav_count; ?> small-block-grid-1 ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
            <li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" tabindex="0"><a id="ui-id-1" class="tab-nav-link-0 ui-tabs-anchor" tabindex="-1" href="#tab1"><? echo $tab1_title; ?></a></li>
            <li class="ui-state-default ui-corner-top" tabindex="-1"><a id="ui-id-2" class="tab-nav-link-1 ui-tabs-anchor" tabindex="-1" href="#tab2"><? echo $tab2_title; ?></a></li>
            <li class="ui-state-default ui-corner-top" tabindex="-1"><a id="ui-id-3" class="tab-nav-link-2 ui-tabs-anchor" tabindex="-1" href="#tab3"><? echo $tab3_title; ?></a></li>
  <? if($tab_count > 3) { ?>
            <li class="ui-state-default ui-corner-top" tabindex="-1"><a id="ui-id-4" class="tab-nav-link-3 ui-tabs-anchor" tabindex="-1" href="#tab4"><? echo $tab4_title; ?></a></li>
            <?  }
  if($tab_count > 4) { ?>
            <li class="ui-state-default ui-corner-top" tabindex="-1"><a id="ui-id-5" class="tab-nav-link-4 ui-tabs-anchor" tabindex="-1" href="#tab5"><? echo $tab5_title; ?></a></li>
            <li class="ui-state-default ui-corner-top" tabindex="-1"><a id="ui-id-6" class="tab-nav-link-5 ui-tabs-anchor" tabindex="-1" href="#tab6"><? echo $tab6_title; ?></a></li>
<?  }
    if($tab_count > 6) { ?>
            <li class="ui-state-default ui-corner-top" tabindex="-1"><a id="ui-id-7" class="tab-nav-link-6 ui-tabs-anchor" tabindex="-1" href="#tab7"><? echo $tab7_title; ?></a></li>
            <li class="ui-state-default ui-corner-top" tabindex="-1"><a id="ui-id-8" class="tab-nav-link-7 ui-tabs-anchor" tabindex="-1" href="#tab8"><? echo $tab8_title; ?></a></li>
<?    }     ?>
        </ul>
    </div>
    <div id="tab1" class="tab-content ui-tabs-panel ui-widget-content ui-corner-bottom">
        <div class="row">
          <?php echo $tab1_header_section; ?>
    <?    if($tab1_column_count > 1){   ?>
            <div class="<? echo $tab1_div_class; ?>">
              <?php echo $tab1_column1_content; ?>
            </div>
            <div class="<? echo $tab1_div_class; ?>">
              <?php echo $tab1_column2_content; ?>
            </div>
    <?    }     ?>

      <? if($tab1_column_count == 3 || $tab1_column_count == 4){  ?>

            <div class="<? echo $tab1_div_class; ?>">
              <?php echo $tab1_column3_content; ?>
            </div>
      <?  } ?>

      <? if($tab1_column_count == 4){   ?>
            <div class="<? echo $tab1_div_class; ?>">
              <?php echo $tab1_column4_content; ?>
            </div>
      <?  }  ?>

        </div>
        <div class="clearfix"></div>
    </div>
    <div id="tab2" class="tab-content ui-tabs-panel ui-widget-content ui-corner-bottom">
        <div class="row">
          <?php echo $tab2_header_section; ?>
    <?    if($tab2_column_count > 1){   ?>
            <div class="<? echo $tab2_div_class; ?>">
              <?php echo $tab2_column1_content; ?>
            </div>
            <div class="<? echo $tab2_div_class; ?>">
              <?php echo $tab2_column2_content; ?>
            </div>
    <?    }   ?>

      <? if($tab2_column_count == 3 || $tab2_column_count == 4){  ?>

            <div class="<? echo $tab2_div_class; ?>">
              <?php echo $tab2_column3_content; ?>
            </div>
      <?  } ?>

      <? if($tab2_column_count == 4){   ?>
            <div class="<? echo $tab2_div_class; ?>">
              <?php echo $tab2_column4_content; ?>
            </div>
      <?  }  ?>

        </div>
        <div class="clearfix"></div>
    </div>
    <div id="tab3" class="tab-content ui-tabs-panel ui-widget-content ui-corner-bottom">
        <div class="row">
          <?php echo $tab3_header_section; ?>
    <?    if($tab3_column_count > 1){   ?>
            <div class="<? echo $tab3_div_class; ?>">
              <?php echo $tab3_column1_content; ?>
            </div>
            <div class="<? echo $tab3_div_class; ?>">
              <?php echo $tab3_column2_content; ?>
            </div>
    <?    }   ?>

      <? if($tab3_column_count == 3 || $tab3_column_count == 4){  ?>

            <div class="<? echo $tab3_div_class; ?>">
              <?php echo $tab3_column3_content; ?>
            </div>
      <?  } ?>

      <? if($tab3_column_count == 4){   ?>
            <div class="<? echo $tab3_div_class; ?>">
              <?php echo $tab3_column4_content; ?>
            </div>
      <?  }  ?>

        </div>
        <div class="clearfix"></div>
    </div>
<? if($tab_count > 3){  ?>
    <div id="tab4" class="tab-content ui-tabs-panel ui-widget-content ui-corner-bottom">
        <div class="row">
          <?php echo $tab4_header_section; ?>
    <?    if($tab4_column_count > 1){   ?>
            <div class="<? echo $tab4_div_class; ?>">
              <?php echo $tab4_column1_content; ?>
            </div>
            <div class="<? echo $tab4_div_class; ?>">
              <?php echo $tab4_column2_content; ?>
            </div>
    <?    }   ?>

      <? if($tab4_column_count == 3 || $tab4_column_count == 4){  ?>

            <div class="<? echo $tab4_div_class; ?>">
              <?php echo $tab4_column3_content; ?>
            </div>
      <?  } ?>

      <? if($tab4_column_count == 4){   ?>
            <div class="<? echo $tab4_div_class; ?>">
              <?php echo $tab4_column4_content; ?>
            </div>
      <?  }  ?>

        </div>
        <div class="clearfix"></div>
    </div>

<?  }  ?>
<? if($tab_count > 5){  ?>
    <div id="tab5" class="tab-content ui-tabs-panel ui-widget-content ui-corner-bottom">
        <div class="row">
          <?php echo $tab5_header_section; ?>
    <?    if($tab5_column_count > 1){   ?>
            <div class="<? echo $tab5_div_class; ?>">
              <?php echo $tab5_column1_content; ?>
            </div>
            <div class="<? echo $tab5_div_class; ?>">
              <?php echo $tab5_column2_content; ?>
            </div>
    <?    }   ?>
      <? if($tab5_column_count == 3 || $tab5_column_count == 4){  ?>

            <div class="<? echo $tab5_div_class; ?>">
              <?php echo $tab5_column3_content; ?>
            </div>
      <?  } ?>

      <? if($tab5_column_count == 4){   ?>
            <div class="<? echo $tab5_div_class; ?>">
              <?php echo $tab5_column4_content; ?>
            </div>
      <?  }  ?>

        </div>
        <div class="clearfix"></div>
    </div>

    <div id="tab6" class="tab-content ui-tabs-panel ui-widget-content ui-corner-bottom">
        <div class="row">
          <?php echo $tab6_header_section; ?>
    <?    if($tab6_column_count > 1){   ?>
            <div class="<? echo $tab6_div_class; ?>">
              <?php echo $tab6_column1_content; ?>
            </div>
            <div class="<? echo $tab6_div_class; ?>">
              <?php echo $tab6_column2_content; ?>
            </div>
    <?    }   ?>

      <? if($tab6_column_count == 3 || $tab6_column_count == 4){  ?>

            <div class="<? echo $tab6_div_class; ?>">
              <?php echo $tab6_column3_content; ?>
            </div>
      <?  } ?>

      <? if($tab6_column_count == 4){   ?>
            <div class="<? echo $tab6_div_class; ?>">
              <?php echo $tab6_column4_content; ?>
            </div>
      <?  }  ?>

        </div>
        <div class="clearfix"></div>
    </div>

<?  }  ?>

<? if($tab_count == 8){  ?>
    <div id="tab7" class="tab-content ui-tabs-panel ui-widget-content ui-corner-bottom">
        <div class="row">
          <?php echo $tab7_header_section; ?>
    <?    if($tab7_column_count > 1){   ?>
            <div class="<? echo $tab7_div_class; ?>">
              <?php echo $tab7_column1_content; ?>
            </div>
            <div class="<? echo $tab7_div_class; ?>">
              <?php echo $tab7_column2_content; ?>
            </div>
    <?    } ?>

      <? if($tab7_column_count == 3 || $tab7_column_count == 4){  ?>

            <div class="<? echo $tab7_div_class; ?>">
              <?php echo $tab7_column3_content; ?>
            </div>
      <?  } ?>

      <? if($tab7_column_count == 4){   ?>
            <div class="<? echo $tab7_div_class; ?>">
              <?php echo $tab7_column4_content; ?>
            </div>
      <?  }  ?>

        </div>
        <div class="clearfix"></div>
    </div>

    <div id="tab8" class="tab-content ui-tabs-panel ui-widget-content ui-corner-bottom">
        <div class="row">
          <?php echo $tab8_header_section; ?>
      <?    if($tab8_column_count > 1){   ?>
            <div class="<? echo $tab8_div_class; ?>">
              <?php echo $tab8_column1_content; ?>
            </div>
            <div class="<? echo $tab8_div_class; ?>">
              <?php echo $tab8_column2_content; ?>
            </div>
      <?    } ?>

      <? if($tab8_column_count == 3 || $tab8_column_count == 4){  ?>

            <div class="<? echo $tab8_div_class; ?>">
              <?php echo $tab8_column3_content; ?>
            </div>
      <?  } ?>

      <? if($tab8_column_count == 4){   ?>
            <div class="<? echo $tab8_div_class; ?>">
              <?php echo $tab8_column4_content; ?>
            </div>
      <?  }  ?>

        </div>
        <div class="clearfix"></div>
    </div>

<?  }  ?>
  </div>
