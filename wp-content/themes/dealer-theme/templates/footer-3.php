<?php   //FOOTER DATA VARIABLES
$footer_title = get_field('footer_contact_title','option');
$footer_left_content = get_field('footer_left_content','option');
$footer_address = get_field('footer_contact_info_1','option');
$footer_contact = get_field('footer_contact_info_2','option');
/*$footer_social = get_field('footer_contact_social','option');*/
$footer_map = get_field('footer_map','option');
$copyright = get_field('copyright','option');
$privacy_page = get_field('privacy_policy_page', 'options');
$privacy_title = get_field('privacy_policy_page_title','options');
$site_by = get_field('site_by','option');
$dark_icons = get_field('footer_icon_color') == 'dark';
$icon_color = get_field('footer_icon_color', 'options');


$social_icons = get_field('footer_social_icons', 'option');
/*list($facebook, $youtube, $yelp, $houzz) = explode($social_icons);*/

if ( empty($privacy_title) ) {
    $privacy_title = $privacy_page->post_title;
}
?>

<script>
jQuery(function($){
   $('html').addClass('footer-3');
});
</script>

<?php


// Check if footer background image is set
if ( get_field( 'add_footer_bg_image', 'option' ) ) { ?>

<!-- Sets image and other properties -->
    <style>
        .footer-3 .TWLA-pre-footer {
            background-image: url(<?php the_field('footer_bg_image', 'option'); ?>);
            background-size: cover;
            background-repeat: no-repeat;
        }

    </style>

<?php } ?>


<div class="clearfix"></div>
<?php //PRINT THE FOOTER
//if( !is_front_page() ):
?>
<a id="pricing"></a>
<div class="footer-3">
<div class="TWLA-pre-footer">
    <a id="contact"></a>
    <div class="row">
        <div class="large-8 large-offset-2 medium-12 small-12 columns">
            <h2 class="footer-title"><?php echo strip_tags($footer_title); ?></h2>
        </div>
    </div>

    <div class="TWLA-footer-menu">
        <div class="row">
            <div class="large-8 large-offset-2 medium-12 small-12 columns">

                <?php $defaults = array(
                    'theme_location'  => '',
                    'menu'            => 'Footer Contact',
                    'container'       => '',
                    'menu_class'      => 'menu',
                    'menu_id'         => '',
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'depth'           => 0,
                    'walker'          => ''
                );
                ?>
              <div class="<? echo $icon_color; ?>">
            <?  wp_nav_menu( $defaults ); ?>
              </div>

            </div>
        </div>
    </div>

    <div class="TWLA-footer-info row">
        <div class="large-3 large-offset-2 medium-12 small-12 columns">
           <div class="contact-info cf">
            <?= $footer_address; ?>
               <div class="social-button " style="padding-bottom: 0;">
                    <?php
                    if (!empty($social_icons)) {
                           if( in_array('Facebook', $social_icons) ) {
                          $facebook_url = get_field('facebook_url','option');
                          echo '<a href="'.$facebook_url.'" class="facebook-social social-icon" target="_blank"></a>';
                      }
                      if( in_array('Youtube', $social_icons) ) {
                          $youtube_url = get_field('youtube_url','option');
                          echo '<a href="'.$youtube_url.'" class="youtube-social social-icon" target="_blank"></a>';
                      }
                      if( in_array('Yelp', $social_icons) ) {
                          $yelp_url = get_field('yelp_url','option');
                          echo '<a href="'.$yelp_url.'" class="yelp-social social-icon" target="_blank"></a>';
                      }
                      if( in_array('Houzz', $social_icons) ) {
                          $houzz_url = get_field('houzz_url','option');
                          echo '<a href="'.$houzz_url.'" class="houzz-social social-icon" target="_blank"></a>';
                      }
                      if( in_array('Google+', $social_icons) ) {
                          $gplus_url = get_field('gplus_url','option');
                          echo '<a href="'.$gplus_url.'" class="gplus-social social-icon" target="_blank"></a>';
                      }
                      if( in_array('Twitter', $social_icons) ) {
                          $twitter_url = get_field('twitter_url','option');
                          echo '<a href="'.$twitter_url.'" class="twitter-social social-icon" target="_blank"></a>';
                      }
                      if( in_array('LinkedIn', $social_icons) ) {
                          $linkedin_url = get_field('linkedin_url','option');
                          echo '<a href="'.$linkedin_url.'" class="linkedin-social social-icon" target="_blank"></a>';
                      }
                      if( in_array('Instagram', $social_icons) ) {
                          $insta_url = get_field('insta_url','option');
                          echo '<a href="'.$insta_url.'" class="insta-social social-icon" target="_blank"></a>';
                      }
                       if( in_array('Pinterest', $social_icons) ) {
                      $pinterest_url = get_field('pinterest_url','option');
                      echo '<a href="'.$pinterest_url.'" class="pinterest-social social-icon" target="_blank"></a>';
                }
                  }
                    ?>
                    </div>
            </div>
        </div>
        <div class="large-5 medium-12 small-12 columns">

            <?= $footer_map; ?>
        </div>
    </div>
</div>

<footer id="footer">
    <div class="row">
        <div class="small-12 xsmall-12 small-centered text-center columns">


            <div class="lower-footer-menu">
                <?php
                    $defaults = array(
                        'theme_location'  => '',
                        'menu'            => 'Lower Footer',
                        'container'       => 'div',
                        'container_class' => 'menu-footer-lower-footer-container',
                        'container_id'    => '',
                        'menu_class'      => 'menu',
                        'menu_id'         => 'menu-footer-lower-footer',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'before'          => '',
                        'after'           => '',
                        'link_before'     => '',
                        'link_after'      => '',
                        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth'           => 0,
                        'walker'          => ''
                    );

                    wp_nav_menu( $defaults );
                ?>
            </div>


            <div class="made-by text-center">
                <a href="https://www.designstudio.com" target="_blank"><img src="
<?php echo get_template_directory_uri(); ?>/dist/img/design-studio-logo.png" alt="Design Studio Logo" /></a>
                       <p class="copy-right"> &nbsp;|&nbsp;  &copy;<?php echo strip_tags(date('Y')) ?> All rights reserved. <span class="version"><?php echo MPDversion(); ?></span></p>

            </div>

        </div>

    </div>
</footer>

</div>


<a class="exit-off-canvas"></a>

</div>
</div>
</div>
