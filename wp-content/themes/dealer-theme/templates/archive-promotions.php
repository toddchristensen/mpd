<?php $show_title_bar = get_field('show_title_bar');

          date_default_timezone_set('America/New_York');
          echo mainAspot();
?>
       <?php if(!empty($show_title_bar)){?>
        <div class="title-bar" style="max-width:960px; margin:0 auto;">
            <h2 class="title page-title" style="margin:0!important; margin-top:.25em!important; float:none; text-align:center;"><?php echo the_title(); ?></h2>

                          <div class="breadcrumbs" style="margin-bottom:.5em;">
                            <div class="row" style="text-align:center;">
                              <?php //BREADCRUMBS
                              if ( function_exists('yoast_breadcrumb') ) {
                                yoast_breadcrumb('','');
                              }
                              ?>
                            </div>
                          </div>

            <div class="clearfix"></div>
        </div>
        <?php } ?>

        <?php
        //GET PROMOTIONS CUSTOM POST TYPE AND DISPLAY CONTENT IF DATES ARE CORRECT
        $today = date("Ymd");

        $promo_tags = get_field("promo_tags");

        $activePromosArgs = array(
          'post_type'             => 'promotion',
          //'post_status'           => 'publish',
          'posts_per_page'        => -1,
          'tag'                   => $promo_tags,
        //  'meta_query'     => array(
        //    array(
        //      'key'     => 'promo_start_date',
        //      'compare' =>  '<=',
        //      'value'   =>  $today
        //    ),
        //    array(
        //      'key'     => 'promo_end_date',
        //      'compare' =>  '>=',
        //      'value'   =>  $today
        //    )
        //  )

        );


$activePromos = new WP_Query( $activePromosArgs );

if($activePromos->have_posts()){
?>
    <div class="the-cl promotions-container cf">
    <?php

      while($activePromos->have_posts()){
      $activePromos->the_post();

      $startdate = get_field('promo_start_date');
      $year = substr($startdate, 0, -4);
      $month = substr($startdate, 4, -2);
      $day = substr($startdate, -2);

      $enddate = get_field('promo_end_date');

      $today = date(Ymd);

      $stillFresh = 'null';

      if($startdate && $enddate) {

        // Checks freshness of Promotion
        if (($startdate < $today) && $enddate > $today ) {
          $stillFresh = true;
        } else {
          $stillFresh = false;
        }
      }


      $startdate = $month . "/" . $day . "/" . $year;



      $year = substr($enddate, 0, -4);
      $month = substr($enddate, 4, -2);
      $day = substr($enddate, -2);

      $enddate = $month . "/" . $day . "/" . $year;



      if($stillFresh && $enddate) { ?>


        <div class="vl-item promotion cf imgp_<?php echo get_field('promo_image_position') ?>">
            <div class="fifty-50 the-image">
              <a href="<?php the_permalink(); ?>">
                  <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                  <img src="<?php echo $url; ?>" alt="<?php the_title(); ?>" />
              </a>
            </div>

            <div class="fifty-50 the-content">
               <a href="<?php the_permalink(); ?>">
                <h3> <?php the_title(); ?> </h3>
                </a>
                <?php


                  $hideDates = get_field('hide_promo_dates');

                  if(empty($hideDates)){
                ?>
                <a href="<?php the_permalink(); ?>">
                    <p><?php echo $startdate ?>&nbsp; - &nbsp;<?php echo $enddate ?></p>
                </a>

            <? } ?>
            <?php
            $promoDescript = get_field('promo_short_description');
            if(!empty ($promoDescript)) {
              echo $promoDescript;
            }else {
              the_content();
            } ?>

            </div>
        </div>


  <?php  } ?>


      <?php } ?>
      </div>
<?php
}
?>
