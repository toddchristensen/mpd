<?php //the_content(); ?>
<?php //wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>

<?php
/*
 *
 */
global $post;
  $page_parent_slug = '';
  $post_data = get_post($post->post_parent);
  $page_parent_slug = $post_data->post_name;
  $brand_class = json_encode($page_parent_slug);

?>
<script type="text/javascript">
    jQuery(function ($) {
     var body_class = <?php echo $brand_class; ?>;
     $('body').addClass(body_class);
    });
</script>

<!-- ASPOT BANNER SECTION -->

<?php echo mainAspot(); ?>

<!-- Anchor Link Bar ---->
<?php
if (get_field('show-sub-nav')) :
?>
<div class="anchor-links-bar">
    <div class="anchor-links-wrapper">
        <?php
            $model = get_field('models-header');
            $model = str_ireplace('HOT SPRING<r></r>', '', $model);
            $model = str_ireplace('CALDERA<r></r>', '', $model);
            $model = str_ireplace('FANTASY<r></r>', '', $model);
            $model = str_ireplace('FREEFLOW<r></r>', '', $model);
            $model = str_ireplace('<r></r>', '', $model);

            $features = get_field('all_features');

            $design = get_field('design-header');
            $features = get_field('features-header');
            $explore = get_field('explore-header');
            $pricing = get_field('form-header');
            $contact = get_field('contact-header');
            $reviews = get_field('show-reviews');

            if (!empty($model)) :
                echo '<a class="anchor-link-top smooth-scroll" href="#models">'.$model.'</a>';
            endif;
            if (!empty($design)) :
                echo '<a class="anchor-link-top smooth-scroll" href="#design">'.$design.'</a>';
            endif;
            if (!empty($features)) :
                echo '<a class="anchor-link-top smooth-scroll" href="#features">'.$features.'</a>';
            endif;
            if($features ):
                echo '<a class="anchor-link-top smooth-scroll" href="#explore">'.$explore.'</a>';
            endif;
            if (!empty($reviews)) :
                echo '<a class="anchor-link-top smooth-scroll" href="#reviews">REVIEWS</a>';
            endif;
            if (!empty($contact)) :
                echo '<a class="anchor-link-top smooth-scroll" href="#pricing">CONTACT US</a>';
            endif;
        ?>
    </div>
</div>

<div class="anchor-links-bar-static">
    <div class="anchor-links-wrapper">
        <?php
            $model = get_field('models-header');
            $model = str_ireplace('HOT SPRING<r></r>', '', $model);
            $model = str_ireplace('CALDERA<r></r>', '', $model);
            $model = str_ireplace('FANTASY<r></r>', '', $model);
            $model = str_ireplace('FREEFLOW<r></r>', '', $model);
            $model = str_ireplace('<r></r>', '', $model);

            $design = get_field('design-header');
            $features = get_field('features-header');
            $explore = get_field('explore-header');
            $pricing = get_field('form-header');
            $contact = get_field('contact-header');
            $reviews = get_field('show-reviews');

            if (!empty($model)) :
                echo '<a class="anchor-link-top smooth-scroll" href="#models">'.$model.'</a>';
            endif;
            if (!empty($design)) :
                echo '<a class="anchor-link-top smooth-scroll" href="#design">'.$design.'</a>';
            endif;
            if (!empty($features)) :
                echo '<a class="anchor-link-top smooth-scroll" href="#features">'.$features.'</a>';
            endif;
            if( $features ):
                echo '<a class="anchor-link-top smooth-scroll" href="#explore">'.$explore.'</a>';
            endif;
            if (!empty($reviews)) :
                echo '<a class="anchor-link-top smooth-scroll" href="#reviews">REVIEWS</a>';
            endif;
            if (!empty($contact)) :
                echo '<a class="anchor-link-top smooth-scroll" href="#contact">CONTACT US</a>';
            endif;

        ?>
    </div>
</div>
<?php else : //add shadow if the nav bar is not being shown?>
    <style>.product-models { box-shadow: inset 0px 2px 3px rgba(0,0,0,0.25); }</style>
<?php endif; ?>
<!---------------------------- MODELS SECTION ---------------------------------------->

<!-- WIZIWIG CONTENT BOX SECTION -->
<div class="top-body-content">
  <div class="content-container" style="max-width:<?php the_field('container_width'); ?>px; width: 100%; margin: 0px auto; padding-top:<? the_field('container_padding_top'); ?>px; padding-bottom:<? the_field('container_padding_bottom'); ?>px;">
<?php  echo the_content(); ?>
  </div>
</div>


<!--PRODUCT SLIDER -->
<?php
//if (!empty(get_field('show-product-slider') ) ) :
$num_models = get_field('number-models');
$current_category = get_field('product-category');
$cat_term = get_term($current_category, 'product_cat');
if ( !empty(get_field('show-the-models'))  ):
?>
<a id="models"></a>
<div class="product-carousel">
 <div class="row">
       <!-- <h2 class="title"><?php /*the_field('models-header');*/  echo get_the_title();?></h2>
       <div class="breadcrumbs">
           <div class="row">
              <?php //BREADCRUMBS
               if ( function_exists('yoast_breadcrumb') ) {
                   yoast_breadcrumb('','');
               }
               ?>
            </div>
         </div> --> 
        <div class="hottubGrid">
    
                <?php

                            $args = array(
                            'post_type'     => 'product',
                            'product_cat'   => $cat_term->slug,
                            'posts_per_page' => -1,
                            'meta_key'              => 'global_product_order',
                            'orderby' 				=> 'meta_value_num',
                            'order' 				=> 'DESC',
                                );
                            $query = new WP_Query( $args );
                            // The Loop
                            if ( $query->have_posts() ) {
                                while ( $query->have_posts() ) {
                                    $query->the_post();
                                    $product_name = get_the_title();
                                    $collection_name = get_field('product-collection');
                                    $top_image = get_field('top_down_image_thumbnail_url');
                                    $product_cap = get_field('product_cap');
                                    $link = get_permalink($query->post->ID);
                                    $width = get_field('width');
                                    $length = get_field('length');
                                    $height = get_field('height');
                                    //$dim = $width. ' x ' .$length. ' x ' .$height;
                                    $dim = $width. ' x ' .$length;
                                    $vol = get_field('product_volume');
                                    $jet_count = get_field('jet_count');
                                    $dim_toggle = 0;
                                        if ( (!empty($width)) && (!empty($length)) && (!empty($height))  ) { $dim_toggle = 1; }
                                    $feat_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full');
                                    if ((!empty($feat_img[0])) || (!empty($top_image)) ) :
                                        echo '<div class="product-slider-box columns small-12 medium-6 large-4">';
                                            echo '<a class="product-loop-link" href="'.$link.'">';
                                                if(!empty($top_image) ): echo '<img src="'.$top_image.'" />';
                                                else :
                                                    //$feat_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full');
                                                    if (!empty($feat_img[0])) :
                                                        echo '<img src="'.$feat_img[0].'" />';
                                                    else : echo '<p style="text-align:center;">NO IMAGE</p>';
                                                    endif;
                                                endif;
                                                echo '<p class="product-model-slide-title"><strong>'.$product_name.'</strong></p>';
                                                echo '<p class="product-capacity">'.$product_cap.' Person Spa</p>';
                                                if ( $dim_toggle == 1 ) {
                                                if(!empty($jet_count)){
                                                    echo '<p>'.$dim.'&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;'.$jet_count.' Jets </p>';
                                                }
                                                else {
                                                    echo '<p>'.$dim.'</p>';
                                                }
                                            }
                                                echo '<div style="clear:both;"></div>';
                                            echo '</a>';
                                        echo '</div>';
                                    else : continue;
                                    endif;
                                }
                            } else {
                                echo " ";
                            }
                        wp_reset_postdata();
                    ?>

    
        </div>
    </div>
</div>
<?php
else:
endif;
?>

<!------------------------- end models section -------------------------------------->
<style>
div.aspot-content h2 {
    font-size:3em !important;
}
div.aspot-content h3 {
    font-size:22px !important;
}
.hottubGrid {
    width:100%;
    max-width:1400px;
    margin:0 auto;
}
.collection-reviews {
    background:#101111 !important;
    color: #ffffff;
}
.feature-media iframe {
    max-width:505px !important;
}
.prod-description .content a {
    color:blue !important;
}
.product-slider-box:nth-child(4n), .product-slider-box:nth-child(7n) {
    clear:both;
}

</style>
<!------------------------------ GALLERY SECTION -------------------------------->
<?php
if( get_field('show-gallery') ) {
?>
<div class="collection-gallery">
    <div class="row">
          <div class="photo-section">
            <div class="row">
                <h2><?php the_field('gallery-header'); ?></h2>
                <!--<div class="large-2 small-12 columns">&nbsp;</div>-->
                <div class="large-12 medium-12 small-12 xsmall-12 columns">
                    <div class="slider">
                          <?php
                            // check if the repeater field has rows of data
                            if( have_rows('gallery') ):
                                // loop through the rows of data
                                while ( have_rows('gallery') ) : the_row();
                                    $image = get_sub_field('image');
                                    $video = get_sub_field('video');
                                    if (!empty($image)) :
                                        echo '<div class="gallery-side"><img src="'.$image.'" /></div>';
                                    elseif (!empty($video)) :
                                        echo '<div class="gallery-side">'.$video.'</div>';
                                    endif;
                                endwhile;
                            else :
                                // no rows found
                            endif;
                            ?>
                    </div>
              </div>
              <!--<div class="large-2 small-12 columns">&nbsp;</div>-->
           </div>
        </div>
    </div>
</div>
<?php } else { echo " "; } ?>
<!---------------------------end GALLERY SECTION -------------------------------->

<!------------------------------ REVIEWS SECTION -------------------------------->
<?php
if( get_field('show-reviews') ) {
$selected_reviews = get_field('select_reviews', false, false);
?>
<a id="reviews" class="cat-anchor"></a> <!-- ANCHOR LINK -->
<div class="collection-reviews test">
    <div class="row">
      <div class="quote-section">
        <div class="row">

            <?php

            //get image variables
            $left_quote = '//watkinsdealer.s3.amazonaws.com/images/sitewide/left-quote.png';
            $right_quote = '//watkinsdealer.s3.amazonaws.com/images/sitewide/right-quote.png';
            $right_quote_white = '//watkinsdealer.s3.amazonaws.com/images/sitewide/right-quote-white1.png';
            $left_quote_white = '//watkinsdealer.s3.amazonaws.com/images/sitewide/left-quote-white1.png';
            ?>
            <div class="large-12 xsmall-12 columns mobile-quotes"><img src="//watkinsdealer.s3.amazonaws.com/images/sitewide/commas-174x54.png" alt="Comas Icon" /></div>
            <div class="large-2 medium-2 small-1 xsmall-1 columns"><img src="<?php echo $left_quote_white; ?>" class="right" style="margin-top: 130px;" alt="Left Quote Icon" /></div>
            <div class="large-8 medium-8 small-12 xsmall-12 columns">
                <div class="slider">

                <?php
                $current_category = get_field('review_cat');
                $term = get_term($current_category, 'reviews_cat');


                    $args = array(
                        'post_type'     => 'reviews',
                        //'orderby'       => 'count',
                        'tax_query'     => array(
                            array(
                            'taxonomy'  =>  'reviews_cat',
                            'field'     =>  'slug',
                            'terms'     =>  $term->slug,
                            ),
                        ),
                        'post__in'          => $selected_reviews,
                        'posts_per_page' => 5
                            );
                        $query = new WP_Query( $args );

                        // The Loop
                        if ( $query->have_posts() ) {

                            while ( $query->have_posts() ) {
                                $query->the_post();
                                $review_title = get_the_title();
                                $quote = custom_field_excerpt();
                                $owner = get_field('review-owner');
                                $location = get_field('review-location');
                                $raw_score = get_field('review-score');
                                $score = round($raw_score);
                                $score_img = '//watkinsdealer.s3.amazonaws.com/images/sitewide/'.$score.'_star_rating.png';



                                echo '<div>';
                                echo '';
                                if (!empty($raw_score)){
                                echo '<img src="'.$score_img.'" alt="'.$raw_score.' Review Score Icon" class="review-score-img" />';
                                }
                                echo '<blockquote class="review-title">'.$review_title.'</blockquote>';
                                echo '<blockquote>'. $quote .'</blockquote>';
                                if( (!empty($owner)) && (!empty($location)) ){
                                echo '<p class="quote-author">'. $owner .' - '.$location.'</p>';
                                }
                                echo '</div>';
                            }
                        } else {
                            echo " ";
                        }
                    wp_reset_postdata();
                ?>
                </div>
          </div>
          <div class="large-2 medium-2 small-1 xsmall-1 columns"><img src="<?php echo $right_quote_white; ?>" alt="Right Quote Icon" class="left" style="margin-top: 130px;" /></div>
       </div>
        </div>
    </div>
</div>
<?php } else { echo " "; } ?>





<!---------------------------end REVIEWS SECTION -------------------------------->

<!------------------------- EXCEPTIONAL DESIGN SECTION ------------------------------->
<?php $section_filled = get_field('show-exceptional-features');
if (!empty($section_filled)) :
?>
<a id="design" class="cat-anchor"></a> <!-- ANCHOR LINK -->
<div class="jet-system cat-page">
   <div class="row">
     <h2 class="title"><?php the_field("design-header"); ?></h2>
     <p class="description"><?php the_field("design-sub-header"); ?></p>
     <div class="large-7 xsmall-12 columns">
       <img id="design-main-img" src="<?php the_field("design-main-image"); ?>" />
       <div class="plus-container">
       </div> <!--  /.plus-container-->
     </div> <!--  /.large-6.columns-->
     <div class="large-5 xsmall-12 columns exceptional-features-wrapper">
       <div class="tabs">
         <ul>
         <?php
            $posts = get_field('select-exceptional-features');
            foreach($posts as $post) {
                setup_postdata($post);

                                            $name = get_field('excep-feat-name');
                                            $desc = get_field('feature-body');
                                            $image = get_field('feature-image');
                                            // display a sub field value
                                            echo '<li><a href="#tabs-'.$count.'">'.$name.'</a></li>';
                                            echo '<div id="tabs-'.$count.'" class="tab-content"><img class="acc-image" src="'.$image.'" /><div class="acc-content">'.$desc.'</div></div>';
                                            echo '<div style="clear:both;"></div>';
                                            $count ++;

            }
                            wp_reset_postdata();


        ?>
         </ul>

       </div> <!--  /.tabs-->
     </div> <!--  /.large-6.columns-->
   </div> <!--  /.row-->
</div> <!--  /.jet-system-->
<?php
else :
endif;
?>
<!---------------------------- end design section ----------------------------------->

<!--------------------------- EXCLUSIVE FEATURES SECTION ----------------------------->
<?php
if (!empty(get_field('show-exclusive-features'))) :
?>
<?php
    $exFeatImg1 = get_field('show_exclusive_features_img_1');
    $exFeatTitle1 = get_field('show_exclusive_features_1_title');
    $exFeatText1 = get_field('show_exclusive_features_1_text');

    $exFeatImg2 = get_field('show_exclusive_features_img_2');
    $exFeatTitle2 = get_field('show_exclusive_features_2_title');
    $exFeatText2 = get_field('show_exclusive_features_2_text');

    $exFeatImg3 = get_field('show_exclusive_features_img_3');
    $exFeatTitle3 = get_field('show_exclusive_features_3_title');
    $exFeatText3 = get_field('show_exclusive_features_3_text');

    $exFeatImg4 = get_field('show_exclusive_features_img_4');
    $exFeatTitle4 = get_field('show_exclusive_features_4_title');
    $exFeatText4 = get_field('show_exclusive_features_4_text');
?>
<a id="features" class="cat-anchor"></a>
<div class="exclusive-features">
  <div class="row">
    <h2 class="title"><?php the_field("features-header"); ?></h2>
    <p class="description"><?php the_field("features-sub-header"); ?></p>

    <?php if ( !empty($exFeatImg1) ){ ?>
        <div class="feature-block large-3 medium-6 small-12 columns">
            <img class="feature-block-img" src="<?php echo $exFeatImg1 ?>" />
            <div class="feature-name"><?php echo $exFeatTitle1 ?></div>
            <div class="feature-desc"><?php echo $exFeatText1 ?></div>
        </div>
    <?php } ?>

    <?php if ( !empty($exFeatImg2) ){ ?>
        <div class="feature-block large-3 medium-6 small-12 columns">
            <img class="feature-block-img" src="<?php echo $exFeatImg2 ?>" />
            <div class="feature-name"><?php echo $exFeatTitle2 ?></div>
            <div class="feature-desc"><?php echo $exFeatText2 ?></div>
        </div>
    <?php } ?>

    <?php if ( !empty($exFeatImg3) ){ ?>
        <div class="feature-block large-3 medium-6 small-12 columns">
            <img class="feature-block-img" src="<?php echo $exFeatImg3 ?>" />
            <div class="feature-name"><?php echo $exFeatTitle3 ?></div>
            <div class="feature-desc"><?php echo $exFeatText3 ?></div>
        </div>
    <?php } ?>

    <?php if ( !empty($exFeatImg4) ){ ?>
        <div class="feature-block large-3 medium-6 small-12 columns">
            <img class="feature-block-img" src="<?php echo $exFeatImg4 ?>" />
            <div class="feature-name"><?php echo $exFeatTitle4 ?></div>
            <div class="feature-desc"><?php echo $exFeatText4 ?></div>
        </div>
    <?php } ?>

    </div>
</div>
<?php
else :
endif;
?>

<!----------------------- end ex features section --------------------------------------->

<!---------------------------GCB SECTION -------------------------------->


            <?php
// Retrieve General Content Rows
$general_rows = get_field('general_content_block_selector');

$general_rows_orders = array();

// Place General Content Row orders into array
$num_general_rows = 0;

if($general_rows){

    foreach($general_rows as $row){

        $general_rows_orders[$num_general_rows] = $row['global_page_order'];

        $num_general_rows++;
    }
}

// Add main section orders to single array - $sections_array
$sections_array = array();

// Add General Content Row orders to array - $sections_array
if(!empty($general_rows_orders)){

    foreach($general_rows_orders as $order){

        array_push($sections_array, $order);
    }
}

/*
 * Loop through the all sections in $sections_array.
 * Displays them according to their section orders.
 * Sections with equivalent orders will cause errors.
 */
$order_count = count($sections_array) + 1 ;
for($i=0; $i<10; $i++){
        $row_num = 0;
        if(!empty($general_rows)){
            foreach($general_rows as $row){
                if($row['global_page_order'] == $i){
                   set_query_var('row_num', $row_num);
                    /*----- GENERAL CONTENT ROW (GCR) SECTION -----*/
                    /*------- can display multiple GCRs -----------*/
                    get_template_part('/templates/blocks/blocks', 'general');
                }
                else {
                    $row_num++;
                }
            }
        }
}
            ?>
<!---------------------------end GCB SECTION -------------------------------->

<!---------------------- EXPLORE SECTION --------------------------------------------->
<?php if (!empty(get_field('show-features')) ) :
$posts = get_field('all_features');
$feature_count = count($posts);
if($feature_count <= 5){
    $large_grid_size = $feature_count;
    $medium_grid_size = $feature_count;
    $more_than_5 = '';
}
elseif( ($feature_count%3 == 0) && ($feature_count > 3) ){
    $large_grid_size = 3;
    $medium_grid_size = 2;
    $more_than_5 = 'more-than-5';
}
?>
<a id="explore" class="cat-anchor"></a> <!-- ANCHOR LINK -->
<div class="prod-description">
   <div class="row">
          <!-- SECTION TITLES -->
          <h2 class="title"><?php the_field("explore-header"); ?></h2>
    </div>
    <div class="tabs">
        <div class="row tab-nav">
        <ul class="product-desc-list large-12 medium-12 small-12 xsmall-12 columns large-block-grid-<?php echo $large_grid_size;?> medium-block-grid-<?php echo $medium_grid_size;?> small-block-grid-1">
        <?php
            $count = 0;
            $posts = get_field('all_features');
                        foreach ($posts as $post){
                            setup_postdata($post);
                            //Extract Feature Post Variables
                            $name = get_field('feature-name');
                            $link_name = str_ireplace(' ', '-', $name);
                            $link_name = str_ireplace('<r></r>', '', $link_name);

                            $tab_index = '';
                            //print
                            echo '<li tabindex="'.$count.'"><a href="#'.$link_name.'" tabindex="'.$count.'" class="tab-nav-link-'.$count.'" >'.$name.'</a></li>';
                            $count++;
                        }

                    wp_reset_postdata();

        echo '</ul>';
       echo '</div><!-- end row tab nav -->';

        $posts = get_field('all_features');
                        foreach ($posts as $post){
                            setup_postdata($post);
                            //Extract Feature Post Variables
                            $name = get_field('feature-name');
                            $block_name = str_ireplace(' ', '-', $name);
                            $block_name = str_ireplace('<r></r>', '', $block_name);

                            $header = get_field('feature-header');
                            $body = get_field('feature-body');
                            $video = get_field('feature-video');
                            $image = get_field('feature-single-image');
                            $format = get_field('feature-format');
                            $feature_title = get_the_title();
                            $feature = get_sub_field('feature');

                            $video1 = get_field('feature_video_1');
                            $video2 = get_field('feature_video_2');
                            //Display Tab Content
                                echo '<div id="'.$block_name.'" class="tab-content">';
                                    echo '<div class="row">';
                                       echo '<div class="large-5 medium-12 small-12 xsmall-12 columns image">';
                                            //IMAGE ONLY
                                            if ($format == 'Image') :
                                                echo '<div class="large-12  xsmall-12 feature-media  columns"><p><img src="'.$image.'" /></p></div>';
                                            //VIDEO ONLY
                                            elseif($format == 'Video' ) :
                                                echo '<div class="large-12  xsmall-12 feature-media  columns">'.$video.'</div>';
                                            //IMAGE SLIDER ONLY
                                            elseif($format == 'Image-Slider') :
                                                 echo '<div class="large-12  xsmall-12 feature-media  columns">';
                                                    echo '<div class="slider">';
                                                        // check if the repeater field has rows of data
                                                        if( have_rows('image-slider') ):
                                                            // loop through the rows of data
                                                            while ( have_rows('image-slider') ) : the_row();
                                                                $image_slide = get_sub_field('image');
                                                                if (!empty($image_slide)) :
                                                                    echo '<div class="gallery-side"><img src="'.$image_slide.'" /></div>';
                                                                else :
                                                                endif;
                                                            endwhile;
                                                        else :
                                                        endif;
                                                    echo '</div><!-- end slider-->';
                                                 echo '</div>';
                                            //IMAGE AND IMAGE SLIDER
                                            elseif($format == 'Image & Image-Slider') :
                                                echo '<div class="large-12  xsmall-12 feature-media  columns"><p><img src="'.$image.'" /></p></div>';
                                                echo '<div class="large-12  xsmall-12 feature-media  columns">';
                                                        // check if the repeater field has rows of data
                                                        if( have_rows('image-slider') ):
                                                echo '<div class="slider2">';
                                                            // loop through the rows of data
                                                            while ( have_rows('image-slider') ) : the_row();
                                                                $image_slide = get_sub_field('image');
                                                                if (!empty($image_slide)) :
                                                                    echo '<div class="gallery-side"><img src="'.$image_slide.'" /></div>';
                                                                else :
                                                                endif;
                                                            endwhile;
                            echo '</div><!-- end slider-->';
                                                        else :


                                                        endif;
                                                 echo '</div>';
                                            //IMAGE AND VIDEO
                                            elseif($format == 'Image & Video') :
                                                echo '<div class="large-12 xsmall-12 feature-media  columns">'.$video.'</div>';
                                                echo '<div class="large-12 xsmall-12 feature-media  columns"><p><img src="'.$image.'" /></p></div>';
                                            //IMAGE SLIDER AND VIDEO
                                            elseif($format == 'Image-Slider & Video') :
                                                echo '<div class="large-12 xsmall-12 feature-media  columns">'.$video.'</div>';
                                                echo '<div class="large-12 xsmall-12 feature-media  columns">';
                                                    echo '<div class="slider">';
                                                        // check if the repeater field has rows of data
                                                        if( have_rows('image-slider') ):
                                                            // loop through the rows of data
                                                            while ( have_rows('image-slider') ) : the_row();
                                                                $image_slide = get_sub_field('image');
                                                                if (!empty($image_slide)) :
                                                                    echo '<div class="gallery-side"><img src="'.$image_slide.'" /></div>';
                                                                else :
                                                                endif;
                                                            endwhile;
                                                        else :
                                                        endif;
                                                    echo '</div><!-- end slider-->';
                                                 echo '</div>';
                                            //DOUBLE VIDEO
                                            elseif($format == 'Double Video'):
                                                echo '<div class="large-12 xsmall-12 columns">'.$video1.'</div>';
                                                echo '<div class="large-12 xsmall-12 columns">'.$video2.'</div>';
                                            endif;
                                       echo '</div>';
                                       echo '<div class="large-7  xsmall-12 columns content">';
                                            /*echo '<h4 class="title">'.$header.'</h4>';*/
                                            echo '<p>'.$body.'</p>';
                                       echo '</div>';
                                       echo '<div class="clearfix"></div>';
                                    echo '</div>';
                                echo '</div><!-- /.tab-content-->';
                        }

                    wp_reset_postdata();

        ?>
    </div><!-- end tabs -->
</div><!-- /.prod-description -->
<?php endif; ?>
<!-------------------------- end explore section ---------------------------->
