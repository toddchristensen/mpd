<?php 

$theContent = get_field('aspot_custom_html');
$aspotHeight = get_field('aspot_height');


?>


<section class="custom-aspot the-aspot" style="height: <?php if( $aspotHeight ) { echo $aspotHeight . 'px'; } else { echo '91vh'; } ?>;">
    <?php echo $theContent ?>
</section>


<!--
<section class="custom-aspot the-aspot">
<div class="intro-text" style="background-image:url('http://ihearthottubs2.wpengine.com/wp-content/uploads/2016/02/crosshatch.jpg');">
    <div class="container">
        <h3>So quick and easy it'll make <span>your head swim. Literally.</span></h3>
    </div>
</div>
<div class="hot tub" style="background-image:url('http://ihearthottubs2.wpengine.com/wp-content/uploads/2016/02/page_bg.png');">
    <div class="thepattern" style="background-image:url('http://ihearthottubs2.wpengine.com/wp-content/uploads/2016/02/crosshatch.jpg');"></div>
    <img class="the-tub" src="http://ihearthottubs2.wpengine.com/wp-content/uploads/2016/02/image.png" alt="hot tub image">
    <a href=""><img src="http://ihearthottubs2.wpengine.com/wp-content/uploads/2016/02/circle.png" alt="Call us 281 531 8757"></a>
</div>
<div class="the-about" style="background-image:url('http://ihearthottubs2.wpengine.com/wp-content/uploads/2016/02/page_bg.png');">
    <div class="container cf">
        <div class="the-text">
           <p class="mobile-hide">See what we mean ► <a href="">Our world-famous super simple installation guide</a></p>
            <p>Looking for a way to get that dream spa without breaking the bank? We offer a few options to help you relax. Literally.</p>
            <p>At I Heart Hot Tubs, we believe that your spa purchasing, delivery, installation and maintenance experiences should be simple, seamless, and most importantly, FUN! Don’t let our lighthearted attitude fool you. We take your satisfaction seriously. In fact, we guarantee it. Come by the showroom for floor-model specials and sales. We carry Dimension One, Caldera, La-Z-Boy, and Sunbelt Spas! We offer free delivery AND installation in Houston, TX and surrounding areas.</p>
        </div>
    </div>
</div>
</section>
-->
