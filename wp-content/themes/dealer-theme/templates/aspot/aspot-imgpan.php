<?php
    $aspotLink1 = get_field('aspot_link_1');
    $aspotLink1Text = get_field('apost_link_1_text');
    $aspotLink2 = get_field('aspot_link_2');
    $aspotLink2Text = get_field('aspot_link_2_text');
    $aspotHeight = get_field('aspot_height');

    $images = get_field('aspot_img_pan_images');

    $overlayType = get_field('aspot_pan_background_overlay_type');

    if($overlayType == 'color'){
        $background = get_field('aspot_pan_background_color');
        $background = 'background-color:'.$background;
    }
    if($overlayType == 'image'){
        $background = get_field('aspot_pan_background_image');
        $background = 'background-image:url('.$background.')';
    }

    $backgroundOpacity = get_field('aspot_pan_background_opacity');
    $backgroundOpacity = 'opacity:'.$backgroundOpacity;

    $theText = get_field('aspot_pan_overlay_text');
    $textColor = get_field('aspot_pan_text_color');

    $textPos = get_field('aspot_pan_text_position');
    $textAlign = get_field('aspot_pan_text_align');
?>
<section class="aspot img-pan" style="height: <?php if( $aspotHeight ) { echo $aspotHeight . 'vh'; } else { echo '91vh'; } ?>;">

<?php if( $images ): ?>

<?php foreach( $images as $image ): ?>

    <div class="pan-item" style="background-image:url('<?php echo $image['url']; ?>'); height: <?php if( $aspotHeight ) { echo $aspotHeight . 'vh'; } else { echo '91vh'; } ?>;"></div>

<?php endforeach; ?>

<?php endif; ?>

    <div class="overlay" style="<?php echo $background ?>; <?php echo $backgroundOpacity ?>"></div>
    <div class="container">
        <div class="the-content pos-<?php echo $textPos ?> align-<?php echo $textAlign ?>">
            <h3 style="color:<?php echo $textColor ?>"><?php echo $theText?></h3>

            <?php if(!empty($aspotLink1Text)){ ?>

            <div class="the-links block-menu">

              <?php if(empty($aspotLink2Text)){ $centerLink1 = 'vertical-center'; } ?>

              <a class="<?php echo $centerLink1 ?>" href="<?php echo $aspotLink1?>"><?php echo $aspotLink1Text ?></a>

              <?php if(!empty($aspotLink2Text)){ ?>

              <a href="<?php echo $aspotLink2 ?>"><?php echo $aspotLink2Text ?></a>

              <?php } ?>

            </div>
        <?php } ?>
      </div>
    </div>

</section>
