<style>
@supports (-ms-ime-align: auto) {
  .video-header video {
        height: initial !important;
  }
}
</style>
<?php
function isMobile() {
    return preg_match( "/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"] );
}

$aspotText      = get_field( 'aspot_video_text' );
$aspotLink1     = get_field( 'aspot_link_1' );
$aspotLink1Text = get_field( 'apost_link_1_text' );
$aspotLink2     = get_field( 'aspot_link_2' );
$aspotLink2Text = get_field( 'aspot_link_2_text' );
$aspotHeight    = get_field( 'aspot_height' );
$pageDownArrow  = get_field( 'page_down_arrow' );
$filterOpacity  = get_field( 'video_filter_opacity' );

// Use the function
if ( isMobile() ) {

    $post_object = get_field( 'select_aspot_video' );

    if ( $post_object ) {

// override $post
        $post = $post_object;
        setup_postdata( $post );
        ?>

        <section class="video-header" style="background-image:url('<?php echo the_field( 'aspot-mobile_pic' ); ?>');">
            <div class="overlay">
                <div class="container">
                    <div class="the-content">
                        <h3 class="withLinks"><?php echo $aspotText ?></h3>

                        <?php if ( ! empty( $aspotLink1Text ) ) { ?>

                            <div class="the-links block-menu">

                                <?php if ( empty( $aspotLink2Text ) ) {
                                    $centerLink1 = 'vertical-center';
                                } ?>

                                <a class="<?php echo $centerLink1 ?>"
                                   href="<?php echo $aspotLink1 ?>"><?php echo $aspotLink1Text ?></a>

                                <?php if ( ! empty( $aspotLink2Text ) ) { ?>

                                    <a href="<?php echo $aspotLink2 ?>"><?php echo $aspotLink2Text ?></a>

                                <?php } ?>

                            </div>

                        <?php } ?>
                    </div>
                </div>
        </section>

        <?php
        wp_reset_postdata();
    }
} else {

    $post_object = get_field( 'select_aspot_video' );

    if ( $post_object ) {

// override $post
        $post = $post_object;
        setup_postdata( $post );
        ?>

        <section class="video-header"
                 style="background-image:url('<?php echo the_field( 'aspot-mobile_pic' ); ?>'); height: <?php if ( $aspotHeight ) {
                     echo $aspotHeight . 'vh';
                 } else {
                     echo '91vh';
                 } ?>;">
            <div class="overlay">
                <div class="container">
                    <div class="the-content">
                        <h3 class="withLinks"><?php echo $aspotText ?></h3>

                        <?php if ( ! empty( $aspotLink1Text ) ) { ?>

                            <div class="the-links block-menu">

                                <?php if ( empty( $aspotLink2Text ) ) {
                                    $centerLink1 = 'vertical-center';
                                } ?>

                                <a class="<?php echo $centerLink1 ?>"
                                   href="<?php echo $aspotLink1 ?>"><?php echo $aspotLink1Text ?></a>

                                <?php if ( ! empty( $aspotLink2Text ) ) { ?>

                                    <a href="<?php echo $aspotLink2 ?>"><?php echo $aspotLink2Text ?></a>

                                <?php } ?>

                            </div>

                        <?php } ?>

                    </div>

                </div>

            </div>

            <video class="video-bg" autoplay loop muted width="100%" style="height: <?php if($aspotHeight) { echo $aspotHeight . 'vh'; } else { echo '91vh'; } ?>">
                <source src="<?php echo the_field( 'aspot-webm' ) ?>" type="video/webm" codecs="vp8.0, vorbis"/>
                <source src="<?php echo the_field( 'aspot-mp4' ) ?>" type="video/mp4"/>
                Your browser does not support the video tag. We suggest you upgrade your browser.
            </video>
            <div class="video-filter" style="opacity:<?php echo $filterOpacity ?>;"></div>
        </section>

        <?php
        wp_reset_postdata();
    }
}

?>

<?php
if ( ! empty( $pageDownArrow ) ) { ?>
    <div class="aspot_anchor">
        <li class="active"><a id="link1" class="nav-section1" href="#section1"><span class="bottom"></span></a></li>
    </div>
<?php }
