<?php
/**
 * Template Name: Blog Template
 */
?>



<?php while (have_posts()) : the_post(); ?>

  <?php get_template_part('templates/content', 'blog'); ?>
<?php endwhile; ?>


<?php if (!have_posts()) : ?>

   <?php get_template_part('templates/page', 'header'); ?>

  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>


<?php the_posts_navigation(); ?>
