<?php
/**
 * Template Name: Hot Tub Product
 */
?>

<?php while (have_posts()) : the_post(); ?>



<div>
   <?php echo do_shortcode('[products ids="1001257"]');?>
   <?php echo do_shortcode('[products skus="foo, bar, baz" orderby="date" order="desc"]');?>
   <?php echo do_shortcode('[add_to_cart id="1001257"]');?>
</div>


<?php endwhile; ?>
