<?php
    $header_img = get_field('blog_header_image', 'option');
    $header_subtitle = get_field('blog_sub_title', 'option');
    $header_overlay = get_field('blog_overlay_color', 'option');
    $overlay_opacity = get_field('blog_overlay_opacity', 'option');
    $blog_search = get_field('blog_search_field', 'option');

    $current_category=''; if (!is_home()) $current_category = get_the_category();

?>
<?php if(!empty($header_img)){ ?>
    <div class="hero" style="background-image:url('<?php echo $header_img ?>');">
       <?php //if( !empty($header_overlay) ){ ?>
            <div class="overlay" style="background-color:<?php echo $header_overlay ?>; opacity:.<?php echo $overlay_opacity ?>;"></div>
        <?php //} ?>
        <!--<img src="<?php echo $header_img ?>" alt="">-->
        <div class="the-content">

            <h2><?php if (!empty($current_category)) echo $current_category[0]->name; else echo 'Categories'; ?></h2>

            <?php if( !empty($header_subtitle) ){ ?>
                  <h3><?php echo single_cat_title(); ?></h3>
            <?php } ?>
            <?php if( !empty($blog_search) ){ ?>
                <!-- Search Blog posts -->

                <form role="search" class="blog-search" method="get" action="<?= esc_url(home_url('/')); ?>">

                <input type="search" size="16" value="" name="s" class="search-field form-control" placeholder="Search Blog Posts" required>

                <input type="hidden" name="post_type" value="blog-posts" /> <!-- // hidden 'blog-posts' value -->

                </form>

            <? } ?>


            <div class="dropdown">
                <a href="javascript:;" class="dropdown">Categories</a>
                <ul>
            <?php
                $args = array(
                //'show_option_all'    => '',
                'orderby'            => 'name',
                'order'              => 'ASC',
                'style'              => 'list',
                //'show_count'         => 0,
                'hide_empty'         => 1,
                'use_desc_for_title' => 1,
                //'child_of'           => 0,
                //'feed'               => '',
                //'feed_type'          => '',
                //'feed_image'         => '',
                //'exclude'            => '',
                //'exclude_tree'       => '',
                //'include'            => '',
                'hierarchical'       => 1,
                //'title_li'           => __( 'Categories' ),
                'show_option_none'   => __( '' ),
                'number'             => null,
                'echo'               => 1,
                //'depth'              => 0,
                //'current_category'   => 0,
                //'pad_counts'         => 0,
                'taxonomy'           => 'category',
                'walker'             => null
                );
                wp_list_categories('title_li=0');
            ?>
            </ul>
        </div>

        <!-- Date Filtered dropdown -->
        <div class="dropdown">
            <a href="javascript:;" class="dropdown">Date</a>
            <ul>

            <?php
                $args = array(
                //'show_option_all'    => '',
                'orderby'            => 'name',
                'order'              => 'DESC',
                'style'              => 'list',
                //'show_count'         => 0,
                'hide_empty'         => 1,
                'use_desc_for_title' => 1,
                //'child_of'           => 0,
                //'feed'               => '',
                //'feed_type'          => '',
                //'feed_image'         => '',
                //'exclude'            => '',
                //'exclude_tree'       => '',
                //'include'            => '',
                'hierarchical'       => 1,
                //'title_li'           => __( 'Categories' ),
                'show_option_none'   => __( '' ),
                'number'             => null,
                'echo'               => 1,
                //'depth'              => 0,
                //'current_category'   => 0,
                //'pad_counts'         => 0,
                'type'               => monthly,
                'walker'             => null
                );
                wp_get_archives('title_li=0');
            ?>
            </ul>
        </div>
                    <?//= custom_search_form( null, 'Search posts', 'post'); ?>

    </div>
</div>
<?php }

?>

<div class="the-posts <?php echo $blog_style ?>-grid masonry-grid" style="max-width:1200px; margin:0 auto; padding: 1em;">

<?php while (have_posts()) : the_post(); ?>

<article <?php post_class('masonry-item') ?>>
          <header>
            <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <?php get_template_part('templates/entry-meta'); ?>
          </header>
          <div class="entry-left-side large-12 small-12 xsmall-12">
              <?php
                $title = get_the_title();
               /*$featured_img = get_the_post_thumbnail();*/
               $featured_img = $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array(450, 250) );
                if($featured_img){
                    echo '<img src="'.$featured_img[0].'" alt="'.$title.'">';
                }
              ?>
          </div>
          <div class="entry-summary entry-right-side large-12 small-12 xsmall-12">
            <?php the_excerpt(); ?>
          </div>
          <div class="clear"></div>
        </article>

<?php endwhile; ?>


<?php if (!have_posts()) : ?>

   <?php get_template_part('templates/page', 'header'); ?>

  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<?php the_posts_navigation(); ?>
</div>
