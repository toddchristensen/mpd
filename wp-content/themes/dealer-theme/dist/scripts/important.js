//jQuery.noConflict();
jQuery(document).ready(function($) {



          // SUPERSCRIPT function

    // $("p,h1,h2,h3,h4,li").each(function(){
    $("p,h1,h2,h3,h4").each(function(){
        $(this).html($(this).html().replace(/&reg;/gi, '<sup class="registermark">&reg;</sup>').replace(/®/gi, '<sup class="registermark">&reg;   </sup>'));
        $(this).html($(this).html().replace(/&#8482;/gi, '<sup class="registermark">&#8482;</sup>').replace(/™/gi, '<sup class="registermark">&#8482;   </sup>'));
    });
  //
  //        $('body :not(script,sup)').contents().filter(function(){
  //            return this.nodeType === 3;
  //        }).replaceWith(function() {
  //            return this.nodeValue.replace(/[™®]/g, '<sup class="registermark">$&</sup>');
  //        });


  if($('.video-header').length !=0) {
    $('body').addClass('a-spot');
  }

  if($('.aspot').length !=0) {
    $('body').addClass('a-spot');
   }

  $('.block-menu').find('a').each(function(){

  var el = $(this),
       elText = el.text();

  el.addClass("three-d");
  el.append('<span aria-hidden="true" class="three-d-box"><span class="front">'+elText+'</span><span class="back">'+elText+'</span></span>');


  });


$('.review-expander').click(function(){ //On .review-expander link click

  if($(this).prev().hasClass('expander-active')){ //If the clicked link has the class to expand already

    var theParent = $(this).prev();
    theParent.removeClass('expander-active'); //Remove the expand class
    $(this).html('...Read More');

  }else{

    var theParent = $(this).prev();
    $('.review-expander').html('...Read More');
    $('.expander-active').removeClass('expander-active'); //If already has expander class, remove.
    theParent.addClass('expander-active'); //Add the expander class to the clicked link.
    $(this).html('Collapse');

  }

});

$('span.version').hide();

$('footer .copy-right').dblclick(function(){
   $('span.version').toggle();
});

$('#map').addClass('scrolloff');                // set the mouse events to none when doc is ready

$('#map-overlay').on("mouseup",function(){          // lock it when mouse up
    $('#map').addClass('scrolloff');
    //somehow the mouseup event doesn't get call...
});
$('#map-overlay').on("mousedown",function(){        // when mouse down, set the mouse events free
    $('#map').removeClass('scrolloff');
});

$("#map").mouseleave(function () {              // becuase the mouse up doesn't work...
    $('#map').addClass('scrolloff');            // set the pointer events to none when mouse leaves the map area
                                                // or you can do it on some other event
});

$('.expander-link').click(function(){
//    alert('hi');
    $(this).next().slideToggle();
});


/*var videoHeroHeight = $(window).height() - $('.the-header').height();
*///    alert($(window).outerHeight());
//    alert($('.the-header').outerHeight());
//    alert(videoHeroHeight);
/*$('section.video-header').css({height:videoHeroHeight});
$('section.img-pan').css({height:videoHeroHeight});*/

$('.ui-tabs-anchor').click(function(){
//            alert('hi');
    $('#slick-slide41 button').trigger('click');
    $('#slick-slide21 button').trigger('click');
});


// PAN AND SCAN

function randomClass(){
var classes = ['pos-top-left','pos-top-right', 'pos-bottom-right', 'pos-bottom-left']; //add as many classes as u want
var randomnumber = Math.floor(Math.random()*classes.length);
var randomClass = classes[randomnumber];
    return randomClass;
}

function nextImg(){
//    alert('next');
    var nextImg = $('.pan-item.current').next('.pan-item');
    var nextImgHeight = nextImg.height();
//    alert(nextImgHeight);
    if( nextImgHeight == null ){
        nextImg = $('.pan-item').first();
    }
    $('.pan-item.current').removeClass('current');
    $(".pan-item").removeClass (function (index, css) {
        return (css.match (/\bpos-\S+/g) || []).join(' ');
    });


}

var windowWidth = $(window).width();
if(windowWidth < 0){


  $('.pan-item').first().addClass('current');
  $('.pan-item').first().addClass(randomClass());
  var counter = 0;
  var interval = setInterval(function() {
      counter++;
      // Display 'counter' wherever you want to display it.
      if (counter == 10) {
          // Display a login box
  //        alert('10seconds');
          nextImg();
  //        randomClass();
          counter = 0;
      }
  }, 1000);

  nextImg.addClass('current');
  nextImg.addClass(randomClass());


} else {

  var duration = 60; // duration in seconds
  var fadeAmount = 0.3; // fade duration amount relative to the time the image is visible

  var images = $(".img-pan .pan-item");
  var numImages = images.size();
  var durationMs = duration * 1000;
  var imageTime = durationMs / numImages; // time the image is visible
  var fadeTime = imageTime * fadeAmount; // time for cross fading
  var visibleTime = imageTime  - (imageTime * fadeAmount * 2);// time the image is visible with opacity == 1
  var animDelay = visibleTime * (numImages - 1) + fadeTime * (numImages - 2); // animation delay/offset for a single image

  images.each( function( index, element ){
    if(index != 0){
      $(element).css("opacity","0");
      setTimeout(function(){
        doAnimationLoop(element,fadeTime, visibleTime, fadeTime, animDelay);
      },visibleTime*index + fadeTime*(index-1));
    }else{
      setTimeout(function(){
        $(element).animate({opacity:0},fadeTime, function(){
          setTimeout(function(){
            doAnimationLoop(element,fadeTime, visibleTime, fadeTime, animDelay);
          },animDelay )
        });
      },visibleTime);
    }
  });


  // creates a animation loop
  function doAnimationLoop(element, fadeInTime, visibleTime, fadeOutTime, pauseTime){
  fadeInOut(element,fadeInTime, visibleTime, fadeOutTime ,function(){
    setTimeout(function(){
      doAnimationLoop(element, fadeInTime, visibleTime, fadeOutTime, pauseTime);
    },pauseTime);
  });
  }

  // shorthand for in- and out-fading
  function fadeInOut( element, fadeIn, visible, fadeOut, onComplete){
  return $(element).animate( {opacity:1}, fadeIn ).delay( visible ).animate( {opacity:0}, fadeOut, onComplete);
  }

}




/**************************OLD CODE NOT BY ALEX */
    $("a#test-link-0").addClass("selected");

    $(".plus-icon").click(function() {
        //remove all selected styles from all icon
        $(".plus-icon").each(function() {
            $(this).removeClass('selected');
        });
        //reassign selected style to clicke dicon
        $(this).addClass('selected');
        //if icon id matches link id...
        //...CLICK IT!
        var current_id = $(this).attr('id');
        $(".jet-link").each(function() {
            var item_id = $(this).attr('id');
            if (current_id == item_id) {
                $(this).click();
            }
        });
    });
    $(".jet-link").click(function() {
        //remove all selected styles from all icon
        $(".plus-icon").each(function() {
            $(this).removeClass('selected');
        });
        //reassign selected style to clicke dicon
        //$(this).addClass('selected');
        //if icon id matches link id...
        //...CLICK IT!
        var current_id = $(this).attr('id');
        $(".plus-icon").each(function() {
            var item_id = $(this).attr('id');
            if (current_id == item_id) {
                $(this).addClass('selected');
            }
        });
    });
    $('.util-drop-down a').click(function() {
        $('div.util-nav').slideToggle('500');
        $('.contact-menus').toggleClass('height-adjust');
        $('.header').toggleClass('no-border');
    });
    $('#menu-item-1000018 a').click(function() {
        $('div.util-nav').slideToggle('500');
        $('.contact-menus').toggleClass('height-adjust');
        $('.header').toggleClass('no-border');
    });
    $(
        '.tab-nav-link-0, .tab-nav-link-1, .tab-nav-link-2, .tab-nav-link-3, .tab-nav-link-4'
    ).click(function() {
        if ($('.tab-content').css('display') == 'block') {
            $('.tab-content').addClass('active-tab');
        }
        $('.active-tab .sliderbutton').click();
    });

    $(".tabs").tabs({
        hide: {
            effect: "fadeOut",
            duration: 300
        },
        show: {
            effect: "fadeIn",
            duration: 300
        }
    });
    $("#contact .tabs").tabs({
        hide: {
            effect: "fadeOut",
            duration: 300
        },
        show: {
            effect: "fadeIn",
            duration: 300
        }
    });
    $('.video').click(function() {
        this.paused ? this.play() : this.pause();
    });
    $(".prod-description .tabs").tabs({
        hide: {
            effect: "fadeOut",
            duration: 300
        },
        show: {
            effect: "fadeIn",
            duration: 300
        }
    }).addClass("ui-tabs-vertical ui-helper-clearfix");
    $(".jet-system .tabs").tabs({
        hide: 'slideUp',
        show: 'slideDown',
        collapsible: true,

    }).addClass("ui-tabs-vertical ui-helper-clearfix");
    $(".specs-primary.tabs").tabs({
        hide: 'slideUp',
        show: 'slideDown',
        collapsible: true
    }).addClass("ui-tabs-vertical ui-helper-clearfix");
    $(".specs-additional.tabs").tabs({
        hide: 'slideUp',
        show: 'slideDown',
        collapsible: true,
        active: 1
    });
    $(".jet-system .tabs li").removeClass("ui-corner-top").addClass(
        "ui-corner-left");
    $(".accordion").accordion({
        heightStyle: "fill"
    });

    $(".collection-wrapper .tabs").tabs({
        collapsible: 'true',
        hide: 'slideUp',
        show: 'slideDown',
        active: 1
    }).addClass("ui-tabs-vertical ui-helper-clearfix");
    $(".collection-wrapper .tabs li").removeClass("ui-corner-top").addClass(
        "ui-corner-left");

    $( ".jet-system .tabs" ).on( "tabsactivate", function( event, ui ) {
        $('video').trigger('stop');
        $('video').trigger('play');
    } );

    $(".quote-section blockquote a").addClass("fancybox");

    $(".slick-dots button").addClass("sliderbutton");

    $('.smooth-scroll').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//,
            '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +
                ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
/******************************END OF OLD CODE*/

    $(document).ready(function(){

        $(window).trigger('resize');

        // NAV MENU TOGGLES

        if ($('html').hasClass('header-5')) {
                var item = $('<span class="header-5-menu-toggle" />');

                item.mouseover(function() {
                $(this).parent().toggleClass('active');
            });
        } else {
            var item = $('<span />');

            item.click(function() {
                //alert('click');
                $(this).parent().toggleClass('active');
            });
        }


        $('li.menu-item-has-children').append(item);




                // fix for li hover color on span hover ///
        $('.header6 ul.sub-menu li span').hover(function(){

            $(this).closest('li').toggleClass('li-is-active');


        });

        // END NAV MENU TOGGLES


        $('.tab-links li:first').addClass('current-tab');

       // $('div.tab:eq(0)').addClass('current-tab');
           //$('div.tab').not(':eq(0)').addClass('test');

        $('.tab-links li a').click(function(){

            $('.current-tab').removeClass('current-tab');
            $(this).parent().addClass('current-tab');
            var currenttab = $('.tab-links li.current-tab a').attr('class').replace(/^(\S*).*/, '$1');
//            alert(currenttab)
            $('div.'+currenttab).addClass('current-tab')

        });


                    // FIX ISSUE WITH FIRST TAB NOT OPENING PROPERLY
        $('ul.tab-links li:first-of-type a').trigger('click');



        $('#nav-toggle').click(function(){
            $('#nav-toggle').toggleClass('active');
            $('nav.mobile.nav').fadeToggle();
            $('html').toggleClass('active');
        });

        $('div.form-wrapper').hide();

        $('a.the-search-icon').click(function(){
            $('div.form-wrapper').fadeToggle();
        });

        $('div.esc-form').click(function(){
            $('div.form-wrapper').fadeToggle();
        });




        //faq
        $('ul.faq li').on('click', function(){
            $(this).toggleClass('current');
        });

        $('div.dropdown > ul').hide();
        $('div.dropdown > a.dropdown').on('click', function(){
            if ($(this).hasClass('active') == true) {
                $(this).removeClass('active').next('ul').slideUp();
            }else{
                $(this).addClass('active').next('ul').slideDown();
            }
        });

        /*************************OLD CODE NOT BY ALEX */
        $(document).foundation();
        $(".fancybox").attr('rel', 'image').fancybox({
            type: 'iframe',
            autoSize: true,
            helpers: {
                overlay: {
                    locked: false
                }
            }
        });
        $('#gform_22 .gform_validation_container').hide();
        $('.jet-details iframe').addClass('jet-video');
        $('.jet-details').each(function() {
            if ($(this).css('display') == 'block') {
                $(this).addClass('active-jet');
            }
            else {
                $(this).removeClass('active-jet');
            }
        });
        $('.prod-description .tab-content .content h4').addClass('title');
        $('.tub-form .true-option').click(function(e) {
            e.preventDefault();
            $('#tub-form .filter-submit').click();
        });

        $('.fancybox-wrap iframe a').addClass("fancybox");
        var iframe_width = $('.contact-map iframe').width();
        var iframe_height = (iframe_width) * (.75);
        $('.contact-map iframe').height(iframe_height);
        if ($(window).width() > 400) {
            var currentTallest = 0;
            var currentHeight = 0;
            $('.description-box').each(function() {
                currentHeight = $(this).height();
                if (currentHeight > currentTallest) {
                    currentTallest = currentHeight;
                }
            });
            $('.description-box').each(function() {
                $(this).height(currentTallest);
            });
        }

        /*********************************END OF OLD CODE */


    });//end doc ready

    $(window).scroll(function(){
        var masoncw = $('.masonry-grid').width()/2;

        $('.masonry-grid').masonry({
          columnWidth: masoncw,
          itemSelector: '.masonry-item'
        });
    });

    $(window).resize(function(){

        /*The height of the home video now lies in aspot-video.php*/

        /*var videoHeroHeight = $(window).height() - $('.the-header').outerHeight();*/
        var aspotWidth = $('.aspot').width();

        /*$('section.video-header').css({height:videoHeroHeight});

        $('section.img-pan').css({height:videoHeroHeight});*/

        var masoncw = $('.masonry-grid').width()/2;

        $('.masonry-grid').masonry({
          columnWidth: masoncw,
          itemSelector: '.masonry-item'
        });

        var numlinks = $(".desktop.nav .main.nav li").length;
        var numlinkssub = $(".desktop.nav .main.nav .sub-menu li").length;
        var numlinkstotal = (100/(numlinks - numlinkssub)) - .15;

        $('.header4 nav.global ul:first li').css({width: numlinkstotal + '%', 'text-align':'center', padding:0});
        $('.header9 nav.global ul:first li').css({width: numlinkstotal + '%', 'text-align':'center', padding:0});
        $('.header6 nav.global ul:first li').css({width: numlinkstotal + '%', 'text-align':'center'});
        $('.header8 nav.global ul:first li').css({width: numlinkstotal + .06 + '%', 'text-align':'center'});
        $('.header3 nav.global ul:first li').css({width: numlinkstotal + '%', 'text-align':'center'});


        var utnumlinks = $(".desktop.nav .menu.utilities li").length;
        var utnumlinkstotal = 100 / utnumlinks + '%';
//        alert( 'calc('+utnumlinkstotal+'%'+' - 80px)' );
        $('.header5 ul.menu.utilities li').css({ width: utnumlinkstotal });

        var numfootcontent = $("footer .gencontent").length;
//      alert(numfootcontent);
        if( $(window).width() > 900 ){
            $('footer .gencontent').css({width: (100/numfootcontent)-.5+'%'});
        }else{
            $('footer .gencontent').css({width: 'auto' });
        }

        var numfootcontent = $("footer .gencontent2").length;
    //      alert(numfootcontent);
            if( $(window).width() > 900 ){
                $('footer .gencontent2').css({width: (100/numfootcontent)-.5+'%'});
            }else{
                $('footer .gencontent2').css({width: 'auto' });
            }

        //prevent video from being bigger than screen
        //check window height and save
        var windowheight = $(window).height();
        //      alert(windowheight)
        //subtract the header height
        var videoheight = windowheight - $('header').height();
        //      alert(videoheight)
        $('.banner').css({'max-height': videoheight});

        //aspot creator text-container height calc for always centering
        var aspotcontentheight1 = $('h2.aspottext').outerHeight();
        var aspotcontentheight2 = $('h3.aspottext').outerHeight();
        var aspotcontentheight3 = $('a.aspottext').outerHeight();
        if( $(window).width() > 1024 ){
        var aspotcontentheight = aspotcontentheight1 + aspotcontentheight2 + aspotcontentheight3 + 40;
        }else{
        var aspotcontentheight = 90
        }
        $('.overlap-bar').find('.aspot-content ').addClass('auto-height');



        if($('.aspot-content').hasClass('auto-height')) {

            $('.aspot-content').css("height","auto");

        } else {

          $('.aspot-content').css({ height: aspotcontentheight });

        }


        if($(window).width() > 768 ){

            // grab the initial top offset of the navigation
            var stickyNavTop = $('header').height();

            // our function that decides weather the navigation bar should have "fixed" css position or not.
            var stickyNav = function(){
                var scrollTop = $(window).scrollTop(); // our current vertical position from the top
                // if we've scrolled more than the navigation, change its position to fixed to stick to top,
                // otherwise change it back to relative
                if (scrollTop > stickyNavTop) {
                    $('.anchor-links-bar').addClass('stick');
                } else {
                    $('.anchor-links-bar').removeClass('stick');
                }
            };

            stickyNav();
            // and run it again every time you scroll
            $(window).scroll(function() {
            stickyNav();

            });
        };

        var arrowstf = true;
        var dotstf = true;
        var slidesnum = 4;
        var autoplaytf = false;
        var galpageslidenum = 2;
        if( $(window).width() > 1024 ){
            galpageslidenum = 5;
            arrowstf = false;
            dotstf = true;
            slidesnum = 4;
            autoplaytf = true;
        }else if( $(window).width() > 600 ){
            galpageslidenum = 3;
            arrowstf = true;
            dotstf = false;
            slidesnum = 2;
            autoplaytf = false;
        }else{
            galpageslidenum = 2;
            arrowstf = true;
            dotstf = false;
            slidesnum = 1;
            autoplaytf = false;
        }
        $('.product-model-slider').slick({
            slidesToShow: slidesnum,
            slidesToScroll: slidesnum,
            arrows: arrowstf,
            dots: dotstf,
            autoplay: arrowstf,
            autoplaySpeed: 5000
        });
        $('.gallery-page').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            asNavFor: '.slider-nav'
        });

        $('.gallery-page-nav').slick({
            slidesToShow: galpageslidenum,
            slidesToScroll: galpageslidenum,
            arrows: true,
            asNavFor: '.gallery-page',
            dots: false,
            centerMode: true,
            focusOnSelect: true
        });
        $('.gallery-page-product').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            autoplaySpeed: 1000
        });
        $('.slider').slick({
            dots: true,
            arrows: false,
        });
        $('.slider2').slick({
            dots: true,
            arrows: false,
        });

        $('#bxslider').bxSlider({
            adaptiveHeight: true,
            mode: 'fade',
            controls: true,
            pager: true,
            pagerCustom: '#bx-pager'
            // minSlides: 3,
            // maxSlides: 4,
            // slideWidth: 170,
            // slideMargin: 10
        });

        $('.own-manual .custom').matchHeight();

        $('.matchHeight').matchHeight();
        $('.matchHeight2').matchHeight();
        $('.matchHeight3').matchHeight();

        /******************************************OLD CODE NOT BY ALEX*/
        var iframe_width = $('.contact-map iframe').width();
        var iframe_height = (iframe_width) * (.75);
        $('.contact-map iframe').height(iframe_height);
        $('.description-box').each(function() {
            $(this).height('auto');
        });
        if ($(window).width() > 400) {
            var currentTallest = 0;
            var currentHeight = 0;
            $('.description-box').each(function() {
                currentHeight = $(this).height();
                if (currentHeight > currentTallest) {
                    currentTallest = currentHeight;
                }
            });
            $('.description-box').each(function() {
                $(this).height(currentTallest);
            });
        }else {
            $('.description-box').each(function() {
                $(this).height('auto');
            });
        }
        /***********/

    });//end window resize

});






// CALC ENERGY

(function ($) {

    $.calculateEnergy = function() {
        var $model = document.ecf.model.value;
        var $climate = document.ecf.climate.value;
        var $error = '';
        var $cost = 0.00;

        var $models = {
            "vista"     : [13.92,18.28,17.81,16.31,20.67,22.99,28.13,27.63,36.61,64.55],
            "grandee"   : [13.92,18.28,17.81,16.31,20.67,22.99,28.13,27.63,36.61,64.55],
            "envoy"     : [11.61,15.53,15.33,14.17,18.09,20.24,24.88,24.54,32.62,57.68],
            "aria"      : [10.98,14.61,14.37,13.25,16.88,18.86,23.14,22.80,30.29,53.51],
            "vanguard"  : [11.22,15.03,14.85,13.74,17.56,19.65,24.17,23.84,31.70,56.07],
            "sovereign" : [11.07,14.70,14.44,13.30,16.93,18.90,23.19,22.84,30.33,53.57],
            "prodigy"   : [10.66,14.21,14.00,12.92,16.47,18.41,22.61,22.28,29.60,52.31],
            "jetsetter" : [10.27,13.74,13.57,12.55,16.02,17.93,22.03,21.73,28.89,51.08]
        }

        if ($model == '' && $climate == '') {
            $error = 'Please select a spa model, climate and rate.';
        } else if ($model == '') {
            $error = 'Please select a spa model';
        } else if ($climate == '') {
            $error = 'Please select a climate and rate';
        } else {
            $cost = $models[$model][$climate];
        }

        //Handle errors and success
        if($error){
            $(".energy-calculator-result").css("display","block");
            $(".energy-calculator-result h3").text($error);
        } else {
            $(".energy-calculator-result").fadeIn();
            $(".energy-calculator-result h3").text("Your monthly cost is estimated to be:");
            if($climate == 7){
                $(".energy-calculator-result span").text("C$" + $cost.toFixed(2));
            } else {
                $(".energy-calculator-result span").text("$" + $cost.toFixed(2));
            }
        }

        return false;
    }

    $('.form-control.selectpicker').change(function(){
        return $.calculateEnergy();
    });

///////////////////////////////
///Read More Homepage Toggle///
///////////////////////////////
$('.read-more-content').addClass('hide');

// Set up the toggle.
$('.read-more-toggle').on('click', function() {
  $(this).prev('.read-more-content').toggleClass('hide');
  if($(this).text()=="Read More")
  {
    $(this).text("Read Less");
  } else {
        $(this).text("Read More");
  }
});

///////////////////////////////
///SMOOTH ANCHOR LINK SCROLL///
///////////////////////////////

function scrollNav() {
  $('.aspot_anchor a').click(function(){
    //Toggle Class
    $(".active").removeClass("active");
    $(this).closest('li').addClass("active");
    var theClass = $(this).attr("class");
    $('.'+theClass).parent('li').addClass('active');
    //Animate
    $('html, body').stop().animate({
        scrollTop: $( $(this).attr('href') ).offset().top
    }, 500);
    return false;
  });
  $('.scrollTop a').scrollTop();
}
scrollNav();

///////////////////////////////////////////////////////////////////
///CHANGES PARENT DIV TO MATCH ABSOLUTE DIV HEIGHT///
///////////////////////////////////////////////////////////////////
if($('body').is('.page-template-template-form')){

  function biggestHeight() {
    var biggestHeight = "0";
    $(".body_container *").each(function(){
     if ($(this).height() > biggestHeight ) {
       biggestHeight = $(this).height()
     }
    });

    $(".body_container").height(biggestHeight +220);
    }
    $(window).resize(function() {
    biggestHeight();
});
  }

//WOOCOMMERCE ADD TO CART MESSAGE BOX
    $('.woocommerce-message').append( '<a href="javascript:;" class="close-woocommerce-box"></a>');
    $('.close-woocommerce-box').click(function(){
           $('.woocommerce-message').hide('fast');
    });





// TABS TO ACCORDIONS
// function windowSize() {
//   windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
// }
//
// $(window).resize(function() {
//   windowSize();
//
//   if (windowWidth < 642) {
//     console.log('width is under 642px !');
//
//     $('div[aria-labelledby="ui-id-1"]').insertAfter('.tabs ul li.ui-state-default[aria-labelledby="ui-id-1"]').addClass('added-tab');
//     $('div[aria-labelledby="ui-id-2"]').insertAfter('.tabs ul li.ui-state-default[aria-labelledby="ui-id-2"]').addClass('added-tab');
//     $('div[aria-labelledby="ui-id-3"]').insertAfter('.tabs ul li.ui-state-default[aria-labelledby="ui-id-3"]').addClass('added-tab');
//     $('div[aria-labelledby="ui-id-4"]').insertAfter('.tabs ul li.ui-state-default[aria-labelledby="ui-id-4"]').addClass('added-tab');
//     $('div[aria-labelledby="ui-id-5"]').insertAfter('.tabs ul li.ui-state-default[aria-labelledby="ui-id-5"]').addClass('added-tab');
//
//     $(".ui-tabs-anchor").click(function() {
//       var href = $(this).attr("href");
//       console.log(href);
//         $('html, body').animate({
//             scrollTop: $('.prod-description').offset().top
//         }, 500);
//     });
//
//
//
//   }
// });


/*$('div[aria-labelledby^="ui-id"]').css("outline","solid");
*/

/*Fixes red border in IE*/
/*$(".single-product .prod-description .tab-content").css({'outline': '2px solid #ffffff'});
*/

})(jQuery);
