  function BVHandleSummaryResults(jsonData) { 
   var ugcSummary = document.getElementById('UgcSummary_' + jsonData.subjectID); 
   if(ugcSummary) { 
    if(jsonData.totalReviews > 0 && jsonData.reviewsUrl.length > 0) {
   
     var readReviewStr = ""Read <span itemprop='reviewCount'>"" + jsonData.totalReviews + ""</span> reviews""; 
     var averageRating = jsonData.averageRating.toFixed(2);
     var averageRatingSplit = averageRating.split('.');
     var averageRatingUrl = averageRatingSplit[0] + '_' + averageRatingSplit[1];
     var starRating = 'https://hotspring.ugc.bazaarvoice.com/0526-en_us/'+ averageRatingUrl +'/5/rating.gif';
     
     ugcSummary.innerHTML += 
      /*""<span style='font-weight:bold;'>"" +
       ""Average <span itemprop='model'>Sovereign</span> Rating: "" +
      ""</span>""+*/
      ""<br />"" +
      ""<span class='hotSpringSpaModelRating' itemprop='aggregateRating' itemscope itemprop='http://schema.org/AggregateRating'>"" +
       ""<span itemprop='ratingValue'><img src=""+ starRating +"" alt=""+ jsonData.averageRating.toFixed(2) +""  style='padding: 5px 0;' /></span>"" +
       ""<br>"" +
       ""<a href="" + jsonData.reviewsUrl + "" title='Read Hot Spring Reviews' target='_blank' style='color: #4E7682; text-decoration: none;'>"" +
       ""Read <span itemprop='reviewCount'>"" + jsonData.totalReviews + ""</span> reviews</a>"" +
      ""</span>""; 
    
    } 
    if(jsonData.totalQuestions > 0 && jsonData.questionsUrl.length > 0) { 
     var readQAStr = ""Read "" + jsonData.totalQuestions + "" questions and "" + jsonData.totalAnswers + "" answers""; ugcSummary.innerHTML += ""<br>"" + readQAStr.link(jsonData.questionsUrl); 
    } 
   } 
  } 