<div class="vl-item promotion cf">
          <div class="the-image">
                <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                <img src="<?php echo $url; ?>" alt="<?php the_title(); ?>" />
          </div>

          <div class="the-content">

              <h3> <?php the_title(); ?> </h3>

              <?php
                $startdate = get_field('promo_start_date');
                $year = substr($startdate, 0, -4);
                $month = substr($startdate, 4, -2);
                $day = substr($startdate, -2);


                $startdate = $month . "/" . $day . "/" . $year;

                $enddate = get_field('promo_end_date');


                $year = substr($enddate, 0, -4);
                $month = substr($enddate, 4, -2);
                $day = substr($enddate, -2);

                $enddate = $month . "/" . $day . "/" . $year;

                $hideDates = get_field('hide_promo_dates');

                if(empty($hideDates)){
              ?>

                  <p><?php echo $startdate ?>&nbsp; - &nbsp;<?php echo $enddate ?></p>
          <? } ?>


              <?php the_content(); ?>
          </div>
    </div>
