<?php
/**
 * Template Name: Gallery Page
 */
?>

<?php while (have_posts()) : the_post(); ?>
  
  <?php get_template_part('templates/content', 'gallery-page'); ?>
<?php endwhile; ?>
