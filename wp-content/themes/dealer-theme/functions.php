<?php
//disable the jumping of gravity forms ajax forms
if(!has_filter( 'gform_confirmation_anchor', '__return_false' )) {
  add_filter( 'gform_confirmation_anchor', '__return_false' );
}
// disable srcset on frontend
add_filter('max_srcset_image_width', create_function('', 'return 1;'));
//CALDERA SYNC ID IN REST API
add_action( 'rest_api_init', 'slug_register_syncIDCS' );
function slug_register_syncIDCS() {
    register_rest_field( 'product','syncIDCS',
        array(
            'get_callback'    => 'slug_get_syncIDCS',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

/**
 * Get the value of the "calderaSyncID" field
 *
 * @param array $object Details of current post.
 * @param string $field_name Name of field.
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function slug_get_syncIDCS( $post, $field_name, $request ) {
    return get_post_meta( $post[ 'id' ], "syncIDCS", true );
}

//CALDERA ACCESSORIES SYNC ID IN REST API
add_action( 'rest_api_init', 'slug_register_syncIDCSA' );
function slug_register_syncIDCSA() {
    register_rest_field( 'product','syncIDCSA',
        array(
            'get_callback'    => 'slug_get_syncIDCSA',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

/**
 * Get the value of the "calderaSyncID" field
 *
 * @param array $object Details of current post.
 * @param string $field_name Name of field.
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function slug_get_syncIDCSA( $post, $field_name, $request ) {
    return get_post_meta( $post[ 'id' ], "syncIDCSA", true );
}

//HOT SPRING SYNC ID IN REST API
add_action( 'rest_api_init', 'slug_register_syncIDHS' );
function slug_register_syncIDHS() {
    register_rest_field( 'product','syncIDHS',
        array(
            'get_callback'    => 'slug_get_syncIDHS',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

/**
 * Get the value of the "syncIDHS" field
 *
 * @param array $object Details of current post.
 * @param string $field_name Name of field.
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function slug_get_syncIDHS( $post, $field_name, $request ) {
    return get_post_meta( $post[ 'id' ], "syncIDHS", true );
}



//HOT SPRING ACCESSORIES SYNC ID IN REST API
add_action( 'rest_api_init', 'slug_register_syncIDHSA' );
function slug_register_syncIDHSA() {
    register_rest_field( 'product','syncIDHSA',
        array(
            'get_callback'    => 'slug_get_syncIDHSA',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

/**
 * Get the value of the "syncIDHS" field
 *
 * @param array $object Details of current post.
 * @param string $field_name Name of field.
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function slug_get_syncIDHSA( $post, $field_name, $request ) {
    return get_post_meta( $post[ 'id' ], "syncIDHSA", true );
}


//FREEFLOW SYNC ID IN REST API
add_action( 'rest_api_init', 'slug_register_syncIDFRS' );
function slug_register_syncIDFRS() {
    register_rest_field( 'product','syncIDFRS',
        array(
            'get_callback'    => 'slug_get_syncIDFRS',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

/**
 * Get the value of the "syncIDFRS" field
 *
 * @param array $object Details of current post.
 * @param string $field_name Name of field.
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function slug_get_syncIDFRS( $post, $field_name, $request ) {
    return get_post_meta( $post[ 'id' ], "syncIDFRS", true );
}


//FREEFLOW ACCESSORIES SYNC ID IN REST API
add_action( 'rest_api_init', 'slug_register_syncIDFRSA' );
function slug_register_syncIDFRSA() {
    register_rest_field( 'product','syncIDFRSA',
        array(
            'get_callback'    => 'slug_get_syncIDFRSA',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

/**
 * Get the value of the "syncIDFRS" field
 *
 * @param array $object Details of current post.
 * @param string $field_name Name of field.
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function slug_get_syncIDFRSA( $post, $field_name, $request ) {
    return get_post_meta( $post[ 'id' ], "syncIDFRSA", true );
}

//FANTASY SYNC ID IN REST API
add_action( 'rest_api_init', 'slug_register_syncIDFAS' );
function slug_register_syncIDFAS() {
    register_rest_field( 'product','syncIDFAS',
        array(
            'get_callback'    => 'slug_get_syncIDFAS',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

/**
 * Get the value of the "syncIDFAS" field
 *
 * @param array $object Details of current post.
 * @param string $field_name Name of field.
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function slug_get_syncIDFAS( $post, $field_name, $request ) {
    return get_post_meta( $post[ 'id' ], "syncIDFAS", true );
}



//ENDLESS POOLS SYNC ID IN REST API
add_action( 'rest_api_init', 'slug_register_syncIDEPFS' );
function slug_register_syncIDEPFS() {
    register_rest_field( 'product','syncIDEPFS',
        array(
            'get_callback'    => 'slug_get_syncIDEPFS',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

/**
 * Get the value of the "syncIDHS" field
 *
 * @param array $object Details of current post.
 * @param string $field_name Name of field.
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function slug_get_syncIDEPFS( $post, $field_name, $request ) {
    return get_post_meta( $post[ 'id' ], "syncIDEPFS", true );
}


//ENDLESS POOLS Accessories SYNC ID IN REST API
add_action( 'rest_api_init', 'slug_register_syncIDEPFSA' );
function slug_register_syncIDEPFSA() {
    register_rest_field( 'product','syncIDEPFSA',
        array(
            'get_callback'    => 'slug_get_syncIDEPFSA',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

/**
 * Get the value of the "syncIDHS" field
 *
 * @param array $object Details of current post.
 * @param string $field_name Name of field.
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function slug_get_syncIDEPFSA( $post, $field_name, $request ) {
    return get_post_meta( $post[ 'id' ], "syncIDEPFSA", true );
}

//Function to add sync ID
//add_action('save_post', 'checkSyncID');



add_action( 'rest_api_init', 'add_thumbnail_to_JSON' );
function add_thumbnail_to_JSON() {
//Add featured image
register_rest_field( 
    'product', // Where to add the field (Here, blog posts. Could be an array)
    'featured_image_src', // Name of new field (You can call this anything)
    array(
        'get_callback'    => 'get_image_src',
        'update_callback' => null,
        'schema'          => null,
         )
    );
}

function get_image_src( $object, $field_name, $request ) {
  $feat_img_array = wp_get_attachment_image_src(
    $object['featured_media'], // Image attachment ID
    'thumbnail',  // Size.  Ex. "thumbnail", "large", "full", etc..
    true // Whether the image should be treated as an icon.
  );
  return $feat_img_array[0];
}

/**
 *  CUSTOM POST TYPES
 */
function cptui_register_my_cpts() {

	/**
	 * Post Type: Reviews.
	 */

	$labels = array(
		"name" => __( "Reviews", "dealer-theme" ),
		"singular_name" => __( "Review", "dealer-theme" ),
		"menu_name" => __( "Reviews", "dealer-theme" ),
		"all_items" => __( "All Reviews", "dealer-theme" ),
		"add_new" => __( "Add Review", "dealer-theme" ),
		"add_new_item" => __( "Add New Review", "dealer-theme" ),
		"edit" => __( "Edit", "dealer-theme" ),
		"edit_item" => __( "Edit Review", "dealer-theme" ),
		"new_item" => __( "New Review", "dealer-theme" ),
		"view" => __( "View", "dealer-theme" ),
		"view_item" => __( "View Review", "dealer-theme" ),
		"search_items" => __( "Search Reviews", "dealer-theme" ),
		"not_found" => __( "No Reviews Found", "dealer-theme" ),
		"not_found_in_trash" => __( "No Reviews in Trash", "dealer-theme" ),
	);

	$args = array(
		"label" => __( "Reviews", "dealer-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "reviews", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-megaphone",
		"taxonomies" => array( "post_tag" ),
	);

	register_post_type( "reviews", $args );

	/**
	 * Post Type: Jets.
	 */

	$labels = array(
		"name" => __( "Jets", "dealer-theme" ),
		"singular_name" => __( "Jet", "dealer-theme" ),
		"parent_item_colon" => __( "Parent Jet System", "dealer-theme" ),
		"add_new_item" => __( "Add New Jet", "dealer-theme" ),
	);

	$args = array(
		"label" => __( "Jets", "dealer-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "jets-api",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "jets", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-image-filter",
		"taxonomies" => array( "jets_cat" ),
	);

	register_post_type( "jets", $args );

	/**
	 * Post Type: Features.
	 */

	$labels = array(
		"name" => __( "Features", "dealer-theme" ),
		"singular_name" => __( "Feature", "dealer-theme" ),
		"add_new_item" => __( "Add New Feature", "dealer-theme" ),
	);

	$args = array(
		"label" => __( "Features", "dealer-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "features",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
    "hierarchical" => false,
    "menu_icon" => 'dashicons-feedback',
		"rewrite" => array( "slug" => "features", "with_front" => true ),
		"query_var" => true,
	);

	register_post_type( "features", $args );

	/**
	 * Post Type: Videos.
	 */

	$labels = array(
		"name" => __( "Videos", "dealer-theme" ),
		"singular_name" => __( "Video", "dealer-theme" ),
		"menu_name" => __( "Videos", "dealer-theme" ),
		"all_items" => __( "All Videos", "dealer-theme" ),
		"add_new" => __( "Add New", "dealer-theme" ),
		"add_new_item" => __( "Add New Video", "dealer-theme" ),
		"edit_item" => __( "Edit Video", "dealer-theme" ),
		"new_item" => __( "New Video", "dealer-theme" ),
		"view_item" => __( "View Video", "dealer-theme" ),
		"search_items" => __( "Search Video", "dealer-theme" ),
		"not_found" => __( "No Videos found", "dealer-theme" ),
		"not_found_in_trash" => __( "No Videos found in Trash", "dealer-theme" ),
		"parent_item_colon" => __( "Parent Video", "dealer-theme" ),
	);

	$args = array(
		"label" => __( "Videos", "dealer-theme" ),
		"labels" => $labels,
		"description" => "Add videos here",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
    "hierarchical" => false,
    "menu_icon" => 'dashicons-video-alt2',
		"rewrite" => array( "slug" => "videos", "with_front" => true ),
		"query_var" => true,
		"menu_position" => 5,
		"supports" => array( "title", "thumbnail" ),
	);

	register_post_type( "videos", $args );

	/**
	 * Post Type: General Blocks.
	 */

	$labels = array(
		"name" => __( "General Blocks", "dealer-theme" ),
		"singular_name" => __( "General Block", "dealer-theme" ),
		"add_new_item" => __( "Add New General Block", "dealer-theme" ),
	);

	$args = array(
		"label" => __( "General Blocks", "dealer-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
    "hierarchical" => false,
    "menu_icon" => 'dashicons-tagcloud',
		"rewrite" => array( "slug" => "general-block", "with_front" => true ),
		"query_var" => true,
	);

	register_post_type( "general-block", $args );

	/**
	 * Post Type: Promotions.
	 */

	$labels = array(
		"name" => __( "Promotions", "dealer-theme" ),
		"singular_name" => __( "promotion", "dealer-theme" ),
		"add_new_item" => __( "Add New Promotion", "dealer-theme" ),
	);

	$args = array(
		"label" => __( "Promotions", "dealer-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
    "map_meta_cap" => true,
    "menu_icon" => 'dashicons-welcome-view-site',
		"hierarchical" => false,
		"rewrite" => array( "slug" => "promotion", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail", "custom-fields" ),
		"taxonomies" => array( "post_tag" ),
	);

	register_post_type( "promotion", $args );

	/**
	 * Post Type: Aspot Videos.
	 */

	$labels = array(
		"name" => __( "Aspot Videos", "dealer-theme" ),
		"singular_name" => __( "Aspot Video", "dealer-theme" ),
		"add_new_item" => __( "Add New Aspot Video", "dealer-theme" ),
	);

	$args = array(
		"label" => __( "Aspot Videos", "dealer-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
    "hierarchical" => false,
    "menu_icon" => 'dashicons-format-video',
		"rewrite" => array( "slug" => "aspot-video", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title" ),
		"taxonomies" => array( "category" ),
	);

	register_post_type( "aspot-video", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );


function cptui_register_my_taxes() {

	/**
	 * Taxonomy: Categories.
	 */

	$labels = array(
		"name" => __( "Categories", "" ),
		"singular_name" => __( "Category", "" ),
		"parent_item" => __( "Parent", "" ),
		"parent_item_colon" => __( "Parent", "" ),
	);

	$args = array(
		"label" => __( "Categories", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Categories",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'reviews_cat', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => false,
		"rest_base" => "reviews_cat",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "reviews_cat", array( "reviews" ), $args );

	/**
	 * Taxonomy: Categories.
	 */

	$labels = array(
		"name" => __( "Categories", "" ),
		"singular_name" => __( "Category", "" ),
	);

	$args = array(
		"label" => __( "Categories", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Categories",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'jets_cat', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "jet-cats",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "jets_cat", array( "jets" ), $args );

	/**
	 * Taxonomy: Categories.
	 */

	$labels = array(
		"name" => __( "Categories", "" ),
		"singular_name" => __( "Category", "" ),
	);

	$args = array(
		"label" => __( "Categories", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Categories",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'product-info-cat', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => false,
		"rest_base" => "product-info-cat",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "product-info-cat", '', $args );

	/**
	 * Taxonomy: Categories.
	 */

	$labels = array(
		"name" => __( "Categories", "" ),
		"singular_name" => __( "Category", "" ),
	);

	$args = array(
		"label" => __( "Categories", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Categories",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'exclusive-feature-cat', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => false,
		"rest_base" => "exclusive-feature-cat",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "exclusive-feature-cat", array( "exclusive-features" ), $args );

	/**
	 * Taxonomy: Categories.
	 */

	$labels = array(
		"name" => __( "Categories", "" ),
		"singular_name" => __( "Category", "" ),
	);

	$args = array(
		"label" => __( "Categories", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Categories",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'feature-cat', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "feature-cat",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "feature-cat", array( "features" ), $args );

	/**
	 * Taxonomy: Video Categories.
	 */

	$labels = array(
		"name" => __( "Video Categories", "" ),
		"singular_name" => __( "Video Category", "" ),
	);

	$args = array(
		"label" => __( "Video Categories", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Video Categories",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'video-category', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => false,
		"rest_base" => "video-category",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "video-category", array( "videos" ), $args );

	/**
	 * Taxonomy: Spa Cabinet Categories.
	 */

	$labels = array(
		"name" => __( "Spa Cabinet Categories", "" ),
		"singular_name" => __( "Spa Cabinet Category", "" ),
	);

	$args = array(
		"label" => __( "Spa Cabinet Categories", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Spa Cabinet Categories",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'spa-cabinet-category', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "spa-cabinet-category",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "spa-cabinet-category", array( "cabinets" ), $args );

	/**
	 * Taxonomy: Spa Shell Categories.
	 */

	$labels = array(
		"name" => __( "Spa Shell Categories", "" ),
		"singular_name" => __( "Spa Shell Category", "" ),
	);

	$args = array(
		"label" => __( "Spa Shell Categories", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Spa Shell Categories",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'spa-shell-categories', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "spa-shell-categories",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "spa-shell-categories", array( "shells" ), $args );

	/**
	 * Taxonomy: Exceptional Features Categories.
	 */

	$labels = array(
		"name" => __( "Exceptional Features Categories", "" ),
		"singular_name" => __( "Exceptional Features Category", "" ),
	);

	$args = array(
		"label" => __( "Exceptional Features Categories", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "Exceptional Features Categories",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'exceptional-features-category', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => false,
		"rest_base" => "exceptional-features-category",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "exceptional-features-category", array( "exceptional-features" ), $args );

	/**
	 * Taxonomy: 360 View Categories.
	 */

	$labels = array(
		"name" => __( "360 View Categories", "" ),
		"singular_name" => __( "360 View Category", "" ),
	);

	$args = array(
		"label" => __( "360 View Categories", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "360 View Categories",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => '360-view-cat', 'with_front' => true, ),
		"show_admin_column" => true,
		"show_in_rest" => true,
		"rest_base" => "360-view-cat",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "360-view-cat", array( "360-view" ), $args );

	/**
	 * Taxonomy: General Blocks Categories.
	 */

	$labels = array(
		"name" => __( "General Blocks Categories", "" ),
		"singular_name" => __( "General Blocks Category", "" ),
	);

	$args = array(
		"label" => __( "General Blocks Categories", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => false,
		"label" => "General Blocks Categories",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'gb-cats', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "gb-cats",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "gb-cats", array( "general-block" ), $args );
}

add_action( 'init', 'cptui_register_my_taxes' );


/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/utils.php',                 // Utility functions
  'lib/init.php',                  // Initial theme setup and constants
  'lib/wrapper.php',               // Theme wrapper class
  'lib/conditional-tag-check.php', // ConditionalTagCheck class
  'lib/config.php',                // Configuration
  'lib/assets.php',                // Scripts and stylesheets
  'lib/titles.php',                // Page titles
  'lib/extras.php',                // Custom functions
];

function MPDversion() {
  echo 'MPD Version: 1.6';
}

foreach ( $sage_includes as $file ) {
  if ( ! $filepath = locate_template( $file ) ) {
    trigger_error( sprintf( __( 'Error locating %s for inclusion', 'sage' ), $file ), E_USER_ERROR );
  }

  require_once $filepath;
}
unset( $file, $filepath );

// Add support for WP 2.9+ post thumbnails
// if ( function_exists( 'add_theme_support' ) ) { // Added in 2.9
//   add_theme_support( 'post-thumbnails' );
//   set_post_thumbnail_size( 189, 189, true ); // default Post Thumbnail dimensions
//
//   add_image_size( 'boxes-image', 260, 193, false ); // blue box image
//   add_image_size( 'brand_logo', 280, 144, false ); // brands image
//   add_image_size( 'section_image', 263, 244, false ); // products image
//   add_image_size( 'quote_image', 174, 54, false ); // quote image
// }

//add_filter( 'show_admin_bar', '__return_false' );

if ( function_exists( 'acf_add_options_page' ) ) {

  acf_add_options_page( array(
    'page_title' => 'General Options',
    'menu_title' => 'General Options',
    'menu_slug'  => 'options',
    'capability' => 'edit_posts',
    'redirect'   => false
  ) );

}


function myplugin_settings() {
// Add tag metabox to page
  register_taxonomy_for_object_type( 'post_tag', 'page' );
// Add category metabox to page
//register_taxonomy_for_object_type('category', 'page');
}

// Add to the admin_init hook of your theme functions.php file
add_action( 'init', 'myplugin_settings' );

add_action( 'wp_enqueue_scripts', 'child_scripts' );

function child_scripts() {

  wp_enqueue_style( 'mcss', get_template_directory_uri() . '/stylesheets/screen.css' );

  wp_enqueue_style( 'slickcss', get_template_directory_uri() . '/dist/scripts/slick/slick.css' );
  wp_enqueue_style( 'hover', get_template_directory_uri() . '/dist/styles/hover-min.css' );
  wp_enqueue_style( 'bxslider', get_template_directory_uri() . '/dist/styles/jquery.bxslider.css' );
  wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' );
  wp_enqueue_style( 'fancyboxcss', get_template_directory_uri() . '/dist/scripts/fancybox/source/jquery.fancybox.css' );

//    wp_enqueue_script('jquery');

  wp_enqueue_script( 'jquery-ui', '//code.jquery.com/ui/1.11.4/jquery-ui.min.js' );

  wp_enqueue_script( 'importantjs', get_template_directory_uri() . '/dist/scripts/important.js', [ 'jquery' ], null, true );

  wp_enqueue_script( 'masonry', get_template_directory_uri() . '/dist/scripts/masonry.min.js', [ 'jquery' ], null, true );

//    wp_enqueue_script('slickEqualheightFancyboxFoundationMain', get_template_directory_uri().'/dist/scripts/slick/slick.min.js', ['jquery'], null, true);

  wp_enqueue_script( 'slick', get_template_directory_uri() . '/dist/scripts/slick/slick.min.js', [ 'jquery' ], null, true );

  wp_enqueue_script( 'bxslider', get_template_directory_uri() . '/dist/scripts/jquery.bxslider.js', [ 'jquery' ], null, true );

  wp_enqueue_script( 'fancyBox', get_template_directory_uri() . '/dist/scripts/fancybox/source/jquery.fancybox.js', [ 'jquery' ], null, true );

  wp_enqueue_script( 'spritespin', get_template_directory_uri() . '/dist/scripts/spritespin.js', [ 'jquery' ], null, true );

  wp_enqueue_script( 'foundation', get_template_directory_uri() . '/dist/scripts/foundation.min.js', [ 'jquery' ], null, true );

  wp_enqueue_script( 'matchHeight', get_template_directory_uri() . '/dist/scripts/matchHeight.js', [ 'jquery' ], null, true );


//    wp_enqueue_script('mainScripts', get_template_directory_uri().'/dist/scripts/main.js', ['jquery'], null, true);

}


/**
 * CONVERT HEX TO RGB COLOR CODE (for ACF Color Picker)
 */

function hex2rgb( $hex ) {
  $hex = str_replace( "#", "", $hex );

  if ( strlen( $hex ) == 3 ) {
    $r = hexdec( substr( $hex, 0, 1 ) . substr( $hex, 0, 1 ) );
    $g = hexdec( substr( $hex, 1, 1 ) . substr( $hex, 1, 1 ) );
    $b = hexdec( substr( $hex, 2, 1 ) . substr( $hex, 2, 1 ) );
  } else {
    $r = hexdec( substr( $hex, 0, 2 ) );
    $g = hexdec( substr( $hex, 2, 2 ) );
    $b = hexdec( substr( $hex, 4, 2 ) );
  }
  $rgb = array( $r, $g, $b );

  return $rgb; // returns an array with the rgb values
}


/*
Insert this script into functions.php in your WordPress theme (be cognizant of the opening and closing php tags) to allow field groups in Gravity Forms. The script will create two new field types - Open Group and Close Group. Add classes to your Open Group fields to style your groups.

Note that there is a stray (but empty) <li> element created. It is given the class "fieldgroup_extra_li" so that you can hide it in your CSS if needed.
*/
add_filter( "gform_add_field_buttons", "add_fieldgroup_fields" );
function add_fieldgroup_fields( $field_groups ) {

  foreach ( $field_groups as &$group ) {
    if ( $group["name"] == "standard_fields" ) {
      $group["fields"][] = array( "class"   => "button",
                                  "value"   => __( "Open Group", "gravityforms" ),
                                  "onclick" => "StartAddField('fieldgroupopen');"
      );
      $group["fields"][] = array( "class"   => "button",
                                  "value"   => __( "Close Group", "gravityforms" ),
                                  "onclick" => "StartAddField('fieldgroupclose');"
      );
      break;
    }
  }

  return $field_groups;
}

// Add title to the Field Group fields
add_filter( 'gform_field_type_title', 'field_group_titles' );
function field_group_titles( $type ) {
  if ( $type == 'fieldgroupopen' ) {
    return __( 'Open Field Group', 'gravityforms' );
  } else if ( $type == 'fieldgroupclose' ) {
    return __( 'Close Field Group', 'gravityforms' );
  }
}

add_filter( "gform_field_content", "create_gf_field_group", 10, 5 );
function create_gf_field_group( $content, $field, $value, $lead_id, $form_id ) {
  if ( ! is_admin() ) {
    if ( rgar( $field, "type" ) == "fieldgroupopen" ) {
      $content = "<ul><li style='display: none;'>";
    } else if ( rgar( $field, "type" ) == "fieldgroupclose" ) {
      $content = "</li></ul><!-- close field group --><li style='display: none;'>";
    }
  }

  return $content;
}

// Add a CSS class to the Field Group Close field so we can hide the extra <li> that is created.
add_action( "gform_field_css_class", "close_field_group_class", 10, 3 );
function close_field_group_class( $classes, $field, $form ) {
  if ( $field["type"] == "fieldgroupclose" ) {
    $classes .= " fieldgroup_extra_li";
  }

  return $classes;
}

add_action( "gform_editor_js_set_default_values", "field_group_default_labels" );
function field_group_default_labels() {
  ?>
  case "fieldgroupopen" :
  field.label = "Field Group Open";
  break;
  case "fieldgroupclose" :
  field.label = "Field Group Close";
  break;
  <?php
}

add_action( "gform_editor_js", "allow_fieldgroup_settings" );
function allow_fieldgroup_settings() {
  ?>
  <script type='text/javascript'>
    fieldSettings["fieldgroupopen"] = fieldSettings["text"] + ", .cssClass";
    fieldSettings["fieldgroupclose"] = fieldSettings["text"] + ", .cssClass";
  </script>
  <?php
}

add_filter( 'gform_field_content', 'gform_column_splits', 10, 5 );
function gform_column_splits( $content, $field, $value, $lead_id, $form_id ) {
  if ( ! IS_ADMIN ) { // only perform on the front end

    // target section breaks
    if ( $field['type'] == 'section' ) {
      $form = RGFormsModel::get_form_meta( $form_id, true );

      // check for the presence of multi-column form classes
      $form_class         = explode( ' ', $form['cssClass'] );
      $form_class_matches = array_intersect( $form_class, array( 'two-column', 'three-column' ) );

      // check for the presence of section break column classes
      $field_class         = explode( ' ', $field['cssClass'] );
      $field_class_matches = array_intersect( $field_class, array( 'gform_column' ) );

      // if field is a column break in a multi-column form, perform the list split
      if ( ! empty( $form_class_matches ) && ! empty( $field_class_matches ) ) { // make sure to target only multi-column forms

        // retrieve the form's field list classes for consistency
        $form              = RGFormsModel::add_default_properties( $form );
        $description_class = rgar( $form, 'descriptionPlacement' ) == 'above' ? 'description_above' : 'description_below';

        // close current field's li and ul and begin a new list with the same form field list classes
        return '</li></ul><ul class="gform_fields ' . $form['labelPlacement'] . ' ' . $description_class . ' ' . $field['cssClass'] . '"><li class="gfield gsection empty">';

      }
    }
  }

  return $content;
}

add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
  register_sidebar( array(
    'name'          => 'Right Sidebar',
    'id'            => 'right-sidebar',
    'description'   => "Content Menu Widget Area",
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ) );

  /*register_sidebar( array(
    'name' => 'Category Sidebar',
    'id' => 'category-sidebar',
    'description' => "Sidebar For Categories Only",
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
  ) );*/
}

add_action( 'after_setup_theme', 'woocommerce_support' );
remove_action( 'woocommerce_single_variation', 'woocommerce_support' );
function woocommerce_support() {
  add_theme_support( 'woocommerce' );
}

function after_header() {
  do_action( 'after_header' );
}

// Display 160 products per page. Goes in functions.php
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 160;' ), 20 );


//remove sections from woo commerce that we aren't using
//
//remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
//remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10, 0 );
//remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20, 0 );
//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10, 0 );
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10, 0 );
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10, 0 );
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20, 0 );
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30, 0 );
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40, 0 );
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 50, 0 );
//add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );


// HEADER 5 NAV LOCATIONS
function register_header5_menus() {
  register_nav_menus(
    array(
      'header_5_left'  => __( 'Header 5 Nav Bar - Left Side' ),
      'header_5_right' => __( 'Header 5 Nav Bar - Right Side' )
    )
  );
}

add_action( 'init', 'register_header5_menus' );


/* CUSTOM CATEGORY PRODUCT FIELDS */

// Add term page
add_action( 'product_cat_add_form_fields', 'wpm_taxonomy_add_new_meta_field', 10, 2 );

function wpm_taxonomy_add_new_meta_field() {
  // this will add the custom meta field to the add new term page
  ?>
  <div class="form-field">
    <label for="term_meta[custom_term_meta]"><?php _e( 'Details', 'wpm' ); ?></label>
    <textarea name="term_meta[custom_term_meta]" id="term_meta[custom_term_meta]" rows="5" cols="40"></textarea>
    <p class="description"><?php _e( 'Detailed category info to appear below the product list', 'wpm' ); ?></p>
  </div>
  <?php
}


// Edit term page
add_action( 'product_cat_edit_form_fields', 'wpm_taxonomy_edit_meta_field', 10, 2 );

function wpm_taxonomy_edit_meta_field( $term ) {

  // put the term ID into a variable
  $t_id = $term->term_id;

  // retrieve the existing value(s) for this meta field. This returns an array
  $term_meta = get_option( "taxonomy_$t_id" );
  $content   = $term_meta['custom_term_meta'] ? wp_kses_post( $term_meta['custom_term_meta'] ) : '';
  $settings  = array( 'textarea_name' => 'term_meta[custom_term_meta]' );
  ?>
  <tr class="form-field">
    <th scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php _e( 'Details', 'wpm' ); ?></label>
    </th>
    <td>
      <?php wp_editor( $content, 'product_cat_details', $settings ); ?>
      <p class="description"><?php _e( 'Detailed category info to appear below the product list', 'wpm' ); ?></p>
    </td>
  </tr>
  <?php
}

// Save extra taxonomy fields callback function
add_action( 'edited_product_cat', 'save_taxonomy_custom_meta', 10, 2 );
add_action( 'create_product_cat', 'save_taxonomy_custom_meta', 10, 2 );

function save_taxonomy_custom_meta( $term_id ) {
  if ( isset( $_POST['term_meta'] ) ) {
    $t_id      = $term_id;
    $term_meta = get_option( "taxonomy_$t_id" );
    $cat_keys  = array_keys( $_POST['term_meta'] );
    foreach ( $cat_keys as $key ) {
      if ( isset ( $_POST['term_meta'][ $key ] ) ) {
        $term_meta[ $key ] = wp_kses_post( stripslashes( $_POST['term_meta'][ $key ] ) );
      }
    }
    // Save the option array.
    update_option( "taxonomy_$t_id", $term_meta );
  }
}

// Display details on product category archive pages
add_action( 'woocommerce_after_shop_loop', 'wpm_product_cat_archive_add_meta' );

function wpm_product_cat_archive_add_meta() {
  $t_id              = get_queried_object()->term_id;
  $term_meta         = get_option( "taxonomy_$t_id" );
  $term_meta_content = $term_meta['custom_term_meta'];
  if ( $term_meta_content != '' ) {
    echo '<div class="woo-sc-box normal rounded full">';
    echo apply_filters( 'the_content', $term_meta_content );
    echo '</div>';
  }
}

/* REVIEW EXCERPTS */
function custom_field_excerpt() {
  global $post;
  $text = get_field( 'review-quote' );
  if ( '' != $text ) {
    $text = strip_shortcodes( $text );
    $text = apply_filters( 'the_content', $text );
    $text = str_replace( ']]>', ']]>', $text );
    if ( is_product() ) : $excerpt_length = 50; // 50 words
    else :$excerpt_length = 50; // 50 words
    endif;
    $excerpt_more = apply_filters( 'excerpt_more', ' ' . '[...]' );
    $text         = wp_trim_words( $text, $excerpt_length, $excerpt_more );
  }

  return apply_filters( 'the_excerpt', $text );
}


function is_desc_cat( $cats, $_post = null ) {
  foreach ( (array) $cats as $cat ) {
    if ( in_category( $cat, $_post ) ) {
      return true;
    } else {
      if ( ! is_int( $cat ) ) {
        $cat = get_cat_ID( $cat );
      }
      $descendants = get_term_children( $cat, 'category' );
      if ( $descendants && in_category( $descendants, $_post ) ) {
        return true;
      }
    }
  }

  return false;
}


/* CUSTOM PRODUCT TEMPLATES */
add_filter( 'template_include', 'product_custom_template', 12 );

function product_custom_template( $template ) {

  //if ( is_singular('product') && (has_term( 'hot-tubs', 'product_cat')) ) {
  global $post;
  $product_type = get_field( 'product-type', $post->ID );
  if ( is_singular( 'product' ) && ( $product_type == "Hot Tub" ) ) {
    $template = get_stylesheet_directory() . '/woocommerce/single-product-hot-tubs.php';

    //check if file exists if not then use the master theme one.
    if(!file_exists($template)) {  
      $template = get_template_directory() . '/woocommerce/single-product-hot-tubs.php';
    }
  }
  
  if ( is_singular( 'product' ) && ( $product_type == "Watkins Other Products" ) ) {
    $template = get_template_directory() . '/woocommerce/single-product-watkins-other.php';
  }

  return $template;
}


/* General Options */
/* After Save - Rewrite _variables.scss */
function rewrite_scss_after_save() {
  $screen = get_current_screen();
  if ( strpos( $screen->id, "options" ) == true ) {
    $creator_file_location = get_template_directory() . '/variable-creator.php';
    include_once( $creator_file_location );

    //Lets compile SASS HERE!
    require get_template_directory() . "/lib/scssphp/scss.inc.php";
    $scss = new scssc();
    $scss->setFormatter("scss_formatter");
    $scss->setImportPaths(get_template_directory() . '/sass/');
    
      $cssOut = $scss->compile('@import "screen.scss";');
    
    file_put_contents(get_template_directory() . '/stylesheets/screen.css', $cssOut);
    
  }
}

add_action( 'acf/save_post', 'rewrite_scss_after_save', 20 );


//ORDER BY PRICE
add_filter( 'woocommerce_get_catalog_ordering_args', 'am_woocommerce_catalog_orderby' );
function am_woocommerce_catalog_orderby( $args ) {
  $args['meta_key'] = '_price';
  $args['orderby']  = 'meta_value_num';
  $args['order']    = 'desc';

  return $args;
}


//CUSTOM WORDPRESS BACKEND STYLING
add_action( 'admin_head', 'custom_homepage_edit_admin' );

function custom_homepage_edit_admin() {
  echo '<style>
    .aspot-overlay-positioning ul {
    width: 400px;
}
.aspot-overlay-positioning ul li {
    width: 110px
}
    }
  </style>';
}


/**
 * WooCommerce Extra Feature
 * --------------------------
 *
 * Change number of related products on product page
 * Set your own value for 'posts_per_page'
 *
 */
function woo_related_products_limit() {
  global $product;

  $args['posts_per_page'] = 4;

  return $args;
}

add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
function jk_related_products_args( $args ) {
  $args['posts_per_page'] = 4; // 4 related products
  $args['columns']        = 4; // arranged in 2 columns
  return $args;
}


//MAIN ASPOT

function mainAspot() {


    $showAspot = get_field('show_aspot_content');
    $aspotImg = get_field('col_aspot_image');
    $aspotOverlayColor = get_field('aspot_overlay_color');
    $aspotOverlayOpacity = get_field("aspot_overlay_opacity");
    $aspotTextContainerSize = get_field('aspot_text_container_size');
    $aspotType = get_field('aspot_type');
    $seventy30Aspot = get_field('seventy30_aspot');
    $aspotHeight = get_field('aspot_height');
    $aspotMobileImg = get_field('aspot_mobile_img');

      if(!empty($seventy30Aspot)) {
        $seventy30Aspot = "seventy30-aspot";
      }


        function aspotTextElements() {
                $aspotHasContainer = get_field('aspot_have_container');
                $aspotTextPosition = get_field('aspot_text_position');
                $aspotTextPositionVertical = get_field('aspot_text_position_vertical');
                $aspotTextAlign = get_field('aspot_text_align');
                $pageTitleSettings = get_field('line1_title_settings');
                $line2TitleSettings = get_field('line2_title_settings');
                $aspotCustomTitle = get_field('custom_aspot_title');
                $aspotSubHeading = get_field('aspot_sub_heading');
                $showCTAbuttons = get_field('show_call_to_action_buttons');
                $aspotCTALink = get_sub_field('aspot_call_to_action_link');
                $aspotCTAText = get_sub_field('aspot_call_to_action_text');
                $showCrumbs = get_field('aspot_show_breadcrumbs');
                $aspotTextSize = get_field('aspot_text_size');
                $aspotTextContainerSize = get_field('aspot_text_container_size');
                $letterspacing = get_field('line_2_letterspacing');
                $underline = get_field('line_1_underline');


                $aspot_seventy30_img = get_field('aspot_seventy30_img');
                $seventy30_bg_color = get_field('seventy30_content_background_color');
                $seventy30_content_font_color = get_field('seventy30_content_font_color');
                $seventy30_gradient = get_field('seventy30_add_gradient');


                $aspotLineQty = '1';

                if(!empty($aspotSubHeading)) {
                    $aspotLineQty++;
                }
                if(!empty($showCrumbs)) {
                    $aspotLineQty++;
                }
                if(!empty($showCTAbuttons)) {
                    $aspotLineQty++;
                }

                // 70-30 ELEMENTS

                $Hex_color = $seventy30_bg_color;
                $RGB_color = hex2rgb($Hex_color);
                $Final_Rgb_color = implode(", ", $RGB_color);
                $seventy30_gradient_rgb = "rgb($Final_Rgb_color)";

                if(!empty($seventy30_gradient)) {

                  $seventy30_gradient = "background: -moz-linear-gradient(left,  rgba(0,0,0,0) 0%, $seventy30_gradient_rgb 100%); background: -webkit-linear-gradient(left,  rgba(0,0,0,0) 0%, $seventy30_gradient_rgb 100%); background: linear-gradient(to right,  rgba(0,0,0,0) 0%, $seventy30_gradient_rgb 100%); filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#325082', endColorstr='#325082',GradientType=1 );";
              }

            ?>

            <?
            $aspotMobileImg = get_field('aspot_mobile_img');
            $aspotImg = get_field('col_aspot_image');
            $aspotHeight = get_field('aspot_height');
            $aspotBgPos = get_field('aspot_bg_pos');
            ?>

            <style>

            .aspot-creator {
              background-size: cover;
            }


            @media only screen and (max-width:1024px) {
              <? if(!empty($aspotMobileImg)){ ?>
              .aspot-creator {
                background-image: url('<?
                  echo $aspotMobileImg; ?>')!important;
                  height:<?php if(!empty($aspotHeight)) { echo $aspotHeight; } else { ?><?php } ?>vh !important;
                background-position: url('<?
                  echo $aspotMobileImg; ?>')!important;
                  height:<?php if(!empty($aspotBgPos)) { echo $aspotBgPos; } else { ?><?php } ?>;
              }
              <? } ?>
              .aspot-creator img.desktop-aspot-img {
                display: none;
              }
            }



            @media only screen and (min-width:1024px) {
              .aspot-creator {
                background-image: url('<? echo $aspotImg; ?>') !important;
                height:<?php if(!empty($aspotHeight)) { echo $aspotHeight; } else { ?><?php } ?>vh !important;
                background-position: url('<?
                  echo $aspotMobileImg; ?>')!important;
                  height:<?php if(!empty($aspotBgPos)) { echo $aspotBgPos; } else { ?><?php } ?>;

              }
              .aspot-creator img.mobile-aspot-img {
                display: none;
              }
              div.aspot-content {
                background: <? echo $seventy30_bg_color; ?>
              }
            }

              div.aspot-content:before {
              <? echo $seventy30_gradient; ?>
              }

            }
            </style>

            <?php

                  if ( empty( $aspotMobileImg ) ) { ?>



                    <style type="text/css">


                      .hide-on-desktop {
                        display: none;
                      }

                      img.mobile-aspot-img {
                      display: none;
                    }

                    </style>

                  <?php } ?>



                <?php
                    //TEXT ELEMENTS BEGIN

                    if( $aspotHasContainer ) { ?>
                    <div class="container" id="container">
                <?php } ?>


                <div class="hide-on-tablet">
                    <div class="aspot-content <?php echo $aspotTextPosition; ?> text-container-<?php echo $aspotTextContainerSize; ?> line-qty-<?php echo $aspotLineQty; ?> h3-<?php echo $aspotTextSize; ?>" >



                        <div class="hide-on-tablet" style="position:relative;">

                          <? if(!empty($aspot_seventy30_img )) { ?>
                            <div class="img-container">
                              <img src="<? echo $aspot_seventy30_img ?>" />
                            </div>
                            <?
                          }


                           if($pageTitleSettings !== 'off') { ?>


                                  <?php if($pageTitleSettings == 'standard') { ?>
                                        <h2 class="aspot-page-title" style="text-decoration: underline;">
                                        <?php the_title(); ?></h2>
                                  <?php }

                                        if($pageTitleSettings == 'custom') { ?>
                                            <h2 class="aspot-page-title"



                                            <?php if($underline) { ?>

                                            style="text-decoration: underline;"

                                            <?php   }  ?> >




                                            <?php echo strip_tags($aspotCustomTitle); ?></h2>
                                      <?php } ?>



                               <?php } ?>


                                <?php
                                    // LINE 2
                                if ($aspotSubHeading) { ?>

                                    <h3 class="aspot-subheading aspot-h3-<?php echo $aspotTextSize; ?>"





                                    <?php if($letterspacing) {
                                    ?> style="letter-spacing: 10px;" <?php
                                            }
                                       ?> >


                                        <?php if($line2TitleSettings == 'standard') { ?>
                                                    <?php the_title(); ?></h3>
                                       <?php    }

                                                if($line2TitleSettings == 'custom') { ?>

                                                    <?php echo strip_tags($aspotSubHeading); ?></h3>
                                                <?php } ?>

                                    <?php } ?>


                                    <?php if (!empty($showCTAbuttons)){ ?>


<!--                                       BEGIN CTA REPEATER -->

          <?php
              // check for rows (sub repeater)
              if( have_rows('call_to_action_buttons') ): ?>

                <?php

                // loop through rows (sub repeater)
                while( have_rows('call_to_action_buttons') ): the_row();

                  // display each item as a list - with a class of completed ( if completed )
                  ?>
                   <a href="<?php echo get_sub_field('aspot_call_to_action_link'); ?>" class="aspottext cta-button

                   <?php echo get_sub_field('button_custom_class'); ?>

                   "><?php echo get_sub_field('aspot_call_to_action_text'); ?></a>
                <?php endwhile; ?>

              <?php endif; //if( get_sub_field('items') ): ?>




<!--                                      END CTA REPEATER -->






                                    <?php } ?>


                                <?php
                                        // BREADCRUMBS
                                        if( !empty($showCrumbs) ){ ?>
                                          <div class="breadcrumbs"><?php
                                              if ( function_exists('yoast_breadcrumb') ) {
                                                yoast_breadcrumb('','');
                                              }
                                         ?></div>
                                 <?php } ?>

                        </div>
                    </div>
                </div>

                <div class="hide-on-desktop">
                    <div class="aspot-content <?php echo $aspotTextPosition; ?><?php echo $aspotTextAlign; ?>">

                        <div class="hide-on-desktop">


                            <?php if ($aspotCustomTitle){ ?>

                            <h2 class="aspottext"><?php echo strip_tags($aspotCustomTitle); ?></h2>
                            <?php } ?>

                            <?php if ($aspotSubHeading){ ?>
                            <h3 class="aspottext"><?php echo strip_tags($aspotSubHeading); ?></h3>
                            <?php } ?>


                        </div>

                    </div>
                </div>

                <?php if( $aspotHasContainer ) { ?>
                    </div>
                <?php }//end if has container ?>



                     <?php
            //TEXT ELEMENTS END
        } // END aspotTextElements()



if(!empty($showAspot) ) {


        if($aspotType == 'video'){

                // Video A-spot
                $aspotVideoMp4Link = get_field('aspot_video_mp4_link');
                $aspotVideoWebmLink = get_field('aspot_video_webm_link');
                $aspotVideoMobileImage = get_field('aspot_video_mobile_image');
//                $aspotVideoHeight = get_field('aspot_vid_height');
//                $aspotCreatorHeight = get_field('aspot_creator_height');
//                $aspotVideoHeightUnit = get_field('aspot_vid_height_unit');
//                $aspotCreatorHeightUnit = get_field('aspot_creator_height_unit');
//                $aspotVideoContentHeight = get_field('aspot_vid_content_height');
//                $aspotVideoContentWidth = get_field('aspot_vid_content_width');

            ?>
              <section class="video-header aspot-creator <?php echo $seventy30Aspot ?>" style="height: <?php if( $aspotHeight ) { echo $aspotHeight . 'vh'; } else { echo '50vh'; } ?>; background-image:url('<? echo $aspotVideoMobileImage; ?>'); background-size: cover;">

                    <?php aspotTextElements(); ?>

                <video class="video-bg" autoplay loop muted width="100%">
                    <source src="<?php echo $aspotVideoWebmLink ?>" type="video/webm" codecs="vp8.0, vorbis"/>
                    <source src="<?php echo $aspotVideoMp4Link ?>" type="video/mp4"/>
                    Your browser does not support the video tag. We suggest you upgrade your browser.
                </video>

                <div class="overlay aspot-overlay-color" style="background-color:<?php echo $aspotOverlayColor; ?>; opacity:<?php echo $aspotOverlayOpacity; ?>"></div>


             </section>
    <?php } //end if aspot type = video

        if($aspotType == 'embed'){

            $embedCode = get_field('embed_code');
            ?>

            <div class="aspot-creator">
                <?php echo $embedCode ?>
            </div>

     <?php }//close if aspot type = embed

     if($aspotType == 'image'){
     ?>
         <div class="aspot-creator aspot-image">
             <?php if ($aspotOverlayColor){ ?>
                 <div class="aspot-color-overlay" style="background-color:<?php echo $aspotOverlayColor; ?>; opacity:<?php echo   $aspotOverlayOpacity; ?>;"></div>
             <?php }

                if(empty($aspotHeight)) { ?>
                  <img class="desktop-aspot-img" src="<?php echo $aspotImg; ?>" style="vertical-align: top; width:100%; opacity: 0;">
              <?
                }
              ?>

                  <img class="mobile-aspot-img" src="<?php echo $aspotMobileImg; ?>" style="vertical-align: top; width:100%; opacity: 0;">


                  <?php aspotTextElements(); ?>
             </div>
 <?php } //end if aspot type = image

 if($aspotType == 'bar') {


   $aspotOverlapBarColor =  get_field("aspot_overlap_bar_color");
   $aspotOverlapBarOpacity =  get_field("aspot_overlap_bar_opacity");
   $aspotOverlapGradientColor =  get_field("aspot_overlap_gradient_bar_color");
   $aspotOverlapGradientDir =  get_field("aspot_overlap_gradient_dir");
   $aspotOverlapStyle =  get_field("overlap_bar_style");
   $aspotOverlapImg =  get_field("overlap_bar_image");

   ?>

   <style>
   <? if ($aspotOverlapStyle == 'gradient') { ?>

     .overlap-bar {
       background: <?php echo $aspotOverlapBarColor ?>; /* For browsers that do not support gradients */
       background: -webkit-linear-gradient(<?php echo $aspotOverlapBarColor ?>, <?php echo $aspotOverlapGradientColor ?>); /* For Safari 5.1 to 6.0 */
       background: -o-linear-gradient(<?php echo $aspotOverlapBarColor ?>, <?php echo $aspotOverlapGradientColor ?>); /* For Opera 11.1 to 12.0 */
       background: -moz-linear-gradient(<?php echo $aspotOverlapBarColor ?>, <?php echo $aspotOverlapGradientColor ?>); /* For Firefox 3.6 to 15 */
       background: linear-gradient(<?php echo $aspotOverlapBarColor ?>, <?php echo $aspotOverlapGradientColor ?>); /* Standard syntax */
     }

   <?php }
   ?>


 </style>

     <div class="aspot-creator aspot-image aspot-overlap-bar">
         <?php if ($aspotOverlayColor){ ?>
             <div class="aspot-color-overlay" style="background-color:<?php echo $aspotOverlayColor; ?>; opacity:<?php echo   $aspotOverlayOpacity; ?>;"></div>
         <?php }

            if(empty($aspotHeight)) { ?>
              <img class="desktop-aspot-img" src="<?php echo $aspotImg; ?>" style="vertical-align: top; width:100%; opacity: 0;">
          <?
            }
          ?>

              <img class="mobile-aspot-img" src="<?php echo $aspotMobileImg; ?>" style="vertical-align: top; width:100%; opacity: 0;">





      </div>

         <?php if($aspotOverlapStyle == 'gradient') { ?>
              <div class="overlap-bar" style="background-color:<?php echo $aspotOverlapBarColor; ?>; opacity:<?php echo  $aspotOverlapBarOpacity; ?>";>
                <?php aspotTextElements(); ?>
              </div>
            <?php } else { ?>
              <div class="overlap-bar logo" style="background-color:white; ?>">
                  <div class="row">

<!--                          <div class="col-md-4">
 -->
                        <img src="<?php echo $aspotOverlapImg; ?>" alt="logo" class="overlap-bar-image"/>

                     <!--  </div> -->

                       <!--  <div class="col-md-8"> -->
                            <?php aspotTextElements(); ?>
                       <!--  </div> -->


                  </div>

              </div>
              <?php } ?>


      <?php

    }




    if($aspotType == 'imgpan'){



        $images = get_field('aspot_img_pan_images');

        $aspotOverlayType = get_field('aspot_background_overlay_type');

        ?>
        <section class="aspot img-pan" style="height: <?php if( $aspotHeight ) { echo $aspotHeight . 'vh'; } else { echo '91vh'; } ?>;">

            <?php if( $images ): ?>

                <?php foreach( $images as $image ): ?>

                    <div class="pan-item" style="background-image:url('<?php echo $image['url']; ?>'); height: <?php if( $aspotHeight ) { echo $aspotHeight . 'vh'; } else { echo '91vh'; } ?>;"></div>

                    <?php endforeach; ?>

                        <?php endif; ?>



            <div class="overlay" style="background-color:<?php echo $aspotOverlayColor; ?>; opacity:<?php echo $aspotOverlayOpacity; ?>"></div>

                                <?php aspotTextElements(); ?>


        </section>


    <?php } //end if aspot type = img pan


        }//end if show aspot

}//end mainspot function


//ShortCode For Calc Energy
function register_shortcodes() {
  add_shortcode( 'calc-energy', 'calcEnergyFunction' );
}

add_action( 'init', 'register_shortcodes' );

function calcEnergyFunction( $brand ) {
  extract( shortcode_atts( array(
    'brand' => 'hotspring',
  ), $atts ) );

  ob_start();
  ?>
  <div class="panel-pane pane-energy-calculator">
    <div class="pane-content">
      <div class="energy-calculator">
        <h2>Estimate your monthly operating costs</h2>
        <form role="form" id="energy-calculator-form" method="post" name="ecf">
          <fieldset>
            <div class="form-group">


              <?php if ( $brand == "hotspring" ) { ?>
                <select name="model" class="form-control selectpicker">
                  <option value="" default selected>Select a Model</option>
                  <option value="grandee">Grandee</option>
                  <option value="envoy">Envoy</option>
                  <option value="aria">Aria</option>
                  <option value="vanguard">Vanguard</option>
                  <option value="sovereign">Sovereign</option>
                  <option value="prodigy">Prodigy</option>
                  <option value="jetsetter">Jetsetter</option>
                </select>
              <?php } ?>

            </div>
            <br/>
            <div class="form-group">
              <select name="climate" class="form-control selectpicker">
                <option value="" default selected>Select a Climate and Rate</option>
                <option value="0">Miami, FL (75&deg;) &mdash; $0.11 kWh</option>
                <option value="1">Las Vegas, NV (70&deg;) &mdash; $0.12 kWh</option>
                <option value="2">Macon, GA (65&deg;) &mdash; $0.10 kWh</option>
                <option value="3">Tulsa, OK (60&deg;) &mdash; $0.08 kWh</option>
                <option value="4">St. Louis, MO (55&deg;) &mdash; $0.09 kWh</option>
                <option value="5">Chicago, IL (50&deg;) &mdash; $0.09 kWh</option>
                <option value="6">Casper, WY (45&deg;) &mdash; $0.10 kWh</option>
                <option value="7">Calgary, Canada (40&deg;) &mdash; $0.09 kWh</option>
                <option value="8">Int'l Falls, MN (35&deg;) &mdash; $0.11 kWh</option>
                <option value="9">Fairbanks, AK (30&deg;) &mdash; $0.18 kWh</option>
              </select>
            </div>
            <br/>
            <div class="clearfix"></div>
            <div class="form-group text-center">
              <!--                        <button type="submit"><span class="blue-button">Calculate Cost</span></button>-->
            </div>
          </fieldset>
        </form>
        <div class="energy-calculator-result">
          <h3></h3>
          <span></span>
        </div>
      </div>
    </div>


  </div>
  <br>
  <br>
  <?php
  return ob_get_clean();

}

//function custom_search_form( $form, $value = "Search", $post_type = 'post' ) {
//    $form_value = (isset($value)) ? $value : attribute_escape(apply_filters('the_search_query', get_search_query()));
//    $form = '<form method="get" class="blog-search" id="searchform" action="' . get_option('home') . '/" >
//        <input type="hidden" name="post_type" value="'.$post_type.'" />
//        <input type="text" placeholder="' . $form_value . '" name="s" id="s" />
//        <input type="submit" id="searchsubmit" value="'.attribute_escape(__('Search')).'" />
//    </form>';
//    return $form;
//}


/**
 * Add REST API support to Woocommerce Products.
 */
add_action( 'init', 'wcproduct_rest', 25 );
function wcproduct_rest() {
  global $wp_post_types;
  //be sure to set this to the name of your post type!
  $post_type_name = 'product';
  if ( isset( $wp_post_types[ $post_type_name ] ) ) {
    $wp_post_types[ $post_type_name ]->show_in_rest          = true;
    $wp_post_types[ $post_type_name ]->rest_base             = $post_type_name;
    $wp_post_types[ $post_type_name ]->rest_controller_class = 'WP_REST_Posts_Controller';
  }

}


/**
 * Add REST API support to an already registered taxonomy.
 */
add_action( 'init', 'my_custom_taxonomy_rest_support', 25 );
function my_custom_taxonomy_rest_support() {
  global $wp_taxonomies;

  //be sure to set this to the name of your taxonomy!
  $taxonomy_name = 'caldera-spas';

  if ( isset( $wp_taxonomies[ $taxonomy_name ] ) ) {
    $wp_taxonomies[ $taxonomy_name ]->show_in_rest          = true;
    $wp_taxonomies[ $taxonomy_name ]->rest_base             = $taxonomy_name;
    $wp_taxonomies[ $taxonomy_name ]->rest_controller_class = 'WP_REST_Terms_Controller';
  }


}




/* Disables WP-SCSS Plugin updates */
function disable_plugin_updates( $value ) {
  unset( $value->response['wp-scss/wp-scss.php'] );
  return $value;
}
add_filter( 'site_transient_update_plugins', 'disable_plugin_updates' );

/*Google map API*/
function my_acf_google_map_api( $api ) {
    $api['key'] = 'AIzaSyAhqthofUu97xg_zGhgzQs5gVjNQfvFnOc';

    return $api;
}

add_filter( 'acf/fields/google_map/api', 'my_acf_google_map_api' );
