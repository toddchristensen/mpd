<?php
/**
 * Template Name: Subcategory Template
 */
?>
<?php
/*************************************************************/
/**** FOR USE WITH ACCESSORIES, OTHER PRODUCTS CATEGORIES ****/
/****************** (NON MAIN PRODUCT LINES) *****************/
   while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
 
  <?php get_template_part('templates/content', 'sub-categories'); ?>
<?php endwhile; ?>
