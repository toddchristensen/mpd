<?php
/**
 * Template Name: Watkins Product Category
 */
?>
<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'watkins-categories'); ?>
<?php endwhile; ?>