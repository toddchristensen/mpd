<?php
/**
 * Template Name: Driving Directions
 */
?>

<?php while (have_posts()) : the_post(); ?>

  <?php get_template_part('templates/content', 'directions'); ?>
<?php endwhile; ?>
