<?php
//Dealer Colors PHP Variables
//Main
$primary = get_field('primary_color','option');
$secondary = get_field('secondary_color','option');
$accent = get_field('accent_color','option');
$link = get_field('basic_link_color','option');
$body_link = get_field('body_link_color','option');
$action = get_field('action_color','option');
$link_active = get_field('active_link_color','option');
//Header
$header_primary_bg = get_field('header_primary_background','option');
$header_secondary_bg = get_field('header_secondary_background','option');
$header_logo_bg = get_field('header_logo_background', 'option');


  //Header Image Options
  // $headerBgImage = get_field('header_bg_image', 'option');
  // $headerBgImageSize = get_field('header_bg_image_size', 'option');
  //
  // if(get_field('header_bg_image_repeat', 'option')){
  //   $headerBgImageRepeat = "repeat";
  // }else{
  //   $headerBgImageRepeat = "no-repeat";
  // }


$primary_nav_bg = get_field('primary_nav_background_color','option');
$primary_nav_bg_hover = get_field('primary_nav_hover_backgroundcolor','option');
$utility_nav_bg = get_field('utility_nav_background_color','option');
$utility_nav_bg_hover = get_field('utility_nav_hover_background_color','option');

$primary_link = get_field('primary_nav_link_color', 'option');
$primary_action = get_field('primary_nav_action_color', 'option');
$primary_active = get_field('primary_nav_active_color', 'option');


$util_link = get_field('utility_nav_link_color', 'option');
$util_action = get_field('utility_nav_action_color', 'option');
$util_active = get_field('utility_nav_active_color', 'option');
//Header Search
$search_bg = get_field('search_background_color', 'option');
$search_text = get_field('search_text_color', 'option');
//Header Nav Link
$primary_nav_link_padding = get_field('primary_nav_link_padding', 'option');
//Footer Image Options
/*$footerBgImage = get_field('footer_bg_image', 'option');
$footerBgImageSize = get_field('footer_bg_image_size', 'option');

if(get_field('footer_bg_image_repeat', 'option')){
  $footerBgImageRepeat = "repeat";
}else{
  $footerBgImageRepeat = "no-repeat";
}*/
//Footer
$footer_text = get_field('footer_text', 'option');
$footer_basic_link = get_field('footer_basic_link', 'option');
$footer_basic_link_hover = get_field('footer_basic_link_hover', 'option');
$footer_primary_bg = get_field('footer_primary_background', 'option');
$footer_secondary_bg = get_field('footer_secondary_background', 'option');
$footer_nav_bg = get_field('footer_nav_background', 'option');
$footer_nav_item_bg = get_field('footer_nav_item_background', 'option');
$footer_link = get_field('footer_nav_link_color', 'option');
$footer_link = get_field('footer_nav_link_color', 'option');
$footer_action = get_field('footer_nav_action_color', 'option');
$footer_active = get_field('footer_nav_active_color', 'option');
//Mobile Menu
$mobile_menu_bg = get_field('mobile_menu_bg', 'option');
$mobile_menu_link = get_field('mobile_link_color', 'option');
$mobile_menu_link_action = get_field('mobile_link_hover_color', 'option');

$scss = '//***** Variables.SCSS *****//' . PHP_EOL;
$scss = '//- Main -//' . PHP_EOL;
$scss .= '$primary: '.$primary.';' . PHP_EOL;#444;         //DARKEST
$scss .= '$secondary: '.$secondary.';' . PHP_EOL; #555;       //MEDIUM
$scss .= '$tertiary: '.$accent.';' . PHP_EOL; #ccc;        //LIGHTEST
$scss .= '$link: '.$link.';' . PHP_EOL; #555;';
$scss .= '$body_link: '.$body_link.';' . PHP_EOL; #555;';
$scss .= '$link-hover: '.$action.';' . PHP_EOL; #ccc;    //similar to tertiary/secondary

$scss .= '// Light through Dark //' . PHP_EOL;
$scss .= '$dark: #333;' . PHP_EOL;
$scss .= '$medium-dark: #999;' . PHP_EOL;
$scss .= '$medium: #aaa;' . PHP_EOL;
$scss .= '$medium-light: #ccc;' . PHP_EOL;
$scss .= '$light: #f1f1f1;' . PHP_EOL;

$scss .= '// White and Black and Transparent //' . PHP_EOL;
$scss .= '$white: #fff;' . PHP_EOL;    //light-text
$scss .= '$black: #000;' . PHP_EOL;
$scss .= '$trans-black: rgba(0,0,0,0.5);' . PHP_EOL;
$scss .= '$trans-black-low: rgba(0,0,0,0.2);' . PHP_EOL;
$scss .= '$trans-white: rgba(255,255,255,0.5);' . PHP_EOL;
$scss .= '$trans-white-low: rgba(255,255,255,0.2);' . PHP_EOL;
$scss .= '$trans: rgba(0,0,0,0);' . PHP_EOL;

//Texts

$scss .= '// Borders //' . PHP_EOL;
$scss .= '$light-border: #c2c2c2;' . PHP_EOL;

$scss .= '// Header //' . PHP_EOL;
// $scss .= '$header-bg-image: url("'.$headerBgImage.'");' . PHP_EOL;
// $scss .= '$header-bg-image-size: '.$headerBgImageSize.';' . PHP_EOL;
// $scss .= '$header-bg-image-repeat: '.$headerBgImageRepeat.';' . PHP_EOL;

$scss .= '$header-primary-bg: '.$header_primary_bg.';' . PHP_EOL;
$scss .= '$header-secondary-bg: '.$header_secondary_bg.';' . PHP_EOL;
if($header_logo_bg) {
  $scss .= '$header_logo_bg: '.$header_logo_bg.';' . PHP_EOL;
  }

$scss .= '$primary-nav-bg: '.$primary_nav_bg.';' . PHP_EOL;
$scss .= '$primary-nav-bg-hover: '.$primary_nav_bg_hover.';' . PHP_EOL;
$scss .= '$utility-nav-bg: '.$utility_nav_bg.';' . PHP_EOL;
$scss .= '$utility-nav-bg-hover: '.$utility_nav_bg_hover.';' . PHP_EOL;

$scss .= '$primary-nav-link: '.$primary_link.';' . PHP_EOL;
$scss .= '$primary-nav-link-action: '.$primary_action.';' . PHP_EOL;
$scss .= '$primary-nav-link-active: '.$primary_active.';' . PHP_EOL;


$scss .= '$util-nav-link: '.$util_link.';' . PHP_EOL;
$scss .= '$util-nav-link-action: '.$util_action.';' . PHP_EOL;
$scss .= '$util-nav-link-active: '.$util_active.';' . PHP_EOL;

$scss .= '$primary-nav-link-padding: '.$primary_nav_link_padding.';' . PHP_EOL;

$scss .= '$search-bg: '. $search_bg.';'.PHP_EOL;
$scss .= '$search-text: '. $search_text.';'.PHP_EOL;

$scss .= '// Footer //' . PHP_EOL;
$scss .= '$footer-text: '.$footer_text.';' . PHP_EOL;
$scss .= '$footer-link: '.$footer_basic_link.';' . PHP_EOL;
$scss .= '$footer-link-hover: '.$footer_basic_link_hover.';' . PHP_EOL;
$scss .= '$footer-primary-bg: '.$footer_primary_bg.';' . PHP_EOL;
$scss .= '$footer-secondary-bg: '.$footer_secondary_bg.';' . PHP_EOL;
$scss .= '$footer-nav-bg: '.$footer_nav_bg.';' . PHP_EOL;
$scss .= '$footer-nav-item-bg: '.$footer_nav_item_bg.';' . PHP_EOL;
$scss .= '$footer-nav-link: '.$footer_link.';' . PHP_EOL;
$scss .= '$footer-nav-link-action: '.$footer_action.';' . PHP_EOL;
$scss .= '$footer-nav-link-active: '.$footer_active.';' . PHP_EOL;

/*$scss .= '$footer-bg-image: url("'.$footerBgImage.'");' . PHP_EOL;
$scss .= '$footer-bg-image-size: '.$footerBgImageSize.';' . PHP_EOL;
$scss .= '$footer-bg-image-repeat: '.$footerBgImageRepeat.';' . PHP_EOL;*/


$scss .= '// Mobile Menu Colors //' . PHP_EOL;
$scss .= '$mobile-menu-bg: '.$mobile_menu_bg.';' . PHP_EOL;
$scss .= '$mobile-menu-link: '.$mobile_menu_link.';' . PHP_EOL;
$scss .= '$mobile-menu-link-action: '.$mobile_menu_link_action.';' . PHP_EOL;

$scss .= '// Watkins Branding //' . PHP_EOL;
$scss .= '$hotspring: #009AA6; //Lagoon (Teal)' . PHP_EOL;
$scss .= '$hotspring-blue: #0039A6; //Royal Blue' . PHP_EOL;

$scss .= '$caldera:#D93B20; //Vibrant Orange' . PHP_EOL;
$scss .= '$caldera-light: #F47A55; //Light Orange' . PHP_EOL;

$scss .= '$fantasy: #13BFBF;' . PHP_EOL;
$scss .= '$freeflow: #3498DB;' . PHP_EOL;

/*************************/




$fname = get_template_directory().'/sass/components/_variables.scss';
/*$fname = 'sass/components/_variables-test.scss';*/
    $fp =fopen($fname,'w');
    fwrite($fp,$scss);
    fclose($fp);
    //echo $download_file = "<a class='download_file' href='".$fname."'>Download</a>";
?>
