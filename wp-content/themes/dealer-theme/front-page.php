<?php
/* ------------------------------------------------------------------------ */
/* Template Name: Homepage Template
/* ------------------------------------------------------------------------ */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>

  <!-- Blank Template Option -->
  <?php if ( get_field('blank_homepage_template') == 'TRUE'):
          the_content();

        else:
          get_template_part('templates/content', 'homepage');

        endif;
    ?>

<?php endwhile; ?>
