<?php 
//Sync Spas
function syncAcce($catNames, $sync) {

	$type = "product";
	$dsFilter = "product_cat";
	$catNames = fixArray($catNames);

foreach ($catNames as $cName) {
	echo $cName;
	$posts = getContent($type,$dsFilter,$cName);

	foreach( $posts as $post ) {

			$postTitle = $post->title->rendered;
			$postSlug = $post->slug;
		  	$content = $post->content->rendered;
			$excerpt = $post->excerpt->rendered;
			$syncIDLabel = "NO";
			$xID = "NO";

 if ($cName == 'hot-spring-accessories') {
	  $syncIDLabel = "syncIDHSA";
	  $xID = $post->syncIDHSA;
 } 
 if ($cName == 'hot-spring-hot-tub-innovation') {
	$syncIDLabel = "syncIDHSA";
	$xID = $post->syncIDHSA;
} 
if ($cName == 'hot-spring-hot-tub-water-care') {
	$syncIDLabel = "syncIDHSA";
	$xID = $post->syncIDHSA;
} 
if ($cName == 'steps-covers-lifters') {
	$syncIDLabel = "syncIDHSA";
	$xID = $post->syncIDHSA;
} 
if ($cName == 'hot-spring-covers') {
	$syncIDLabel = "syncIDHSA";
	$xID = $post->syncIDHSA;
} 
if ($cName == 'hot-spring-lifters') {
	$syncIDLabel = "syncIDHSA";
	$xID = $post->syncIDHSA;
} 
if ($cName == 'hot-spring-steps') {
	$syncIDLabel = "syncIDHSA";
	$xID = $post->syncIDHSA;
} 

if ($cName == 'caldera-spas-accessories') {
	$syncIDLabel = "syncIDCSA";
	$xID = $post->syncIDCSA;
} 

if ($cName == 'caldera-spas-hot-tub-enjoyment') {
	$syncIDLabel = "syncIDCSA";
	$xID = $post->syncIDCSA;
} 

if ($cName == 'caldera-spas-hot-tub-innovation') {
	$syncIDLabel = "syncIDCSA";
	$xID = $post->syncIDCSA;
} 

if ($cName == 'caldera-spas-hot-tub-water-care') {
	$syncIDLabel = "syncIDCSA";
	$xID = $post->syncIDCSA;
} 

if ($cName == 'caldera-spas-steps-covers-lifters') {
	$syncIDLabel = "syncIDCSA";
	$xID = $post->syncIDCSA;
} 

if ($cName == 'caldera-spas-covers') {
	$syncIDLabel = "syncIDCSA";
	$xID = $post->syncIDCSA;
} 
 
if ($cName == 'caldera-spas-lifters') {
	$syncIDLabel = "syncIDCSA";
	$xID = $post->syncIDCSA;
} 

if ($cName == 'caldera-spas-steps') {
	$syncIDLabel = "syncIDCSA";
	$xID = $post->syncIDCSA;
} 

if ($cName == 'freeflow-spas-accessories') {
	$syncIDLabel = "syncIDFRSA";
	$xID = $post->syncIDFRSA;
}

if ($cName == 'fantasy-spas-accessories') {
	$syncIDLabel = "syncIDFASA";
	$xID = $post->syncIDFASA;
}

if ($cName == 'ep-accessories') {
	$syncIDLabel = "syncIDEPFSA";
	$xID = $post->syncIDEPFSA;
}
 echo $cName; 
 //CHECK IF THE ITEMS ARE SUPPOSE TO BE SYNCED
if ($syncIDLabel == "NO") { 
	echo "Something is wrong... Couldn't find syncable products.";
	echo "<br>";
	} else {

 $syncQueryArgs = array(
    'posts_per_page'   => 1000,
    'post_type'        => 'product',
	'meta_query' => array(
		array('key' => $syncIDLabel,
			'value' => $xID,
			'compare'   => '=',
		)
	)
);
$syncQuery = new WP_Query( $syncQueryArgs );

if( $syncQuery->have_posts() ) {
	while( $syncQuery->have_posts() ) {
	  $syncQuery->the_post();
		//Get Product ID
		$localPostTitle = get_the_title(); 
		$pageID = get_the_ID();

							$productPost = array(
							'ID'           => $pageID,
							'post_title'   => $postTitle,
							'post_content' => $content,
							'post_excerpt' => $excerpt,
						);
		
						// Update the post into the database
						wp_update_post( $productPost );
						
						echo "Update Product: " . $postTitle . " <br> ";
	  
	} // end while

} else {

	$author = "0";
	$term = $dsFilter;
	$taxonomy = getTaxonomy($cName);
	 //if its new create it!
	 $pageID = createWPPost($postTitle,$author,$content,$excerpt,$type);
	 wp_set_object_terms($pageID, $taxonomy, $term);
	 add_post_meta($pageID, $syncIDLabel, $xID, true);
	 echo "Created Product: " . $postTitle . " <br> ";
	//add featured image 
						
			 //Get media to download from server
			 $featuredMedia = $post->featured_media;
			 $APIMediaURL = "https://myproductdata.com/wp-json/wp/v2/media/";
			 $featuredMediaURLAPI = $APIMediaURL . $featuredMedia;
			 $response = wp_remote_get($featuredMediaURLAPI);
							  
				 if( is_wp_error( $response ) ) {
						 echo $response;
						 echo "<br>";
						 echo "try again please!";
						 die;
				 }
		 
			$media = json_decode( wp_remote_retrieve_body( $response ) );
			$mainImage = $media->guid->rendered;
			 addFeaturedImg($mainImage,$pageID);

	
}

					//new way to update acfs 
						 //get acfs from restapi 
						 $acfs = object_2_array($post->acf);
						  if($acfs) {
							foreach ($acfs as $acfName => $acfValue) {
								if($acfName == "jet-selection") {
									$acfValue = object_2_array($acfValue);
								}
								if($acfName == "reviews_code_embed") {
									$acfValue = str_replace("<p>", "", $acfValue);
									$acfValue = str_replace("</p>", "", $acfValue);
								}
								if($acfName == "features") {
									$features = object_2_array($acfValue);
									$featuresArray = [];
									foreach ($features as $feature) {
										$featTitle = $feature['post_title'];
										//get post id 
										$featureObj = get_page_by_title( $featTitle, OBJECT, "features" );
										array_push($featuresArray, $featureObj->ID);
									}
									$acfValue = $featuresArray;
								} 

								//special EF ACFS 

								if($acfName == "add_images") {
									$images = object_2_array($acfValue);
									$imgIDs = [];
									foreach ($images as $image) {
										$imgID = addImg($image['url']);
										array_push($imgIDs, $imgID);
									}
									$acfValue = $imgIDs;
								}
								if($acfName == "visual_list") {
									$acfContents = object_2_array($acfValue);
									$acfArray = [];
									foreach($acfContents as $acfContent) {
										$acfCs = object_2_array($acfContent);
										$acfCArray = [];
										foreach($acfCs as $acfN => $acfV) {
											if($acfN == "vl_image") {
												$imgID = addImg($acfV);
												$acfV = $imgID;
											}
											$acfCArray[$acfN] = $acfV;
										}
										array_push($acfArray, $acfCArray);
									}
									$acfValue = $acfArray;
								}
								if($acfName == "col_aspot_image") {
									$imgID = addImg($acfValue);
									$acfValue = $imgID; 
								}

								update_field($acfName, $acfValue, $pageID);
							}
						}

					
				}
     	}

		}

		echo ' 
		<div class="alert alert-success" role="alert">
		Accessories have been updated!
		</div>
		';
 }

 ?>