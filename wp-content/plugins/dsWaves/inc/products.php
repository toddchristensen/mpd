<?php 

//Sync Spas
function syncProds($catNames) {

	$type = "product";
	$dsFilter = "product_cat";
	
	$catNames = fixArray($catNames);

foreach ($catNames as $cName) {
$didItWork = false;
	$posts = getContent($type,$dsFilter,$cName);

	foreach( $posts as $post ) {

			$postTitle = $post->title->rendered;
			$postSlug = $post->slug;
		  	$content = $post->content->rendered;
			$excerpt = $post->excerpt->rendered;
			$syncIDLabel = "NO";
			$xID = "NO";


 if ($cName == 'utopia') {
	  $syncIDLabel = "syncIDCS";
	  $xID = $post->syncIDCS;
 } 
 if ($cName == 'paradise') {
	$syncIDLabel = "syncIDCS";
	$xID = $post->syncIDCS;
} 
if ($cName == 'vacanza') {
	$syncIDLabel = "syncIDCS";
	$xID = $post->syncIDCS;
} 

 if ($cName ==  'highlife-nxt' ) {
	 $syncIDLabel = "syncIDHS";
	 $xID = $post->syncIDHS;
 } 
 if ($cName ==  'highlife' ) {
	$syncIDLabel = "syncIDHS";
	$xID = $post->syncIDHS;
} 
if ($cName ==  'limelight' ) {
	$syncIDLabel = "syncIDHS";
	$xID = $post->syncIDHS;
} 
if ($cName ==  'hotspot' ) {
	$syncIDLabel = "syncIDHS";
	$xID = $post->syncIDHS;
} 

 if ($cName == 'fantasy-spas' ) {
	 $syncIDLabel = "syncIDFAS";
	 $xID = $post->syncIDFAS;
 } 

 if ($cName == 'freeflow-spas') {
	 $syncIDLabel = "syncIDFRS";
	 $xID = $post->syncIDFRS;
 }

 if ($cName == 'ep-swimcross-exercise-systems') {
	 $syncIDLabel = "syncIDEPFS";
	 $xID = $post->syncIDEPFS;
 }   
 if ($cName == 'ep-fitness-systems') {
	$syncIDLabel = "syncIDEPFS";
	$xID = $post->syncIDEPFS;
} 
if ($cName == 'ep-recsport-recreation-systems') {
	$syncIDLabel = "syncIDEPFS";
	$xID = $post->syncIDEPFS;
}     

 //CHECK IF THE ITEMS ARE SUPPOSE TO BE SYNCED
if ($syncIDLabel == "NO") { 
	echo "Something is wrong... Couldn't find syncable products.";
	echo " cname: ".$cName." xID: ".$xID;
	echo "<br>";
	} else {

 $syncQueryArgs = array(
    'posts_per_page'   => 1000,
    'post_type'        => 'product',
	'meta_query' => array(
		array('key' => $syncIDLabel,
			'value' => $xID,
			'compare'   => '=',
		)
	)
);
$syncQuery = new WP_Query( $syncQueryArgs );

if( $syncQuery->have_posts() ) {
	while( $syncQuery->have_posts() ) {
	  $syncQuery->the_post();
		//Get Product ID
		$localPostTitle = get_the_title(); 
		$pageID = get_the_ID();

							$productPost = array(
							'ID'           => $pageID,
							'post_title'   => $postTitle,
							'post_content' => $content,
							'post_excerpt' => $excerpt,
						);
		
						// Update the post into the database
						wp_update_post( $productPost );
						
						echo "Update Product: " . $postTitle . " <br> ";
	  
	} // end while

} else {

	$author = "0";
	$term = $dsFilter;
	$taxonomy = getTaxonomy($cName);
	 //if its new create it!
	 $pageID = createWPPost($postTitle,$author,$content,$excerpt,$type);
	 wp_set_object_terms($pageID, $taxonomy, $term);
	 add_post_meta($pageID, $syncIDLabel, $xID, true);
	 echo "Created Product: " . $postTitle . " <br> ";
	//add featured image 
					
}
			 //Get media to download from server
			//  $featuredMedia = $post->featured_media;
			//  $APIMediaURL = "https://myproductdata.com/wp-json/wp/v2/media/";
			//  $featuredMediaURLAPI = $APIMediaURL . $featuredMedia;
			//  $response = wp_remote_get($featuredMediaURLAPI);
							  
			// 	 if( is_wp_error( $response ) ) {
			// 			 echo $response;
			// 			 echo "<br>";
			// 			 echo "try again please!";
			// 			 die;
			// 	 }
		 
			//$media = json_decode( wp_remote_retrieve_body( $response ) );
			//$mainImage = $media->guid->rendered;
			// addFeaturedImg($mainImage,$pageID);
					//new way to update acfs 
						 //get acfs from restapi 
						 $acfs = object_2_array($post->acf);
						  if($acfs) {

							$fakeData = array(
								'name' => '',
								'value' => '',
						  );
						  add_row('field_5ba131857a263', $fakeData, $postID);
						  add_row('field_5ba13670ec410', $fakeData, $postID);

							foreach ($acfs as $acfName => $acfValue) {
								if($acfName == "acc_products") {
									$accIds = array();
									$acfName = "field_55f1d17a56c8b";
									foreach ($acfValue as $accValue) {
										$accIds[] .= $accValue['ID'];
									}
									$acfValue = $accIds;
								}
								if($acfName == "jet-selection") {
									$acfValue = object_2_array($acfValue);
								}
								if($acfName == "reviews_code_embed") {
									$acfValue = str_replace("<p>", "", $acfValue);
									$acfValue = str_replace("</p>", "", $acfValue);
								}
								if($acfName == "features") {
									$features = object_2_array($acfValue);
									$featuresArray = [];
									foreach ($features as $feature) {
										$featTitle = $feature['post_title'];
										//get post id 
										$featureObj = get_page_by_title( $featTitle, OBJECT, "features" );
										array_push($featuresArray, $featureObj->ID);
									}
									$acfValue = $featuresArray;
								} 

								//special EF ACFS 

								if($acfName == "add_images") {
									$images = object_2_array($acfValue);
									$imgIDs = [];
									foreach ($images as $image) {
										$imgID = addImg($image['url']);
										array_push($imgIDs, $imgID);
									}
									$acfValue = $imgIDs;
								}
								if($acfName == "visual_list") {
									$acfContents = object_2_array($acfValue);
									$acfArray = [];
									foreach($acfContents as $acfContent) {
										$acfCs = object_2_array($acfContent);
										$acfCArray = [];
										foreach($acfCs as $acfN => $acfV) {
											if($acfN == "vl_image") {
												$imgID = addImg($acfV);
												$acfV = $imgID;
											}
											$acfCArray[$acfN] = $acfV;
										}
										array_push($acfArray, $acfCArray);
									}
									$acfValue = $acfArray;
								}
								if($acfName == "col_aspot_image") {
									$imgID = addImg($acfValue);
									$acfValue = $imgID; 
								}

								update_field($acfName, $acfValue, $pageID);
							}
						}

					
				}
     	}

		}

		echo ' 
<div class="alert alert-success" role="alert">
Products have been updated!
</div>
';
 }

 ?>