<?php
// create a scheduled event (if it does not exist already)
function cronstarter_activation2() {
	if( !wp_next_scheduled( 'autoSyncPromo' ) ) {  
	   wp_schedule_event( time(), 'hourly', 'autoSyncPromo' );  
	}
}
// and make sure it's called whenever WordPress loads
// add_action('wp', 'cronstarter_activation');
// unschedule event upon plugin deactivation
function cronstarter_deactivate2() {	
	// find out when the last event was scheduled
	$timestamp = wp_next_scheduled ('autoSyncPromo');
	// unschedule previous event if any
	wp_unschedule_event ($timestamp, 'autoSyncPromo');
} 
register_deactivation_hook (__FILE__, 'cronstarter_deactivate');

// here's the function we'd like to call with our cron job
function autoSyncPromoFunc() {
    //get brands from database
    //spin up functions to run
    global $wpdb;
    $rows = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}waves WHERE id= 1", OBJECT );
    foreach($rows as $row) {
      $wavesDB = $row;
    }
    $wavesBrandsPromoRaw = $wavesDB->brandsPromo;
    $wavesBrandsPromo = explode( ',', $wavesBrandsPromoRaw );
    $wavesBrandsPromo = array_filter($wavesBrandsPromo);
    foreach($wavesBrandsPromo as $wavesBrandPromo) {
			//sync Promos
			syncPromos($wavesBrandPromo);
    }

    $log  = "Run Promo Sync at ".date("F j, Y, g:i a").PHP_EOL.
        "Attempt: ". $wavesBrandPromoRaw .PHP_EOL.
        "-------------------------".PHP_EOL;
//Save string to log, use FILE_APPEND to append.
file_put_contents('./log_'.date("j.n.Y").'.txt', $log, FILE_APPEND);

}

// hook that function onto our scheduled event:
add_action ('autoSyncPromo', 'autoSyncPromoFunc');

function autoSyncPromo($autoSync, $catNames) {
	global $wpdb;
    $brands = "";

	foreach ($catNames as $cName) {
        $brands .= $cName . ",";
    }
        
	 if($autoSync == null) {
		 $autoSync = 0;
	 }
	$results = $wpdb->update( 
		"wp_waves", 
		array(
			'autoSyncPromo' => $autoSync, 
			'brandsPromo' => $brands
		), 
		array('id' => 1)
	);

	if ($autoSync == 1) {
        cronstarter_activation2();
		echo '
		<div class="alert alert-success" role="alert">
		Auto Sync for Promos Enabled!
		</div>
		';
	} else {
        cronstarter_deactivate2();
		echo '
		<div class="alert alert-success" role="alert">
		Auto Sync for Promos Disabled!
		</div>
		';
	}
}
?>