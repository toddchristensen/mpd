<?php 

function syncGCBS($cName, $sync) {
	$type = "general-block";
	$host = "https://myproductdata.com/wp-json/wp/v2/";
	$ItemIDS = [];
	$didItWork = false;

	//check that category and get the ids
	switch ($cName) {
		case 'endless-pools-fitness-systems':
			$itemIDS = ["1006277", "1006312", "1006322", "1006337", "1012360"];
		break;
	}
	//loop over the ids and update each one
	foreach($itemIDS as $itemID) {
		$response = wp_remote_get($host.$type.'/'.$itemID);
		if( is_wp_error( $response ) ) {
			echo $response->get_error_message();
				echo "<br>";
				echo "try again please!";
				die;
		}
		$post = json_decode( wp_remote_retrieve_body( $response ) );

		$postTitle = html_entity_decode($post->title->rendered);
		$content = $post->content->rendered;
		$excerpt = $post->excerpt->rendered;
		$pageSlug = $post->slug;
		$pageID = $post->id;
		$type = html_entity_decode($type);
		$author = "0";
		$date = new DateTime();
		$date->add(DateInterval::createFromDateString('yesterday'));
		$contentModified = $date->format('Y-m-d') . "\n";

		  $postObject = get_page_by_slug($pageSlug);
		  //var_dump($postObject);
		  if ($postObject) {
			  $pageID = $postObject->ID;
			
	  } else {
					  //create GCB 	
					  $pageID = createWPPost($postTitle,$author,$content,$excerpt,$type);
					  echo "Created: " . $postTitle . "<br>";
				  
		  }

		  
		  if($sync == "auto") {
			//check if it really needs an update 
			$contentModifiedSync = $post->modified;
			$contentModifiedSync = explode('T', $contentModifiedSync);
			$contentModifiedSync = $contentModifiedSync[0];
		 }  
		 if($sync == "manual") {
			$contentModifiedSync = 0;	
			$contentModified = 0; 
		 }

	
		  if($contentModified == $contentModifiedSync) {
			$didItWork = true;
			echo "Updated: " . $postTitle . " " . $pageID . "<br>";	
		  //update acfs
		  if($post->acf) {
			  $acfs = object_2_array($post->acf);
			  foreach ($acfs as $acfName => $acfValue) {
				  if($acfName == "gb_header_background_image") {
					  if(!empty($acfValue)) {
						  $imgID = addImg($acfValue);
						  $acfValue = $imgID;
					  }
				  }
				  update_field($acfName, $acfValue, $pageID);
			  }
		  }
		}

	}


	if($didItWork) {
		echo ' 
<div class="alert alert-success" role="alert">
			'.$cName.' General Content Blocks have been updated!
</div>';
} else {
	echo ' 
	<div class="alert alert-warning" role="alert">
				'.$cName.' General Content Blocks did not need an update
	</div>';			
}

}
?>