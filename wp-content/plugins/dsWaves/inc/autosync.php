<?php
// create a scheduled event (if it does not exist already)
function cronstarter_activation() {
	if( !wp_next_scheduled( 'autoSync' ) ) {  
	   wp_schedule_event( time(), 'daily', 'autoSync' );  
	}
}
// and make sure it's called whenever WordPress loads
// add_action('wp', 'cronstarter_activation');
// unschedule event upon plugin deactivation
function cronstarter_deactivate() {	
	// find out when the last event was scheduled
	$timestamp = wp_next_scheduled ('autoSync');
	// unschedule previous event if any
	wp_unschedule_event ($timestamp, 'autoSync');
} 
register_deactivation_hook (__FILE__, 'cronstarter_deactivate');

// here's the function we'd like to call with our cron job
function autoSyncFunc() {
    //get brands from database
    //spin up functions to run
    global $wpdb;
    $rows = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}waves WHERE id= 1", OBJECT );
    foreach($rows as $row) {
      $wavesDB = $row;
    }
    $wavesBrandsRaw = $wavesDB->brands;
    $wavesBrands = explode( ',', $wavesBrandsRaw );
    $wavesBrands = array_filter($wavesBrands);
    $sync == "auto";
    foreach($wavesBrands as $wavesBrand) {

        //send the brands to functions
        //syncFeats
        syncFeats($wavesBrand, $sync);
        //syncJets
        syncJets($wavesBrand, $sync);
        // //syncGCBS
        syncGCBS($wavesBrand, $sync);
        //syncProducts
        switch($wavesBrand) {
            case 'hot-spring':
            $catNames = "highlife-nxt,highlife,limelight,hotspot";
            break;
            case 'caldera-spas':
            $catNames = "utopia,paradise,vacanza";
            break;
            case 'fantasy-spas':
            $catNames = "fantasy-spas";
            break;
            case 'freeflow-spas':
            $catNames = "freeflow-spas";
            break;
            case 'endless-pools-fitness-systems':
            $catNames = "ep-fitness-systems,ep-recsport-recreation-systems,ep-swimcross-exercise-systems";
            break;
        }
        $syncProds = syncProds($catNames, $sync);

        //syncAccessories 
         switch($wavesBrand) {
            case 'hot-spring':
            $catNames = "hot-spring-accessories,hot-spring-hot-tub-innovation,hot-spring-hot-tub-water-care,hot-spring-covers,hot-spring-lifters,hot-spring-steps";
            break;
            case 'caldera-spas':
            $catNames = "caldera-spas-accessories,caldera-spas-hot-tub-enjoyment,caldera-spas-hot-tub-innovation,caldera-spas-hot-tub-water-care,caldera-spas-steps-covers-lifters,caldera-spas-covers,caldera-spas-lifters,caldera-spas-steps";
            break;
            case 'fantasy-spas':
            $catNames = "fantasy-spas-accessories";
            break;
            case 'freeflow-spas':
            $catNames = "freeflow-spas-accessories";
            break;
            case 'endless-pools-fitness-systems':
            $catNames = "ep-accessories";
            break;
        }
        syncAcce($catNames, $sync);
        

    }

    $log  = "Run Sync at ".date("F j, Y, g:i a").PHP_EOL.
        "Attempt: ". $syncProds .PHP_EOL.
        "-------------------------".PHP_EOL;
//Save string to log, use FILE_APPEND to append.
file_put_contents('./log_'.date("j.n.Y").'.txt', $log, FILE_APPEND);

}

// hook that function onto our scheduled event:
add_action ('autoSync', 'autoSyncFunc');

function autoSync($autoSync, $catNames) {
	global $wpdb;
    $brands = "";

	foreach ($catNames as $cName) {
        $brands .= $cName . ",";
    }
        
	 if($autoSync == null) {
		 $autoSync = 0;
	 }
	$results = $wpdb->update( 
		"wp_waves", 
		array(
			'autoSync' => $autoSync, 
			'brands' => $brands
		), 
		array('id' => 1)
	);

	if ($autoSync == 1) {
        cronstarter_activation();
		echo '
		<div class="alert alert-success" role="alert">
		Auto Sync Enabled!
		</div>
		';
	} else {
        cronstarter_deactivate();
		echo '
		<div class="alert alert-success" role="alert">
		Auto Sync Disabled!
		</div>
		';
	}
}
?>