<?php 
//SYNC JETS
function syncJets($cName, $sync) {

    $author = "1";
	$content = "Jets";
	$except = "Jets";
	$type = "jets";
	$term = "jets_cat";
	$dsType = "jets-api";

		$posts = getContent($dsType,$term,$cName);

            foreach( $posts as $post ) {
                $postTitle = $post->title->rendered;
				$postID = $post->id;
				$content = $post->content->rendered;
				$excerpt = $post->excerpt->rendered;
                //Check if post exists
                if (post_exists($postTitle) ) {
					echo "Updated: " . $postTitle . "<br>";
				} else {
					 //if its new create it!
			     	 $postID = createWPPost($postTitle,$author,$content,$except,$type);
					 wp_set_object_terms($postID, $cName, $term);
					 echo "Created: " . $postTitle . "<br>";
				}
				if($post->acf) {
					$acfs = object_2_array($post->acf);
					if($acfs) {
					  foreach ($acfs as $acfName => $acfValue) { 
						  	update_field($acfName, $acfValue, $postID);
					  } 
					}
            	} 
		}
		echo ' 
		<div class="alert alert-success" role="alert">
		 Jets have been updated!
		</div>
		';
}
?>