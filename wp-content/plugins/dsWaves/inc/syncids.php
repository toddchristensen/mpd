<?php 
//SHOW IDS
function showIDs($cName) {
	// The args
	$args = array(
		'post_type'      => "product",
		'post_status' => array('publish', 'draft'),
		'posts_per_page' => -1,
		'product_cat' => $cName
	);
	// The Query
	$the_query = new WP_Query( $args );

	// The Loop
	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$itemID = get_the_ID();
			
			switch($cName) {

				case "hot-spring":
				$metaKey = "syncIDHS";
				break;
				case "hot-spring-accessories":
				$metaKey = "syncIDHSA";
				break;
				case "hot-spring-hot-tub-innovation":
				$metaKey = "syncIDHSA";
				break;
				case "hot-spring-hot-tub-water-care":
				$metaKey = "syncIDHSA";
				break;
				case "hot-spring-covers":
				$metaKey = "syncIDHSA";
				break;
				case "hot-spring-lifters":
				$metaKey = "syncIDHSA";
				break;
				case "hot-spring-steps":
				$metaKey = "syncIDHSA";
				break;
				case "caldera-spas":
				$metaKey = "syncIDCS";
				break;
				case "caldera-spas-accessories":
				$metaKey = "syncIDCSA";
				break;
				case "caldera-spas-hot-tub-enjoyment":
				$metaKey = "syncIDCSA";
				break;
				case "caldera-spas-hot-tub-innovation":
				$metaKey = "syncIDCSA";
				break;
				case "caldera-spas-hot-tub-water-care":
				$metaKey = "syncIDCSA";
				break;
				case "caldera-spas-steps-covers-lifters":
				$metaKey = "syncIDCSA";
				break;
				case "caldera-spas-covers":
			    $metaKey = "syncIDCSA";
				break;
				case "caldera-spas-lifters":
				$metaKey = "syncIDCSA";
				break;
				case "caldera-spas-steps":
				$metaKey = "syncIDCSA";
				break;
				case "freeflow-spas":
				$metaKey = "syncIDFRS";
				break;
				case "freeflow-spas-accessories":
				$metaKey = "syncIDFRSA";
				break;
				case "fantasy-spas":
				$metaKey = "syncIDFAS";
				break;
				case "fantasy-spas-accessories":
				$metaKey = "syncIDFASA";
				break;
				case "endless-pools-fitness-systems":
				$metaKey = "syncIDEPFS";
				break;
				case "ep-accessories":
				$metaKey = "syncIDEPFSA";
				break;
			
			}

			$syncID = get_post_meta($itemID, $metaKey, true);
			echo get_the_title() . " - " . $itemID. " | SyncID: ". $syncID;
			echo "<hr><br>";

		}

	} else {
		// no posts found
	}
	/* Restore original Post Data */
	wp_reset_postdata();
}

//SYNC IDS
function syncIDs($cName) {
	// The args
	$args = array(
		'post_type'      => "product",
		'post_status' => array('publish', 'draft'),
		'posts_per_page' => -1,
		'product_cat' => $cName
	);
	// The Query
	$the_query = new WP_Query( $args );
    $syncIDDone = false;
	// The Loop
	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$itemID = get_the_ID();
			$itemName = get_the_title();
			//match ID with SyncID and ADD the META KEY 

			//Check WHAT IS THE Category Name
			if($cName == "hot-spring") {
				$metaKey = "syncIDHS";
				$syncIDs = ["1001534","1015490","1015474","1001533","1001548","1001545","1001532","1001531","1001526","1001525","1001524","1001523","1001549","1001521","1001522","1001520","1001519","1001547","1001518","1001517","1001516","1001515","1015791","1015794","1015820", "1018465", "1018462", "1018459"];
				$syncNames = ["GrandeeNXT","VanguardNXT","AriaNXT","EnvoyNXT","JetsetterNXT","Grandee","Envoy","Aria","Vanguard","Sovereign","Prodigy","Jetsetter","Gleam","Pulse","Flair","Glow","Bolt","Tempo","Rhythm","Relay","SX","TX","Prism","Flash","Beam"];
				$hs = 1;
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					
					
					if($itemName == $syncName) {
						delete_post_meta($itemID, $metaKey);
						
						add_post_meta($itemID, $metaKey, $hs, true);
					
						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $hs ."<br><hr><br>";
                        $syncIDDone = true;
					}
					$hs ++;
				}
			}


			if($cName == "hot-spring-accessories") {
				$metaKey = "syncIDHSA";
				$syncNames = ["FreshWater® MPS Oxidizer","FreshWater® MPS Tabs","FreshWater® Defoamer","FreshWater® Enzyme Clarifier","FreshWater® pH/Alkalinity Down","FreshWater® pH/Alkalinity Up","FreshWater® pH Balance Plus","FreshWater® Stain & Scale Defense","FreshWater® MPScents Spring Shower","FreshWater® MPScents Island Escape","FreshWater® MPScents Citrus Blossom","FreshWater® MPScents Zen Garden","FreshWater® Cover Shield","FreshWater® Filter Cleaner Granules 2-lb.","FreshWater® Instant Filter Cleaner Spray 16 oz.","FreshWater® Spa Shine","FreshWater® Sodium Bromide Solution","FreshWater® Chlorinating Granules","FreshWater® Balanced Shock 4 in 1 Blend","FreshWater® 5-way Test Strips","FreshWater® ACE® Cell Cleaner","FreshWater® pH/Alkalinity Down 1.5 lb.","FreshWater® Chlorinating Granules","FreshWater® pH/Alkalinity Up 1.25 lb.","FreshWater® Salt Test Strips 25 ct","FreshWater® ACE® Spa Salt","Continuous Silver Ion (AG+) Purifier","Vanishing Act™ Calcium Remover","EverFresh® Water Care System","Hot Spring® Replacement Filters","Hot Spring® Clean Screen® Pre-Filter","ACE® Salt Water Sanitizing System","Hot Spring® Spas 22” HD Wireless Monitor","Hot Spring® 32″ Polymer Spa Steps","Hot Spring® Everwood® HD Spa Steps","Hot Spring® NXT Spa Steps","Hot Spring® Spastone® Spa Steps","Hot Spring® 32″ Everwood Spa Steps","Hot Spring® Connextion™ Remote Monitoring System","Cool Zone™ Hot Tub Cooling System","Hot Spring® Sound System with Bluetooth® Wireless Technology","Hot Spring® Replacement Covers","Hot Spring® Lift ‘N Glide® Cover Lifter","Hot Spring® UpRite® Cover Lifter","Hot Spring® CoverCradle® II Cover Lifter","Hot Spring® CoverCradle® Cover Lifter"];
				$hsA = 1;
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					$syncName = fixName($syncName);

					if($itemName == $syncName) {
						delete_post_meta($itemID, $metaKey);
						
						add_post_meta($itemID, $metaKey, $hsA, true);
					
						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $hsA ."<br><hr><br>";
                        $syncIDDone = true;
					}
					$hsA ++;
				}
			}

			

			if($cName == "caldera-spas") {
				$metaKey = "syncIDCS";
				$syncIDs = ["1000582","1000581","1000570","1000580","1000579","1000578","1000577","1000576","1000575","1000574","1000573","1000628","1000571", "1018440", "1018444"];
				$syncNames = ["Cantabria", "Geneva", "Niagara", "Tahitian", "Makena", "Salina", "Martinique", "Kauai", "Palatino", "Marino", "Vanto", "Tarino", "Aventine", "Florence", "Provence"];
				$cs = 1;
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					
					if($itemName == $syncName) {
	
						delete_post_meta($itemID, $metaKey);

						add_post_meta($itemID, $metaKey, $cs, true);

						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $cs."<br><hr><br>";
                        $syncIDDone = true;
					}
					$cs ++;
				}

			}


			if($cName == "caldera-spas-accessories") {
				$metaKey = "syncIDCSA";
				$syncNames = ["MPS Test Strips","Monarch MPS Plus®","Monarch Silver Cartridge","MPS Non-Chlorine Oxidizer","Monarch® Stain & Scale Defense 16 oz.","Monarch® pH/Alkalinity Up 1.25 lb.","Monarch® pH/Alkalinity Down 1.5 lb.","Monarch® Defoamer 16 oz.","Monarch® Calcium Hardness Increaser 32 oz.","Monarch® Instant Filter Cleaner Spray 16 oz","Monarch® Chlorinating Granules","Monarch® Filter Cleaner Granules 2-lb.","Monarch® Cover Shield","Clean Screen™ Pre-filter","Vanishing Act™ Calcium Remover","Caldera® Spas Retail-Ready Filters","MONARCH® CD Ozone Water Care System","Caldera® Spas 22” HD Wireless Monitor","Caldera® Spas 32″ Polymer Hot Tub Steps","Caldera® Spas Connextion™ Remote Monitoring System","Caldera® Paradise™ Series Speakers","Caldera® Utopia® Series Speakers","Caldera® Spas Replacement Cover","Caldera® Spas In Home Wireless","Caldera® Spas Vacanza Speaker","FROG® Water Care System","Caldera® Spas CoolZone™","Caldera® Spas ProLift® Hot Tub Cover Lifter","Caldera® Spas ProLift® IV Hot Tub Cover Lifter","Caldera® Spas ProLift® II Hot Tub Cover Lifter","Caldera® Spas ProLift® III Hot Tub Cover Lifter","Caldera® Spas Sound System with Bluetooth® Wireless Technology","Caldera® Spas EcoTech® Steps","Caldera® Spas Retail-Ready Filters","Caldera® Spas 32″ Polymer Hot Tub Steps"];
				$csA = 1;
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					$syncName = fixName($syncName);
					
					if($itemName == $syncName) {
	
						delete_post_meta($itemID, $metaKey);

						add_post_meta($itemID, $metaKey, $csA, true);

						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $csA ."<br><hr><br>";
                        $syncIDDone = true;
					}
					$csA ++;
				}

			}

			

			if($cName == "freeflow-spas") {
				$metaKey = "syncIDFRS";
				$syncIDs = ["1587","1602","1611","1613","1614","1615","1616"];
			    $frs = 1;
				foreach ($syncIDs as $syncID) {
					if($itemID == $syncID){
						delete_post_meta($itemID, $metaKey);
						
						add_post_meta($itemID, $metaKey, $frs, true);
						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $frs."<br><hr><br>";
                        $syncIDDone = true;
					}
					$frs ++;
				} 

			}	


			if($cName == "freeflow-spas-accessories") {
				$metaKey = "syncIDFRSA";
				$syncNames = ["Freeflow® Spas Replacement Filters","Freeflow® Spas Spa Steps","Square Cover Lifter","Round/Triangle Cover Lifter"];
				$frsA = 1;
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					$syncName = fixName($syncName);

					if($itemName == $syncName) {
						delete_post_meta($itemID, $metaKey);
						
						add_post_meta($itemID, $metaKey, $frsA, true);
					
						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $frsA ."<br><hr><br>";
                        $syncIDDone = true;
					}
					$frsA ++;
				}
			}


			if($cName == "fantasy-spas") {
				$metaKey = "syncIDFAS";
				$syncIDs = ["1619","1621","1622","1623","2938"];
			    $fas = 1;
				foreach ($syncIDs as $syncID) {
					if($itemID == $syncID){
						delete_post_meta($itemID, $metaKey);
						
						add_post_meta($itemID, $metaKey, $fas, true);
						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $fas."<br><hr><br>";
                        $syncIDDone = true;
					}
					$fas ++;
				} 

			}

			if($cName == "fantasy-spas-accessories") {
				$metaKey = "syncIDFASA";
				$syncNames = ["Fantasy Spas® Replacement Filters","Fantasy Spas® Spa Steps","Square Cover Lifter","Ozone Kit"];
				$fasA = 1;
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					$syncName = fixName($syncName);

					if($itemName == $syncName) {
						delete_post_meta($itemID, $metaKey);
						
						add_post_meta($itemID, $metaKey, $fasA, true);
					
						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $fasA ."<br><hr><br>";
                        $syncIDDone = true;
					}
					$fasA ++;
				}
			}



			if($cName == "endless-pools-fitness-systems") {
				$metaKey = "syncIDEPFS";
				$syncNames = ["E2000 Endless Pools® Fitness Systems","E700 Endless Pools® Fitness Systems","E500 Endless Pools® Fitness Systems","X2000 SwimCross™ Exercise Systems","X200 SwimCross™ Exercise Systems","X500 SwimCross™ Exercise Systems","RecSport™ R120 Recreation Systems"];
				$epfs = 1;
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					$syncName = fixName($syncName);

					if($itemName == $syncName) {
						delete_post_meta($itemID, $metaKey);
						
						add_post_meta($itemID, $metaKey, $epfs, true);
					
						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $epfs ."<br><hr><br>";
                        $syncIDDone = true;
					}
					$epfs ++;
				}
			}

	
			
			if($cName == "ep-accessories") {
				$metaKey = "syncIDEPFSA";
				$syncNames = ["Watkins CoverCradle®","CoolZone™ Cooling System","Endless Pools Fit@Home™ Wifi and Mobile App","Underwater Treadmill","Pace Displays","Wifi and Mobile App","Vacuseal™ Lifting System","Watkins Cover & Lifter","Bluetooth®-Enabled Sound System","Swim Tether","Row Bar Kit with Resistance Gear","Underwater Mirror"];
				$epfsA = 1;	
				
				foreach ($syncNames as $syncName) {	
					//remove the symbols from the name
					$itemName = fixName($itemName);
					$syncName = fixName($syncName);
	
					if($itemName == $syncName) {
						delete_post_meta($itemID, $metaKey);
						
						add_post_meta($itemID, $metaKey, $epfsA, true);
					
						echo $itemName . " Added SyncKey!<br>";
                        echo $itemID . " - " . $epfsA ."<br><hr><br>";
                        $syncIDDone = true;
					}
					$epfsA ++;
				}
			}
			
			

		}

    } 

    if (   $syncIDDone == true) {
    echo ' 
<div class="alert alert-success" role="alert">
Sync IDs are Done!
</div>
';
} else {
    echo ' 
    <div class="alert alert-danger" role="alert">
    Something when wrong! for '.$cName .'
    </div>
    ';
}
	/* Restore original Post Data */
	wp_reset_postdata();
}
?>