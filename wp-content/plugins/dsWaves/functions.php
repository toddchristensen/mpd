<?php
//Get Core Functions 
include_once "inc/core.php";
//Get Images Fuctions
include_once "inc/img.php";
//Get ACFS Functions
include_once "inc/acfs.php";
//Get Sync Ids
include_once "inc/syncids.php";
//Get Features Functions
include_once "inc/features.php";
//Get GCBS Functions
include_once "inc/gcbs.php";
//Get Jets Fuctions
include_once "inc/jets.php";
//Get Accessories Functions
include_once "inc/accessories.php";
//Get Products Functions
include_once "inc/products.php";
//Get Promotions Functions
include_once "inc/promotions.php";
//Auto Sync Functions disable for 1.6
//include_once "inc/autosync.php";
//Auto Sync Promos Functions disable for 1.6
// include_once "inc/autosyncpromos.php";


