<?php
/**
* Plugin Name: dsWaves NA
* Plugin URI: http://DesignStudio.com/
* Description: Syndication Platform for WordPress.
* Version: 1.6.2 (NA)
* Author: DesignStudio
* Author URI: http://DesignStudio.com/
* License: GPL12
*/

 
// function to create the DB / Options / Defaults					
function your_plugin_options_install() {
   	global $wpdb;
    global $your_db_name;
    $your_db_name = $wpdb->prefix . 'waves';
	$charset_collate = $wpdb->get_charset_collate();
	
	// create the ECPT metabox database table
	if($wpdb->get_var("show tables like '$your_db_name'") != $your_db_name) 
	{
		$sql = "CREATE TABLE " . $your_db_name . " (
		`id` mediumint(9) NOT NULL AUTO_INCREMENT,
		`autoSync` tinyint(1) NOT NULL,
    `brands` VARCHAR(2000) NOT NULL,
    `autoSyncPromo` tinyint(1) NOT NULL,
		`brandsPromo` VARCHAR(2000) NOT NULL,
		UNIQUE KEY id (id)
		)$charset_collate;";
 
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
    
    $autoSync = 0;
    $brands = "";

$wpdb->insert( 
	$your_db_name, 
	array(  
		'autoSync' => $autoSync, 
		'brands' => $brands, 
	) 
);

	}
 
}
// run the install scripts upon plugin activation
register_activation_hook(__FILE__,'your_plugin_options_install');

/**********************
****** Includes *******
**********************/

require_once 'functions.php';

/**********************
***** Admin Page ******
**********************/
add_action('admin_menu', 'dsWaves');


function dsWaves() {
  add_menu_page('DS Waves Menu', 'DS Waves', 'manage_options', 'ds-waves-page', 'dsWavesPage', plugin_dir_url( __FILE__ ) . 'assets/img/icons/dsWaves.png');
}


   //display admin menu page
   function dsWavesPage() {
  	global $wpdb;
    $rows = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}waves WHERE id= 1", OBJECT );
    foreach($rows as $row) {
      $wavesDB = $row;
    }
    $wavesBrands = $wavesDB->brands;
    $wavesBrandsPromo = $wavesDB->brandsPromo;
    $wavesSyncPromo = $wavesDB->autoSyncPromo;
    $wavesSync = $wavesDB->autoSync;
    $wavesBrands = explode( ',', $wavesBrands );
    $wavesBrands = array_filter($wavesBrands);
 
    $wavesBrandsPromo = explode( ',', $wavesBrandsPromo );
    $wavesBrandsPromo = array_filter($wavesBrandsPromo);
?>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/css/style.css" rel="stylesheet">
    <div id="waves" class="container">
     		<div class='box'>
		 		  <div class='wave -one'></div>
		 		  <div class='wave -two'></div>
		 		  <div class='wave -three'></div>
		 		  <div class='title'>WaVeS 1.6.2 (NA)</div>
		 		</div>
         <?php
      //auto Sync
      // if(isset($_POST['autoSyncSubmit'])) {
      //   if($_POST['autoSyncSwitch']) {
      //   $autoSync = $_POST['autoSyncSwitch'];
      //   } else {
      //     $autoSync = 0;
      //   }
      //   if($_POST['syncBrand']) {
      //   $catNames = $_POST['syncBrand'];
      //   } else {
      //     $catNames = [];
      //   } 
        
      //   autoSync($autoSync,$catNames); 
      // }

            //auto Sync Promo
            // if(isset($_POST['autoSyncPromoSubmit'])) {
            //   if($_POST['autoSyncPromoSwitch']) {
            //   $autoSync = $_POST['autoSyncPromoSwitch'];
            //   } else {
            //     $autoSync = 0;
            //   }
            //   if($_POST['syncBrandPromo']) {
            //   $catNames = $_POST['syncBrandPromo'];
            //   } else {
            //     $catNames = [];
            //   } 
              
            //   autoSyncPromo($autoSync,$catNames); 
            // }
        $sync = "manual";
      //Sync ACFS
      if(isset($_POST['acfSubmit'])) { syncAllACF(); }
      //Sync Features
      if(isset($_POST['syncFeatsSubmit'])) {$cName = $_POST['featCat']; syncFeats($cName, $sync); }
      //Sync GCBS
      if(isset($_POST['syncGCBSSubmit'])) {$cName = $_POST['brand']; syncGCBS($cName, $sync); }
      //Sync Jets
      if(isset($_POST['syncJetsSubmit'])) { $cName = $_POST['jetCat']; syncJets($cName, $sync); }
      //Sync Accessories
      if(isset($_POST['syncAcceSubmit'])) { $catNames = $_POST['brand']; syncAcce($catNames, $sync); }
      //Sync Products
      if(isset($_POST['syncProdsSubmit'])) { $catNames = $_POST['brand']; syncProds($catNames); }
      //SyncIDs
      if(isset($_POST['syncIDs'])) { $cName = $_POST['brand']; syncIDs($cName); }
      //SyncIDs
      if(isset($_POST['syncPromoSubmit'])) { $cName = $_POST['brand']; syncPromos($cName); }
    ?>
		    <!-- <div class="autoSync">
          <h2>Auto Sync</h2>
        
          <form method="post" action=""> 
            <div class="switch success-switch" id="autoSyncSwitch"> 
              <label> Off
              <input type="checkbox" name="autoSyncSwitch" value="1" id="autoSyncCheck"/>
              <span class="lever"></span> On </label>
          </div> -->

<style>
  .autoSyncMore {
    display:none;
  }
</style>
          <!-- <div class="autoSyncMore">
      
            <h4>Brands to Sync</h4>
            <br>
              <div class="form-check">
                  <input class="form-check-input" name="syncBrand[]" type="checkbox" value="hot-spring" id="defaultCheckbox1">
                  <label class="form-check-label" for="defaultCheckbox1">
                      Hot Spring Spas
                  </label>
                </div>
                <br>
                <div class="form-check">
                  <input class="form-check-input" name="syncBrand[]" type="checkbox" value="caldera-spas" id="defaultCheckbox2">
                  <label class="form-check-label" for="defaultCheckbox2">
                      Caldera Spas
                  </label>
                </div>
                <br>
                <div class="form-check">
                  <input class="form-check-input" name="syncBrand[]" type="checkbox" value="fantasy-spas" id="defaultCheckbox3">
                  <label class="form-check-label" for="defaultCheckbox3">
                      Fantasy Spas
                  </label>
                </div>
                <br>
                <div class="form-check">
                  <input class="form-check-input" name="syncBrand[]" type="checkbox" value="freeflow-spas" id="defaultCheckbox4">
                  <label class="form-check-label" for="defaultCheckbox4">
                      Freeflow Spas
                  </label>
                </div>
                <br>
                <div class="form-check">
                  <input class="form-check-input" name="syncBrand[]" type="checkbox" value="endless-pools-fitness-systems" id="defaultCheckbox5">
                  <label class="form-check-label" for="defaultCheckbox5">
                  Endless Pools Fitness Systems
                  </label>
                </div>
                <br>
                </div>
                <button id="button7" class="activeBtn" type="submit" name="autoSyncSubmit"></button>
                
               </form>
         
    </div> -->
<br>

<h2>Manual Sync </h2>
<br>

<!-- <h4><strong>Step 1:</strong> Download ACFs (Taking this out for 1.6)</h4>
<form method="post">
     <button id="button2" class="activeBtn" type="submit" name="acfSubmit"></button>
</form> -->


<br><br>
<h4><strong>Step 1:</strong> Add Sync IDs</h4>
<form method="post">
<select class="mdb-select colorful-select dropdown-dark syncidSelect" name="brand">
<option value="" disabled selected>Add Sync IDs</option>
      <option value="hot-spring" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/hot-spring.png" >Hot Spring Spas</option>
      <option value="hot-spring-accessories" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/hot-spring.png" >Hot Spring Spas Accessories</option>
      <option value="caldera-spas" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/caldera-spas.png">Caldera Spas</option>
      <option value="caldera-spas-accessories" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/caldera-spas.png" >Caldera Spas Accessories</option>
      <option value="fantasy-spas" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/fantasy-spas.png" >Fantasy Spas</option>
      <option value="fantasy-spas-accessories" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/fantasy-spas.png" >Fantasy Spas Accessories</option>
      <option value="freeflow-spas" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/freeflow-spas.png" >Freeflow Spas</option>
      <option value="freeflow-spas-accessories" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/freeflow-spas.png" >Freeflow Spas Accessories</option>
      <option value="endless-pools-fitness-systems"data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/endless-pools.png" > Endless Pools Fitness Systems</option>
      <option value="ep-accessories"data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/endless-pools.png" > Endless Pools Fitness Systems Accessories</option>
     
      </select>
<button type="submit" id="syncIDBtn" name="syncIDs"></button>
</form>

    <br><br>

  <h4><strong>Step 2:</strong> Sync Features </h4>
  <form method="post">
        <select class="mdb-select colorful-select dropdown-dark featsSelect" name="featCat">
        <option value="" disabled selected>Sync Features</option>
      <option value="hot-spring" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/hot-spring.png" >Hot Spring Spas</option>
      <option value="caldera-spas" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/caldera-spas.png" >Caldera Spas</option>
      <option value="fantasy-spas"data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/fantasy-spas.png" >Fantasy Spas</option>
      <option value="freeflow-spas" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/freeflow-spas.png" >Freeflow Spas</option>
      <!-- <option value="endless-pools-fitness-systems"data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/endless-pools.png" > Endless Pools Fitness Systems</option> -->
      </select>
        <button id="button6" type="submit" name="syncFeatsSubmit"></button>
    </form>

    <br><br>
  
    <h4><strong>Step 3:</strong> Sync General Content Blocks </h4>

  <form method="post">
        <select class="mdb-select colorful-select dropdown-dark gcbSelect" name="brand">
        <option value="" disabled selected>Sync GCBS</option>
      <option value="hot-spring" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/hot-spring.png" >Hot Spring Spas</option>
      <option value="caldera-spas" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/caldera-spas.png" >Caldera Spas</option>
      <option value="fantasy-spas"data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/fantasy-spas.png" >Fantasy Spas</option>
      <option value="freeflow-spas" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/freeflow-spas.png" >Freeflow Spas</option>
      <option value="endless-pools-fitness-systems"data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/endless-pools.png" > Endless Pools Fitness Systems</option>
      </select>
        <button id="button3" type="submit" name="syncGCBSSubmit"></button>
    </form>


    <br><br>
  <h4><strong>Step 4:</strong> Sync Jets </h4>
    <form method="post">
      <select class="mdb-select colorful-select dropdown-dark jetsSelect" name="jetCat">
      <option value="" disabled selected>Sync Jets</option>
      <option value="hot-spring" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/hot-spring.png" >Hot Spring Spas</option>
      <option value="caldera-spas" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/caldera-spas.png" >Caldera Spas</option>
      <option value="fantasy-spas"data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/fantasy-spas.png" >Fantasy Spas</option>
      <option value="freeflow-spas" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/freeflow-spas.png" >Freeflow Spas</option>
      <!-- <option value="endless-pools-fitness-systems"data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/endless-pools.png" > Endless Pools Fitness Systems</option> -->
      </select>
        <button id="button5" type="submit" name="syncJetsSubmit"></button>
    </form>
    <br><br>
  <h4><strong>Step 5:</strong> Sync Accessories </h4>

    <form method="post">
          <select class="mdb-select colorful-select dropdown-dark accSelect" name="brand">
          <option value="" disabled selected>Sync Accessories</option>
      <option value="hot-spring-accessories,hot-spring-hot-tub-innovation,hot-spring-hot-tub-water-care,steps-covers-lifters,hot-spring-covers,hot-spring-lifters,hot-spring-steps" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/hot-spring.png" >Hot Spring Spas</option>
      <option value="caldera-spas-accessories,caldera-spas-hot-tub-enjoyment,caldera-spas-hot-tub-innovation,caldera-spas-hot-tub-water-care,caldera-spas-steps-covers-lifters,caldera-spas-covers,caldera-spas-lifters,caldera-spas-steps" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/caldera-spas.png" >Caldera Spas</option>
      <option value="fantasy-spas-accessories"data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/fantasy-spas.png" >Fantasy Spas</option>
      <option value="freeflow-spas-accessories" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/freeflow-spas.png" >Freeflow Spas</option>
      <option value="ep-accessories"data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/endless-pools.png" > Endless Pools Fitness Systems</option>
      </select>
      <button id="button4" type="submit" name="syncAcceSubmit"></button>
    </form>

    <br><br>
  <h4><strong>Step 6:</strong> Sync Products </h4>

      <form method="post">
<select class="mdb-select colorful-select dropdown-dark prodSelect" name="brand">
    <option value="" disabled selected>Sync Products</option>
      <option value="highlife-nxt,highlife,limelight,hotspot" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/hot-spring.png" >Hot Spring Spas</option>
      <option value="utopia,paradise,vacanza" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/caldera-spas.png" >Caldera Spas</option>
      <option value="fantasy-spas"data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/fantasy-spas.png" >Fantasy Spas</option>
      <option value="freeflow-spas" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/freeflow-spas.png" >Freeflow Spas</option>
      <option value="ep-fitness-systems,ep-recsport-recreation-systems,ep-swimcross-exercise-systems"data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/endless-pools.png" > Endless Pools Fitness Systems</option>
      </select>
      <button id="button" type="submit" name="syncProdsSubmit"></button>
    </form>



    <br><br>
  <h4><strong>Optional:</strong> Sync Promotions </h4>

      <form method="post">
<select class="mdb-select colorful-select dropdown-dark promoSelect" name="brand">
    <option value="" disabled selected>Sync Promotions</option>
    <option value="hot-spring" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/hot-spring.png" >Hot Spring Spas</option>
      <option value="caldera-spas" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/caldera-spas.png" >Caldera Spas</option>
      <option value="fantasy-spas"data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/fantasy-spas.png" >Fantasy Spas</option>
      <option value="freeflow-spas" data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/freeflow-spas.png" >Freeflow Spas</option>
      <option value="endless-pools-fitness-systems"data-icon="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/img/logos/endless-pools.png" > Endless Pools Fitness Systems</option>
     </select>
      <button id="button8" type="submit" name="syncPromoSubmit"></button>
    </form>
</div>


<!-- JQuery -->
<script type="text/javascript" src="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo  plugin_dir_url( __FILE__ ); ?>assets/js/mdb.min.js"></script>


<script>

$.noConflict();
jQuery( document ).ready(function( $ ) {

 $('.mdb-select').material_select();

});

   // Material Select Initialization
   jQuery(document).ready(function() {

<?php foreach($wavesBrandsPromo as $wavesBrandPromo) {  ?>
<?php if($wavesBrandPromo == "hot-spring") {?>
jQuery('#defaultCheckboxPromo1').attr('checked','checked');
<?php } ?>
  <?php if($wavesBrandPromo == "caldera-spas") {?>
  jQuery('#defaultCheckboxPromo2').attr('checked','checked');
  <?php } ?>
  <?php if($wavesBrandPromo == "fantasy-spas") {?>
  jQuery('#defaultCheckboxPromo3').attr('checked','checked');
  <?php } ?>
  <?php if($wavesBrandPromo == "freeflow-spas") {?>
  jQuery('#defaultCheckboxPromo4').attr('checked','checked');
  <?php } ?>
  <?php if($wavesBrandPromo == "endless-pools-fitness-systems") {?>
  jQuery('#defaultCheckboxPromo5').attr('checked','checked');
  <?php }} 
foreach ($wavesBrands as $wavesBrand) {  ?>
<?php if($wavesBrand == "hot-spring") {?>
  jQuery('#defaultCheckbox1').attr('checked','checked');
  <?php } ?>
  <?php if($wavesBrand == "caldera-spas") {?>
  jQuery('#defaultCheckbox2').attr('checked','checked');
  <?php } ?>
  <?php if($wavesBrand == "fantasy-spas") {?>
  jQuery('#defaultCheckbox3').attr('checked','checked');
  <?php } ?>
  <?php if($wavesBrand == "freeflow-spas") {?>
  jQuery('#defaultCheckbox4').attr('checked','checked');
  <?php } ?>
  <?php if($wavesBrand == "endless-pools-fitness-systems") {?>
  jQuery('#defaultCheckbox5').attr('checked','checked');
  <?php }} if($wavesSyncPromo == 1) { ?>
  jQuery('#autoSyncPromoCheck').attr('checked','checked');
  jQuery('.autoSyncPromoMore').toggle();
    <?php } if($wavesSync == 1) { ?>
  jQuery('#autoSyncCheck').attr('checked','checked');
  jQuery('.autoSyncMore').toggle();
    <?php }?>

  jQuery('#defaultCheckbox1').change(function(){

    var attr = jQuery('#defaultCheckbox1').attr('checked');
    // For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
    if (typeof attr !== typeof undefined && attr !== false) {
      jQuery('#defaultCheckbox1').removeAttr('checked');
    } else {
      jQuery('#defaultCheckbox1').attr('checked','checked');
    }
  });

  jQuery('#defaultCheckbox2').change(function(){
    
    var attr = jQuery('#defaultCheckbox2').attr('checked');
    // For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
    if (typeof attr !== typeof undefined && attr !== false) {
      jQuery('#defaultCheckbox2').removeAttr('checked');
    } else {
      jQuery('#defaultCheckbox2').attr('checked','checked');
    }
  });

  jQuery('#defaultCheckbox3').change(function(){

    var attr = jQuery('#defaultCheckbox3').attr('checked');
    // For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
    if (typeof attr !== typeof undefined && attr !== false) {
      jQuery('#defaultCheckbox3').removeAttr('checked');
    } else {
      jQuery('#defaultCheckbox3').attr('checked','checked');
    }
  });


  jQuery('#defaultCheckbox4').change(function(){

var attr = jQuery('#defaultCheckbox4').attr('checked');

// For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
if (typeof attr !== typeof undefined && attr !== false) {
jQuery('#defaultCheckbox4').removeAttr('checked');
} else {
jQuery('#defaultCheckbox4').attr('checked','checked');
}
});

  jQuery('#defaultCheckbox5').change(function(){

var attr = jQuery('#defaultCheckbox5').attr('checked');

// For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
if (typeof attr !== typeof undefined && attr !== false) {
jQuery('#defaultCheckbox5').removeAttr('checked');
} else {
jQuery('#defaultCheckbox5').attr('checked','checked');
}
});

  jQuery('#defaultCheckboxPromo1').change(function(){

var attr = jQuery('#defaultCheckboxPromo1').attr('checked');

// For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
if (typeof attr !== typeof undefined && attr !== false) {
jQuery('#defaultCheckboxPromo1').removeAttr('checked');
} else {
jQuery('#defaultCheckboxPromo1').attr('checked','checked');
}
});

jQuery('#defaultCheckboxPromo2').change(function(){

var attr = jQuery('#defaultCheckboxPromo2').attr('checked');

// For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
if (typeof attr !== typeof undefined && attr !== false) {
jQuery('#defaultCheckboxPromo2').removeAttr('checked');
} else {
jQuery('#defaultCheckboxPromo2').attr('checked','checked');
}
});


jQuery('#defaultCheckboxPromo3').change(function(){

var attr = jQuery('#defaultCheckboxPromo3').attr('checked');

// For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
if (typeof attr !== typeof undefined && attr !== false) {
jQuery('#defaultCheckboxPromo3').removeAttr('checked');
} else {
jQuery('#defaultCheckboxPromo3').attr('checked','checked');
}
});


  jQuery('#defaultCheckboxPromo4').change(function(){

var attr = jQuery('#defaultCheckboxPromo4').attr('checked');

// For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
if (typeof attr !== typeof undefined && attr !== false) {
jQuery('#defaultCheckboxPromo4').removeAttr('checked');
} else {
jQuery('#defaultCheckboxPromo4').attr('checked','checked');
}
});

  jQuery('#defaultCheckboxPromo5').change(function(){
var attr = jQuery('#defaultCheckboxPromo5').attr('checked');
// For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
if (typeof attr !== typeof undefined && attr !== false) {
jQuery('#defaultCheckboxPromo5').removeAttr('checked');
} else {
jQuery('#defaultCheckboxPromo5').attr('checked','checked');
}});

   jQuery('#autoSyncPromoSwitch').change(function(){

var attr = jQuery('#autoSyncPromoCheck').attr('checked');

// For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
if (typeof attr !== typeof undefined && attr !== false) {
jQuery('#autoSyncPromoCheck').removeAttr('checked');

} else {
jQuery('#autoSyncPromoCheck').attr('checked','checked');
}
jQuery('.autoSyncPromoMore').toggle();
});


     jQuery('#autoSyncSwitch').change(function(){

       var attr = jQuery('#autoSyncCheck').attr('checked');

// For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
if (typeof attr !== typeof undefined && attr !== false) {
  jQuery('#autoSyncCheck').removeAttr('checked');
} else {
  jQuery('#autoSyncCheck').attr('checked','checked');
}
      jQuery('.autoSyncMore').toggle();
    });


  jQuery('#syncIDBtn').prop('disabled', true);

jQuery('.syncidSelect').bind("change", function() { 
    jQuery('#syncIDBtn').prop('disabled', false);
    jQuery('#syncIDBtn').addClass('activeBtn');
});

  });

</script>

<script>
jQuery(function() {



 jQuery( "#syncIDBtn" ).click(function() {
  toastr["info"]("Please wait while IDs are syncing...");
});

      jQuery('#button').prop('disabled', true);

jQuery('.prodSelect').bind("change", function() { 
    jQuery('#button').prop('disabled', false);
    jQuery('#button').addClass('activeBtn');
});

  jQuery( "#button" ).click(function() {
    toastr["info"]("Please wait while products are syncing...");
    jQuery( "#button" ).addClass( "onclic", 250, validate);
  });

  function validate() {
    setTimeout(function() {
      jQuery( "#button" ).removeClass( "onclic" );
      jQuery( "#button" ).addClass( "validate", 450, callback );
    }, 2250 );
  }
    function callback() {
      setTimeout(function() {
        jQuery( "#button" ).removeClass( "validate" );
      }, 1250 );
    }
  });

</script>


<script>
jQuery(function() {

  jQuery( "#button2" ).click(function() {
    toastr["info"]("Please wait while ACFs are downloading...");
    jQuery( "#button2" ).addClass( "onclic", 250, validate);
  });

  function validate() {
    setTimeout(function() {
      jQuery( "#button2" ).removeClass( "onclic" );
      jQuery( "#button2" ).addClass( "validate", 450, callback );
    }, 2250 );
  }
    function callback() {
      setTimeout(function() {
        jQuery( "#button2" ).removeClass( "validate" );
      }, 1250 );
    }
  });
</script>

<script>
jQuery(function() {

    jQuery('#button3').prop('disabled', true);

jQuery('.gcbSelect').bind("change", function() { 
    jQuery('#button3').prop('disabled', false);
    jQuery('#button3').addClass('activeBtn');
});

  jQuery( "#button3" ).click(function() {
    toastr["info"]("Please wait while General Content Blocks are syncing...");
    jQuery( "#button3" ).addClass( "onclic", 250, validate);
  });

  function validate() {
    setTimeout(function() {
      jQuery( "#button3" ).removeClass( "onclic" );
      jQuery( "#button3" ).addClass( "validate", 450, callback );
    }, 2250 );
  }
    function callback() {
      setTimeout(function() {
        jQuery( "#button3" ).removeClass( "validate" );
      }, 1250 );
    }
  });
</script>

<script>
jQuery(function() {
  jQuery('#button4').prop('disabled', true);

jQuery('.accSelect').bind("change", function() { 
    jQuery('#button4').prop('disabled', false);
    jQuery('#button4').addClass('activeBtn');
});
  jQuery( "#button4" ).click(function() {
    toastr["info"]("Please wait while Accessories are syncing...");
    jQuery( "#button4" ).addClass( "onclic", 250, validate);
  });

  function validate() {
    setTimeout(function() {
      jQuery( "#button4" ).removeClass( "onclic" );
      jQuery( "#button4" ).addClass( "validate", 450, callback );
    }, 2250 );
  }
    function callback() {
      setTimeout(function() {
        jQuery( "#button4" ).removeClass( "validate" );
      }, 1250 );
    }
  });
</script>

<script>
jQuery(function() {
  jQuery('#button5').prop('disabled', true);

jQuery('.jetsSelect').bind("change", function() { 
    jQuery('#button5').prop('disabled', false);
    jQuery('#button5').addClass('activeBtn');
});
  jQuery( "#button5" ).click(function() {
    toastr["info"]("Please wait while Jets are syncing...");
    jQuery( "#button5" ).addClass( "onclic", 250, validate);
  });

  function validate() {
    setTimeout(function() {
      jQuery( "#button5" ).removeClass( "onclic" );
      jQuery( "#button5" ).addClass( "validate", 450, callback );
    }, 2250 );
  }
    function callback() {
      setTimeout(function() {
        jQuery( "#button5" ).removeClass( "validate" );
      }, 1250 );
    }
  });
</script>


<script>
jQuery(function() {
  jQuery('#button6').prop('disabled', true);

jQuery('.featsSelect').bind("change", function() { 
    jQuery('#button6').prop('disabled', false);
    jQuery('#button6').addClass('activeBtn');
});
  jQuery( "#button6" ).click(function() {
    toastr["info"]("Please wait while Features are syncing...");
    jQuery( "#button6" ).addClass( "onclic", 250, validate);
  });

  function validate() {
    setTimeout(function() {
      jQuery( "#button6" ).removeClass( "onclic" );
      jQuery( "#button6" ).addClass( "validate", 450, callback );
    }, 2250 );
  }
    function callback() {
      setTimeout(function() {
        jQuery( "#button6" ).removeClass( "validate" );
      }, 1250 );
    }
  });
</script>


<script>
jQuery(function() {
  jQuery('#button8').prop('disabled', true);

jQuery('.promoSelect').bind("change", function() { 
    jQuery('#button8').prop('disabled', false);
    jQuery('#button8').addClass('activeBtn');
});
  jQuery( "#button8" ).click(function() {
    toastr["info"]("Please wait while Promotions are syncing...");
    jQuery( "#button8" ).addClass( "onclic", 250, validate);
  });

  function validate() {
    setTimeout(function() {
      jQuery( "#button8" ).removeClass( "onclic" );
      jQuery( "#button8" ).addClass( "validate", 450, callback );
    }, 2250 );
  }
    function callback() {
      setTimeout(function() {
        jQuery( "#button8" ).removeClass( "validate" );
      }, 1250 );
    }
  });
</script>

<script>
jQuery(function() {
  jQuery( "#button10" ).click(function() {
    toastr["info"]("Please wait while Enabling Promotions auto syncing...");
    jQuery( "#button10" ).addClass( "onclic", 250, validate);
  });

  function validate() {
    setTimeout(function() {
      jQuery( "#button10" ).removeClass( "onclic" );
      jQuery( "#button10" ).addClass( "validate", 450, callback );
    }, 2250 );
  }
    function callback() {
      setTimeout(function() {
        jQuery( "#button10" ).removeClass( "validate" );
      }, 1250 );
    }
  });
</script>

   

<?php 
} ?>
